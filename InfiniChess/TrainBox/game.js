var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
window.onload = function () {
    var game = new InfiniChessClient.Game();
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var IdentifierPathPair = (function () {
        function IdentifierPathPair(_assetKey, _path) {
            this.assetKey = _assetKey;
            this.path = _path;
        }
        return IdentifierPathPair;
    }());
    InfiniChessClient.IdentifierPathPair = IdentifierPathPair;
    //Class to handle the management and loading of assets. Assets need to be preloaded before they are added to the canvas
    var Assets = (function () {
        function Assets() {
        }
        //Load all the assets into the game, except for the loading bar. For use in the preloadedstate. Needs to be kept updated when a new asset is added
        Assets.LoadAllAssets = function (state) {
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.ChessGrid);
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.CellHighlight);
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.ChessPieces);
            InfiniChessClient.Assets.LoadSpriteSheet(state, InfiniChessClient.Assets.Button, 350, 120);
            InfiniChessClient.Assets.LoadSound(state, InfiniChessClient.Assets.PlaceTrackAudio);
        };
        //static functions for preloading images into memory before they are stage added.
        Assets.LoadImage = function (state, imageToLoad) {
            state.load.image(imageToLoad.assetKey, imageToLoad.path);
        };
        Assets.LoadSound = function (state, soundToLoad) {
            state.load.audio(soundToLoad.assetKey, soundToLoad.path);
        };
        Assets.LoadSpriteSheet = function (state, spriteSheetToLoad, frameWidth, frameHeight, frameNum) {
            if (frameNum === void 0) { frameNum = -1; }
            if (frameNum == -1) {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight);
            }
            else {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight, frameNum);
            }
        };
        //List of the asset keys paired to their reletive paths
        //Images
        Assets.LoadingBar = new InfiniChessClient.IdentifierPathPair('loadingBar', 'assets/sprites/loadingBar.png');
        Assets.ChessGrid = new InfiniChessClient.IdentifierPathPair('chessGrid', 'assets/sprites/ChessGrid.png');
        Assets.CellHighlight = new InfiniChessClient.IdentifierPathPair('cellHighlight', 'assets/sprites/CellHighlight.png');
        Assets.ChessPieces = new InfiniChessClient.IdentifierPathPair('chessPieces', 'assets/sprites/Pieces.png');
        Assets.Button = new InfiniChessClient.IdentifierPathPair('button', 'assets/sprites/Button.png');
        //Audio
        Assets.PlaceTrackAudio = new InfiniChessClient.IdentifierPathPair('placeTrackAudio', 'assets/audio/PlaceTrack.mp3');
        return Assets;
    }());
    InfiniChessClient.Assets = Assets;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    (function (PieceType) {
        PieceType[PieceType["Pawn"] = 0] = "Pawn";
        PieceType[PieceType["Rook"] = 1] = "Rook";
        PieceType[PieceType["Knight"] = 2] = "Knight";
        PieceType[PieceType["Bishop"] = 3] = "Bishop";
        PieceType[PieceType["Queen"] = 4] = "Queen";
    })(InfiniChessClient.PieceType || (InfiniChessClient.PieceType = {}));
    var PieceType = InfiniChessClient.PieceType;
    //The base piece class, has subclasses for each individual piece
    var Piece = (function (_super) {
        __extends(Piece, _super);
        //Setup Piece
        function Piece(game, grid, serverPieceIndexMapping, xGridSpawnIndex, yGridSpawnIndex, pieceSpriteKey, texSubRect, owningPlayer, pieceGroup, boardGroup, uiGroup) {
            var _this = this;
            _super.call(this, game, grid.GetCellAtIndex(xGridSpawnIndex, yGridSpawnIndex).GetCenter().x, grid.GetCellAtIndex(xGridSpawnIndex, yGridSpawnIndex).GetCenter().y, pieceSpriteKey);
            this.lastTimeStampLandedOnTile = 0; //The last timestamp that this piece was placed on a tile ACCORDING TO THE SERVER
            this.movementCoolDownInSeconds = 5; //How many seconds must pass between the piece landing and it being able to move again. Set in derived class. Shouldn't really do anything but effect the look of the timer/allow selection, validation done server side.
            this.isSelected = false;
            this.targetCell = null; //Set when the MakeMove function is called, and the player tweens towards its new cell, used as the player stays in motion for a while and dosent arrive directly.
            //Called when piece is clicked on, toggles selection grid on/off
            this.ToggleSelection = function () {
                //Only do selection/highlights if we're local
                if (!_this.owningPlayer.IsLocalPlayer()) {
                    return;
                }
                if (_this.IsSelectable()) {
                    if (_this.IsSelected()) {
                        _this.DeSelectPiece();
                    }
                    else {
                        _this.SelectPiece();
                    }
                }
            };
            //Called when the player clicks the piece to highlight it for movement
            this.SelectPiece = function () {
                //Only do selection/highlights if we're local
                if (!_this.owningPlayer.IsLocalPlayer()) {
                    return;
                }
                _this.isSelected = true;
                _this.ShowMovementHighlightSquares();
            };
            //Unselects the piece, hides the movement
            this.DeSelectPiece = function () {
                //Only do selection/highlights if we're local
                if (!_this.owningPlayer.IsLocalPlayer()) {
                    return;
                }
                _this.isSelected = false;
                _this.HideMovementHighlightSquares();
            };
            //Whether or not this piece is currently selected for movemetn
            this.IsSelected = function () {
                return _this.isSelected;
            };
            //Shows the squares this piece can move to by placing and enabling overlay squares on the grid
            this.ShowMovementHighlightSquares = function () {
                //Only do selection/highlights if we're local
                if (!_this.owningPlayer.IsLocalPlayer()) {
                    return;
                }
                //The movement highlights are lazily created, so they, (or even their containing array,) may not exist when we get to the function, so we just create them.
                if (_this.movementPatternHighlights == null) {
                    _this.movementPatternHighlights = [];
                }
                //If the movement pattern dosent have enough sprites for the indices, make em. This should really only happen the first time this is called, as pieces dont change their movement pattern, but even if it happens more, it's no biggie.
                for (var i = 0; i < _this.movementPatternIndices.length; i++) {
                    if (_this.movementPatternHighlights.length <= i) {
                        _this.movementPatternHighlights.push(new InfiniChessClient.CellHighlight(_this.game, _this));
                        _this.boardGroup.add(_this.movementPatternHighlights[i]);
                        _this.movementPatternHighlights[i].kill();
                    }
                }
                //Set the now definately existing movement tile highlights to be visible and be in the correct position
                for (var i = 0; i < _this.movementPatternIndices.length; i++) {
                    try {
                        var cellToPlaceOn = _this.grid.GetCellAtIndex(_this.currentCell.GetGridIndex().x + _this.movementPatternIndices[i].x, _this.currentCell.GetGridIndex().y + _this.movementPatternIndices[i].y);
                        if (cellToPlaceOn != null) {
                            _this.movementPatternHighlights[i].PlaceOnCell(cellToPlaceOn);
                            _this.movementPatternHighlights[i].revive();
                        }
                    }
                    catch (error) {
                    }
                }
            };
            //Hides the movement highlights for when the piece is deselected. Tiles arnt deleted, as they get re-used, just disabled
            this.HideMovementHighlightSquares = function () {
                //Kill all the movement pattern highlights, they get revived in the ShowMovementHighlightSquares method
                for (var i = 0; i < _this.movementPatternHighlights.length; i++) {
                    _this.movementPatternHighlights[i].kill();
                }
            };
            //Called from the CellHighlights in response to mouse clicks
            this.SelectionResponse = function (cellSelected) {
                if (_this.IsSelected()) {
                    _this.RequestMovement(InfiniChessClient.GameState.socket, cellSelected);
                    _this.DeSelectPiece();
                }
            };
            //Send a packet to the server and request movement. This is basically calling "Move()" over the network, with verification n shiz.
            this.RequestMovement = function (connectionSocket, movementRequestCell) {
                connectionSocket.emit('movementrequest', Date.now(), _this.owningPlayer.GetPlayerNetworkID(), _this.serverPieceIndexMapping, _this.GetPieceType(), movementRequestCell.GetGridIndex().x, movementRequestCell.GetGridIndex().y);
            };
            //Purge all the piece data to prepare for destruction
            this.PurgePiece = function () {
                if (_this.currentCell != null) {
                    _this.currentCell.NotifyCellIsNowEmpty();
                }
                _this.body = null;
                _this.nameLabel.destroy();
                _this.coolDownLabel.destroy();
                _this.owningPlayer = null;
            };
            //Set the text of the piece name label
            this.UpdatePieceNameLabel = function (name) {
                _this.nameLabel.text = name;
            };
            //Do a move to a certain cell over a certain time, normally in response to a movement packet
            this.MakeMove = function (cellToMoveTo, timestampToArrive) {
                //Do sanity checks
                if (cellToMoveTo == null) {
                    throw new ReferenceError('Null Cell to move to');
                }
                if (_this.targetCell != null) {
                    throw new Error('Target cell needs to be null to make a move, is target cell is not null, piece should be in motion');
                }
                _this.DeSelectPiece();
                _this.RemoveFromCell();
                var movementTween = _this.game.add.tween(_this.position);
                console.log('Move Timestamp : ' + Date.now());
                var timeMoveTakes = timestampToArrive - Date.now();
                if (timeMoveTakes < 0) {
                    timeMoveTakes = 0;
                }
                movementTween.to({ x: cellToMoveTo.GetCenter().x, y: cellToMoveTo.GetCenter().y }, timeMoveTakes);
                _this.targetCell = cellToMoveTo;
                movementTween.onComplete.add(_this.ArriveAtTargetCell, _this);
                movementTween.start();
            };
            //Called from make move as a tween callback, places to piece on the cell.
            this.ArriveAtTargetCell = function () {
                _this.PlaceOnCell(_this.targetCell);
                _this.SetCellAllignedPosition();
                _this.targetCell = null;
            };
            //Put the piece on a cell at a specified index
            this.PlaceOnCellAtIndex = function (xIndex, yIndex) {
                _this.PlaceOnCell(_this.grid.GetCellAtIndex(xIndex, yIndex));
            };
            //Put the piece on the cell, letting the cell and piece know about the other
            this.PlaceOnCell = function (cell) {
                if (_this.currentCell != null) {
                    _this.currentCell.NotifyCellIsNowEmpty();
                }
                cell.NotifyPieceOnCell(_this);
                _this.currentCell = cell;
                _this.SetCellAllignedPosition();
                _this.SetTimestampOfLastCellLanding(Date.now());
            };
            //Remove this piece from whatever cell it's on
            this.RemoveFromCell = function () {
                if (_this.currentCell != null) {
                    _this.currentCell.NotifyCellIsNowEmpty();
                    _this.currentCell = null;
                }
            };
            //Remove this piece from the grid, and destroy the sprite.
            this.RemoveFromGame = function () {
                _this.RemoveFromCell();
                _this.nameLabel.destroy();
                _this.coolDownLabel.destroy();
                _this.destroy();
            };
            //Puts the piece at the correct screen position according to what cell it's sitting on
            this.SetCellAllignedPosition = function () {
                _this.position.x = _this.currentCell.GetCenter().x;
                _this.position.y = _this.currentCell.GetCenter().y;
            };
            //Sets the last time that this piece landed on a cell, from the server normally. Used for movement colldowns
            this.SetTimestampOfLastCellLanding = function (timeStamp) {
                _this.lastTimeStampLandedOnTile = timeStamp;
            };
            //Get the value of the cooldown timer, based on the current timestamp and the last placed on cell timestamp
            this.GetMovmentCooldownTimerValue = function () {
                var coolDownTimerVal = ((_this.lastTimeStampLandedOnTile + (_this.movementCoolDownInSeconds * 1000)) - Date.now()) / 1000; //Is ms by default
                if (coolDownTimerVal < 0) {
                    coolDownTimerVal = 0;
                }
                return coolDownTimerVal;
            };
            //Return whether or not the local cooldown timer thinks the piece is selectable, ie the timer is <= 0 and we're not moving
            this.IsSelectable = function () {
                if ((_this.GetMovmentCooldownTimerValue() <= 0) && (_this.currentCell != null)) {
                    return true;
                }
                return false;
            };
            //Bring the name label to the top of the view
            this.BringNameLableToTop = function () {
                _this.nameLabel.bringToTop();
            };
            //Bring the cooldown label to the top of the view
            this.BringCooldownLabelToTop = function () {
                _this.coolDownLabel.bringToTop();
            };
            //Get what type of piece this is
            this.GetPieceType = function () {
                return _this.pieceType;
            };
            //Get the piece map index as it is known to the server
            this.GetServerPieceMapIndex = function () {
                return _this.serverPieceIndexMapping;
            };
            //Set the correct texture subrect for the piece
            this.cropRect = texSubRect;
            if (!owningPlayer.IsLocalPlayer()) {
                this.cropRect.offset(0, Piece.blackPieceSubRectYOffset);
                this.body = null;
            }
            this.updateCrop();
            this.grid = grid;
            this.owningPlayer = owningPlayer;
            this.serverPieceIndexMapping = serverPieceIndexMapping;
            //Setup the text that displays the owning players name
            var style = { font: "18px Arial", fill: "#fff", align: "center" };
            this.nameLabel = new Phaser.Text(game, this.x, this.y, this.owningPlayer.GetPlayerName(), style);
            this.nameLabel.stroke = '#000000';
            this.nameLabel.strokeThickness = 4;
            this.nameLabel.anchor.set(0.5);
            this.pieceGroup = pieceGroup;
            this.boardGroup = boardGroup;
            this.uiGroup = uiGroup;
            this.pieceGroup.add(this.nameLabel);
            //Setup the text that displays the piece move cooldown timer
            this.coolDownLabel = new Phaser.Text(game, this.x, this.y, this.owningPlayer.GetPlayerName(), style);
            this.coolDownLabel.stroke = '#000000';
            this.coolDownLabel.strokeThickness = 4;
            this.coolDownLabel.anchor.set(0.5);
            this.pieceGroup.add(this.coolDownLabel);
            //Should be over-set by network later
            this.lastTimeStampLandedOnTile = Date.now();
            //Put the piece on the spawn point.
            this.PlaceOnCellAtIndex(xGridSpawnIndex, yGridSpawnIndex);
            //Set the sprite anchor so that the piece appears to sit on the cell
            this.anchor = new Phaser.Point(0.5, 0.86);
            //Initialised movement pattern array, uses derived class implementation of base method
            this.movementPatternIndices = this.GetCandidateMovementIndices();
            //Setup mouse selection, if this is a local piece
            if (this.owningPlayer.IsLocalPlayer()) {
                this.inputEnabled = true;
                this.events.onInputDown.add(this.ToggleSelection, this);
            }
            //Add this piece to the piece group
            this.pieceGroup.add(this);
        }
        //Frame by frame logic update
        Piece.prototype.update = function () {
            //Update the piece specific UI
            if (this.nameLabel != null) {
                this.nameLabel.position.x = this.position.x;
                this.nameLabel.position.y = this.position.y + this.height / 8;
            }
            if (this.coolDownLabel != null) {
                this.coolDownLabel.position.x = this.position.x;
                this.coolDownLabel.position.y = this.position.y + this.height / 4;
                this.coolDownLabel.text = Math.round(this.GetMovmentCooldownTimerValue()).toString(); //Cooldown label based on timestamp
            }
        };
        Piece.blackPieceSubRectYOffset = 126; //How much to offset the sprite sheet rect if we want to be a black piece as opposed to a while. Assumes the black piece is directly below the white on the spritesheet
        return Piece;
    }(Phaser.Sprite));
    InfiniChessClient.Piece = Piece;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A pawn piece.
    var Pawn = (function (_super) {
        __extends(Pawn, _super);
        function Pawn(game, grid, serverPieceIndexMapping, spawnIndexOnGrid, owningPlayer, pieceGroup, boardGroup, uiGroup) {
            _super.call(this, game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, InfiniChessClient.Assets.ChessPieces.assetKey, new Phaser.Rectangle(21, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);
            this.pieceType = InfiniChessClient.PieceType.Pawn;
            this.movementCoolDownInSeconds = 3;
        }
        Pawn.prototype.GetCandidateMovementIndices = function () {
            var pts = [];
            //Load all of the possible pawn movement squares
            pts.push(new Phaser.Point(0, 1));
            pts.push(new Phaser.Point(0, -1));
            pts.push(new Phaser.Point(1, 0));
            pts.push(new Phaser.Point(-1, 0));
            return pts;
        };
        return Pawn;
    }(InfiniChessClient.Piece));
    InfiniChessClient.Pawn = Pawn;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A knight piece.
    var Knight = (function (_super) {
        __extends(Knight, _super);
        function Knight(game, grid, serverPieceIndexMapping, spawnIndexOnGrid, owningPlayer, pieceGroup, boardGroup, uiGroup) {
            _super.call(this, game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, InfiniChessClient.Assets.ChessPieces.assetKey, new Phaser.Rectangle(212, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);
            this.pieceType = InfiniChessClient.PieceType.Knight;
            this.movementCoolDownInSeconds = 5;
        }
        Knight.prototype.GetCandidateMovementIndices = function () {
            var pts = [];
            //Load all of the possible knight movement squares
            pts.push(new Phaser.Point(2, 1));
            pts.push(new Phaser.Point(1, 2));
            pts.push(new Phaser.Point(-2, 1));
            pts.push(new Phaser.Point(-1, 2));
            pts.push(new Phaser.Point(2, -1));
            pts.push(new Phaser.Point(1, -2));
            pts.push(new Phaser.Point(-2, -1));
            pts.push(new Phaser.Point(-1, -2));
            return pts;
        };
        return Knight;
    }(InfiniChessClient.Piece));
    InfiniChessClient.Knight = Knight;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A bishop piece.
    var Bishop = (function (_super) {
        __extends(Bishop, _super);
        function Bishop(game, grid, serverPieceIndexMapping, spawnIndexOnGrid, owningPlayer, pieceGroup, boardGroup, uiGroup) {
            _super.call(this, game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, InfiniChessClient.Assets.ChessPieces.assetKey, new Phaser.Rectangle(307, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);
            this.pieceType = InfiniChessClient.PieceType.Bishop;
            this.movementCoolDownInSeconds = 5;
        }
        Bishop.prototype.GetCandidateMovementIndices = function () {
            var pts = [];
            var range = 10;
            //Load all of the possible bishop movement squares
            for (var i = 1; i <= range; i++) {
                pts.push(new Phaser.Point(i, i));
            }
            for (var j = 1; j <= range; j++) {
                pts.push(new Phaser.Point(-j, j));
            }
            for (var k = 1; k <= range; k++) {
                pts.push(new Phaser.Point(k, -k));
            }
            for (var l = 1; l <= range; l++) {
                pts.push(new Phaser.Point(-l, -l));
            }
            return pts;
        };
        return Bishop;
    }(InfiniChessClient.Piece));
    InfiniChessClient.Bishop = Bishop;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A rook piece.
    var Rook = (function (_super) {
        __extends(Rook, _super);
        function Rook(game, grid, serverPieceIndexMapping, spawnIndexOnGrid, owningPlayer, pieceGroup, boardGroup, uiGroup) {
            _super.call(this, game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, InfiniChessClient.Assets.ChessPieces.assetKey, new Phaser.Rectangle(117, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);
            this.pieceType = InfiniChessClient.PieceType.Rook;
            this.movementCoolDownInSeconds = 5;
        }
        Rook.prototype.GetCandidateMovementIndices = function () {
            var pts = [];
            var range = 10;
            //Load all of the possible rook movement squares
            for (var i = 1; i <= range; i++) {
                pts.push(new Phaser.Point(0, i));
            }
            for (var j = 1; j <= range; j++) {
                pts.push(new Phaser.Point(0, -j));
            }
            for (var k = 1; k <= range; k++) {
                pts.push(new Phaser.Point(k, 0));
            }
            for (var l = 1; l <= range; l++) {
                pts.push(new Phaser.Point(-l, 0));
            }
            return pts;
        };
        return Rook;
    }(InfiniChessClient.Piece));
    InfiniChessClient.Rook = Rook;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A queen piece.
    var Queen = (function (_super) {
        __extends(Queen, _super);
        function Queen(game, grid, serverPieceIndexMapping, spawnIndexOnGrid, owningPlayer, pieceGroup, boardGroup, uiGroup) {
            _super.call(this, game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, InfiniChessClient.Assets.ChessPieces.assetKey, new Phaser.Rectangle(402, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);
            this.pieceType = InfiniChessClient.PieceType.Queen;
            this.movementCoolDownInSeconds = 8;
        }
        Queen.prototype.GetCandidateMovementIndices = function () {
            var pts = [];
            var range = 10;
            //Load all of the possible queen movement squares
            for (var i = 1; i <= range; i++) {
                pts.push(new Phaser.Point(0, i));
            }
            for (var j = 1; j <= range; j++) {
                pts.push(new Phaser.Point(0, -j));
            }
            for (var k = 1; k <= range; k++) {
                pts.push(new Phaser.Point(k, 0));
            }
            for (var l = 1; l <= range; l++) {
                pts.push(new Phaser.Point(-l, 0));
            }
            for (var a = 1; a <= range; a++) {
                pts.push(new Phaser.Point(a, a));
            }
            for (var b = 1; b <= range; b++) {
                pts.push(new Phaser.Point(-b, b));
            }
            for (var c = 1; c <= range; c++) {
                pts.push(new Phaser.Point(c, -c));
            }
            for (var d = 1; d <= range; d++) {
                pts.push(new Phaser.Point(-d, -d));
            }
            return pts;
        };
        return Queen;
    }(InfiniChessClient.Piece));
    InfiniChessClient.Queen = Queen;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //Represents a player. Basically a storage class for pieces, and allowing for good differentiation between the local and remote players
    var Player = (function () {
        function Player(grid, gameState, playerID, uiGroup, boardGroup, pieceGroup, ui) {
            var _this = this;
            this.playerName = ""; //The name of the player
            this.isLocalPlayer = false; //Whether or not this player is the local player
            //Takes information from the piece update packet, and adds a piece if it dosent exist, or updates the piece if it does
            this.UpdatePieceInformationFromNetwork = function (pieceMapIndex, pieceType, xPos, yPos, timestampOfLastCellLanding) {
                if (!_this.pieces.containsKey(pieceMapIndex)) {
                    //we want to add a piece as it dosent exist
                    _this.GivePlayerNewPiece(_this.gameState.game, pieceType, pieceMapIndex, xPos, yPos, timestampOfLastCellLanding);
                    return true;
                }
                else {
                    //just update the piece
                    _this.pieces.getValue(pieceMapIndex).PlaceOnCellAtIndex(xPos, yPos);
                    //this.pieces.getValue(pieceMapIndex).SetTimestampOfLastCellLanding(timestampOfLastCellLanding);
                    return false;
                }
            };
            //Update the player properties on the server
            this.UpdatePlayerPropertiesFromNetwork = function (playerName, money) {
                for (var _i = 0, _a = _this.pieces.values(); _i < _a.length; _i++) {
                    var piece = _a[_i];
                    if (piece != null) {
                        piece.UpdatePieceNameLabel(playerName);
                    }
                }
                if (_this.IsLocalPlayer) {
                    _this.ui.UpdateMoney(money);
                }
            };
            //Completely remove everything about this player from the game, mainly the pieces it owns
            this.PurgeFromGame = function () {
                for (var _i = 0, _a = _this.pieces.values(); _i < _a.length; _i++) {
                    var piece = _a[_i];
                    if (piece != null) {
                        piece.PurgePiece();
                        piece.destroy();
                    }
                }
            };
            //Get the name of the player
            this.GetPlayerName = function () {
                return _this.playerName;
            };
            //Directly set what the player ID is. Dangerous, used just to hook up the local player ID.
            this.SetPlayerNetworkIDDirectly = function (playerID) {
                _this.playerID = playerID;
            };
            //Get the players network ID
            this.GetPlayerNetworkID = function () {
                return _this.playerID;
            };
            //Let this player know directly that it's the local one, from the gamestate
            this.SetIsLocalPlayer = function () {
                _this.isLocalPlayer = true;
            };
            //Whether or not this player is the one that the local client is controlling
            this.IsLocalPlayer = function () {
                return _this.isLocalPlayer;
            };
            //Gives this player a new piece on the grid at the specified index
            this.GivePlayerNewPiece = function (game, piece, serverPieceIndexMapping, pieceXIndex, pieceYIndex, timestampOfLastLandingOnCell) {
                var pieceToAdd;
                if (piece == InfiniChessClient.PieceType.Pawn) {
                    pieceToAdd = new InfiniChessClient.Pawn(game, _this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), _this, _this.pieceGroup, _this.boardGroup, _this.uiGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Knight) {
                    pieceToAdd = new InfiniChessClient.Knight(game, _this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), _this, _this.pieceGroup, _this.boardGroup, _this.uiGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Bishop) {
                    pieceToAdd = new InfiniChessClient.Bishop(game, _this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), _this, _this.pieceGroup, _this.boardGroup, _this.uiGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Rook) {
                    pieceToAdd = new InfiniChessClient.Rook(game, _this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), _this, _this.pieceGroup, _this.boardGroup, _this.uiGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Queen) {
                    pieceToAdd = new InfiniChessClient.Queen(game, _this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), _this, _this.pieceGroup, _this.boardGroup, _this.uiGroup);
                }
                else {
                    throw new Error('Incorrect Piece Type, GivePlayerNewPiece, Player.ts');
                }
                pieceToAdd.SetTimestampOfLastCellLanding(timestampOfLastLandingOnCell);
                _this.pieces.setValue(serverPieceIndexMapping, pieceToAdd);
                return pieceToAdd;
            };
            //In response to a movement command from server, move a specific piece
            this.MakePieceMove = function (pieceIndex, cellToMoveToX, cellToMoveToY, timestampToArriveAt) {
                var cellToMoveTo = _this.grid.GetCellAtIndex(cellToMoveToX, cellToMoveToY);
                if (cellToMoveTo == null) {
                    throw new ReferenceError('Attempting to make move to invalid cell');
                }
                //Check that the piece exists
                if (_this.pieces.containsKey(pieceIndex)) {
                    _this.pieces.getValue(pieceIndex).MakeMove(cellToMoveTo, timestampToArriveAt);
                }
                else {
                    throw new ReferenceError('Attempting to make move on piece that does not exist');
                }
            };
            //Get a specific piece at the provided map index
            this.GetPieceAtIndex = function (pieceIndex) {
                if (_this.pieces.containsKey(pieceIndex)) {
                    return _this.pieces.getValue(pieceIndex);
                }
                console.log('Attemping to get null piece at index : ' + pieceIndex + 'on player ID : ' + _this.GetPlayerNetworkID());
                return null;
            };
            //In response to a message from the server concerning one of this players pieces being taken. Removes the taken piece from the game.
            this.PieceTaken = function (takenPieceIndex) {
                var takenPiece = _this.GetPieceAtIndex(takenPieceIndex);
                takenPiece.RemoveFromGame();
                _this.pieces.remove(takenPieceIndex);
            };
            //In response to a message from the server concerning one of this player pieces taking another. At the moment just places the taking piece on the taking cell
            this.HasTakenPiece = function (takingPieceIndex, takeCell) {
                if (takeCell == null) {
                    throw new Error('Invalid take cell');
                }
                var takingPiece = _this.GetPieceAtIndex(takingPieceIndex);
                takingPiece.PlaceOnCell(takeCell);
            };
            this.gameState = gameState;
            this.grid = grid;
            this.playerID = playerID;
            this.uiGroup = uiGroup;
            this.boardGroup = boardGroup;
            this.pieceGroup = pieceGroup;
            this.ui = ui;
            this.pieces = new InfiniChessClient.Dictionary();
        }
        return Player;
    }());
    InfiniChessClient.Player = Player;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A highlight that is placed on the cell when deciding to move a piece. Can then be clicked on for selection.
    var CellHighlight = (function (_super) {
        __extends(CellHighlight, _super);
        function CellHighlight(_game, highlightPiece) {
            var _this = this;
            _super.call(this, _game, 0, 0);
            this.PlaceOnCell = function (highlightCell) {
                _this.highlightingCell = highlightCell;
                _this.x = _this.highlightingCell.GetCenter().x;
                _this.y = _this.highlightingCell.GetCenter().y;
            };
            //The player has just clicked this highlight, and this cell was selected
            this.SelectCell = function () {
                _this.highlightingPiece.SelectionResponse(_this.highlightingCell);
            };
            this.loadTexture(InfiniChessClient.Assets.CellHighlight.assetKey);
            this.anchor = new Phaser.Point(0.5, 0.5);
            this.highlightingPiece = highlightPiece;
            this.inputEnabled = true;
            this.events.onInputDown.add(this.SelectCell, this);
        }
        return CellHighlight;
    }(Phaser.Sprite));
    InfiniChessClient.CellHighlight = CellHighlight;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A single cell on the grid, can hold a reference to a track.
    var Cell = (function () {
        function Cell(_game, _parentGrid, _gridIndex, _worldPosition) {
            var _this = this;
            this.sittingPiece = null; //Whatever piece is on the cell, dont use this for game logic. Null if its nothing.
            this.NotifyPieceOnCell = function (piece) {
                _this.sittingPiece = piece;
            };
            this.NotifyCellIsNowEmpty = function () {
                _this.sittingPiece = null;
            };
            this.GetPieceOnCell = function () {
                return _this.sittingPiece;
            };
            this.GetCenter = function () {
                var center = new Phaser.Point(_this.position.x, _this.position.y);
                center.x += InfiniChessClient.Grid.CELLXPIXELS / 2;
                center.y += InfiniChessClient.Grid.CELLYPIXELS / 2;
                return center;
            };
            this.GetNorthCell = function () {
                return _this.parentGrid.GetCellAtIndex(_this.gridIndex.x, _this.gridIndex.y - 1);
            };
            this.GetEastCell = function () {
                return _this.parentGrid.GetCellAtIndex(_this.gridIndex.x + 1, _this.gridIndex.y);
            };
            this.GetSouthCell = function () {
                return _this.parentGrid.GetCellAtIndex(_this.gridIndex.x, _this.gridIndex.y + 1);
            };
            this.GetWestCell = function () {
                return _this.parentGrid.GetCellAtIndex(_this.gridIndex.x - 1, _this.gridIndex.y);
            };
            this.GetGridIndex = function () {
                return _this.gridIndex;
            };
            //Returns the position in the center of one edge, as defined by direction (0-3, N-W)
            this.GetEdgePoint = function (direction) {
                direction = Math.floor(direction);
                if (direction < 0) {
                    direction = 0;
                }
                if (direction > 3) {
                    direction = 3;
                }
                if (direction == 0) {
                    return new Phaser.Point(_this.position.x + InfiniChessClient.Grid.CELLXPIXELS / 2, _this.position.y);
                }
                else if (direction == 1) {
                    return new Phaser.Point(_this.position.x + InfiniChessClient.Grid.CELLXPIXELS, _this.position.y + InfiniChessClient.Grid.CELLYPIXELS / 2);
                }
                else if (direction == 2) {
                    return new Phaser.Point(_this.position.x + InfiniChessClient.Grid.CELLXPIXELS / 2, _this.position.y + InfiniChessClient.Grid.CELLYPIXELS);
                }
                else if (direction == 3) {
                    return new Phaser.Point(_this.position.x, _this.position.y + InfiniChessClient.Grid.CELLYPIXELS / 2);
                }
                else {
                    throw new Error("Incorrect Direction. GetEdgePoint(), Cell.ts");
                }
            };
            this.parentGrid = _parentGrid;
            this.gridIndex = _gridIndex;
            this.position = _worldPosition;
            this.game = _game;
            this.sittingPiece = null;
        }
        return Cell;
    }());
    InfiniChessClient.Cell = Cell;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //The square grid, contains an array of cells and accesor methods for um ... accesing them.
    var Grid = (function () {
        function Grid(game, state, xGridWidth, yGridWidth) {
            var _this = this;
            //Called from constructor, so we can bind 'this' reference properly
            this.PopulateGridWithCells = function (game, xGridWidth, yGridWidth) {
                _this.cells = [];
                //The provided x and y numbers are rounded so they're ints, as javascript dosen't "do" ints, the high maintenance bitch.
                xGridWidth = Math.round(xGridWidth);
                yGridWidth = Math.round(yGridWidth);
                //Shove the cells in, creating Y-arrays as we go. Cells are given reference to this, and we use the CELLX/YPIXEL constants to hook up cell positions
                for (var i = 0; i < xGridWidth; i++) {
                    _this.cells[i] = [];
                    for (var j = 0; j < yGridWidth; j++) {
                        var newCell = new InfiniChessClient.Cell(game, _this, new Phaser.Point(i, j), new Phaser.Point(i * Grid.CELLXPIXELS, j * Grid.CELLYPIXELS));
                        _this.cells[i].push(newCell);
                    }
                }
            };
            //Directly access cell at specified index
            this.GetCellAtIndex = function (indexX, indexY) {
                //Sanity, also stops move highlights going off the board
                if ((indexX < 0) || (indexY < 0)) {
                    return null;
                }
                //Round numbers to ints
                indexX = Math.floor(indexX);
                indexY = Math.floor(indexY);
                if (_this.cells.length > indexX) {
                    if (_this.cells[indexX].length > indexY) {
                        return _this.cells[indexX][indexY];
                    }
                }
                //If we got here, we tried to access out of array bounds
                throw new Error('Attempted to access out of bounds cell');
            };
            //Access the cell under a world position, most useful for getting a cell at the mouse position I'd imagine, although if theres camera translation screen->worldspace will need to be done.
            this.GetCellAtWorldPoint = function (worldPosition) {
                //Derive x/y indices from world positions
                var xIndex = worldPosition.x / Grid.CELLXPIXELS;
                var yIndex = worldPosition.y / Grid.CELLYPIXELS;
                //Round the indices down.
                xIndex = Math.floor(xIndex);
                yIndex = Math.floor(yIndex);
                if (_this.cells.length > xIndex) {
                    if (_this.cells[xIndex].length > yIndex) {
                        return _this.cells[xIndex][yIndex];
                    }
                }
                //If we got here, we tried to access out of array bounds, and should error.
                throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtWorldPoint()");
            };
            //Gets how many tiles the grid has horizontally
            this.GetGridWidth = function () {
                return _this.cells.length;
            };
            //Gets how many tiles the grid has vertically
            this.GetGridHeight = function () {
                if (_this.cells.length == 0) {
                    return 0;
                }
                return _this.cells[0].length;
            };
            this.PopulateGridWithCells(game, xGridWidth, yGridWidth);
            this.gridImage = state.add.tileSprite(0, 0, xGridWidth * Grid.CELLXPIXELS, yGridWidth * Grid.CELLYPIXELS, InfiniChessClient.Assets.ChessGrid.assetKey);
            //Set the background logic disabled for performance
            this.gridImage.body = null;
        }
        //The width/height of the cells, in pixels (hopefully).
        Grid.CELLXPIXELS = 94;
        Grid.CELLYPIXELS = 50;
        return Grid;
    }());
    InfiniChessClient.Grid = Grid;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //A wrapper around the camera controls, so we can limit them if we want, and have controls
    var Camera = (function () {
        //Pass in start positions
        function Camera(game, startX, startY, stageWidth, stageHeight) {
            var _this = this;
            this.camMoveSpeed = 0.3; //How fast the camera moves aboot'
            this.isScrolling = false; //Whether or not the camera is moving
            this.Update = function (deltaTime) {
                _this.isScrolling = false;
                //Move camera according to keyboard controls
                //Up, W and UpArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.W)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.UP))) {
                    _this.gameRef.camera.y -= _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                //Down, S and DownArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.S)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.DOWN))) {
                    _this.gameRef.camera.y += _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                //Up, A and LeftArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.A)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.LEFT))) {
                    _this.gameRef.camera.x -= _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                //Up, D and RightArrow
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.D)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.RIGHT))) {
                    _this.gameRef.camera.x += _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
            };
            this.ScreenToWorldSpace = function (screenPos) {
                var worldPos = new Phaser.Point(screenPos.x, screenPos.y);
                worldPos.x += _this.gameRef.camera.x;
                worldPos.y += _this.gameRef.camera.y;
                return worldPos;
            };
            this.IsScrolling = function () {
                return _this.isScrolling;
            };
            game.camera.setPosition(startX * 2, startY * 2);
            game.camera.bounds = new Phaser.Rectangle(startX * 2, startY * 2, stageWidth, stageHeight);
            this.gameRef = game;
        }
        return Camera;
    }());
    InfiniChessClient.Camera = Camera;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //Display the UI for the local player
    var UI = (function () {
        function UI(game, localPlayer, uiGroup) {
            var _this = this;
            this.UpdateMoney = function (newMoney) {
                _this.moneyLabel.text = "$" + Math.round(newMoney).toString();
            };
            this.game = game;
            this.localPlayer = localPlayer;
            this.uiGroup = uiGroup;
            var style = { font: "32px Arial", fill: "#fff", align: "center" };
            this.moneyLabel = new Phaser.Text(game, 50, 35, "$0", style);
            this.moneyLabel.stroke = '#000000';
            this.moneyLabel.strokeThickness = 4;
            this.moneyLabel.anchor.set(0.5);
            uiGroup.add(this.moneyLabel);
        }
        return UI;
    }());
    InfiniChessClient.UI = UI;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            _super.apply(this, arguments);
        }
        BootState.prototype.preload = function () {
            InfiniChessClient.Assets.LoadImage(this, InfiniChessClient.Assets.LoadingBar);
        };
        BootState.prototype.create = function () {
            //Disable the right click context menu, cause we want to use right click for rotating
            this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); };
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = false;
            if (this.game.device.desktop) {
            }
            else {
            }
            this.game.state.start('PreloaderState', true, false);
        };
        return BootState;
    }(Phaser.State));
    InfiniChessClient.BootState = BootState;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    //Entry point, called from app.ts. Just for state setup
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, window.innerWidth, window.innerHeight, Phaser.AUTO, 'content', null);
            this.state.add('BootState', InfiniChessClient.BootState, false);
            this.state.add('PreloaderState', InfiniChessClient.PreloaderState, false);
            this.state.add('ConnectionState', InfiniChessClient.ConnectionState, false);
            this.state.add('GameState', InfiniChessClient.GameState, false);
            this.state.start('BootState');
        }
        return Game;
    }(Phaser.Game));
    InfiniChessClient.Game = Game;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    var ConnectionState = (function (_super) {
        __extends(ConnectionState, _super);
        function ConnectionState() {
            var _this = this;
            _super.apply(this, arguments);
            this.JoinLocalServer = function () {
                _this.connect(true);
            };
            this.JoinRemoteServer = function () {
                _this.connect(false);
            };
            this.OnKeyboardKeyDown = function () {
                _this.playerName += _this.game.input.keyboard.lastChar;
                _this.nameText.text = _this.playerName;
            };
            this.DeletePressed = function () {
                if (_this.playerName.length > 0) {
                    _this.playerName = _this.playerName.slice(0, _this.playerName.length - 1);
                    _this.nameText.text = _this.playerName;
                }
            };
            this.connect = function (connectToLocal) {
                if (connectToLocal === void 0) { connectToLocal = false; }
                _this.game.state.start('GameState', true, false, _this.playerName, connectToLocal);
            };
        }
        ConnectionState.prototype.create = function () {
            this.game.input.enabled = true;
            var style = { font: "65px Arial", fill: "#ffffff", align: "center" };
            this.playerName = "Elliot";
            this.nameText = new Phaser.Text(this.game, 50, 150, this.playerName, style);
            this.game.add.existing(this.nameText);
            this.instructionMessageString = "Type yo' name.";
            this.instructionMessageText = new Phaser.Text(this.game, 50, 50, this.instructionMessageString, style);
            this.game.add.existing(this.instructionMessageText);
            this.game.input.keyboard.addCallbacks(this.game, null, null, this.OnKeyboardKeyDown);
            this.deleteKey = this.game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
            this.deleteKey.onDown.add(this.DeletePressed, this);
            //Setup the text that displays the owning players name
            var style = { font: "32px Arial", fill: "#000", align: "center" };
            this.game.add.button(100, 500, InfiniChessClient.Assets.Button.assetKey, this.JoinLocalServer, this, 1, 0, 2, 0);
            this.game.add.text(145, 540, "Join Local Server", style);
            this.game.add.button(600, 500, InfiniChessClient.Assets.Button.assetKey, this.JoinRemoteServer, this, 1, 0, 2, 0);
            this.game.add.text(635, 540, "Join Remote Server", style);
        };
        ConnectionState.prototype.update = function () {
        };
        return ConnectionState;
    }(Phaser.State));
    InfiniChessClient.ConnectionState = ConnectionState;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    var PreloaderState = (function (_super) {
        __extends(PreloaderState, _super);
        function PreloaderState() {
            _super.apply(this, arguments);
        }
        PreloaderState.prototype.preload = function () {
            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, InfiniChessClient.Assets.LoadingBar.assetKey);
            this.load.setPreloadSprite(this.preloadBar);
            //  Load game assets here, like this : this.load.image(TrainBox.AssetPaths.Asset.AssetKey, TrainBox.AssetPaths.Asset.AssetKey);
            InfiniChessClient.Assets.LoadAllAssets(this);
        };
        PreloaderState.prototype.create = function () {
            //Loading bar
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);
        };
        PreloaderState.prototype.startGame = function () {
            this.game.state.start('ConnectionState', true, false);
        };
        return PreloaderState;
    }(Phaser.State));
    InfiniChessClient.PreloaderState = PreloaderState;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    var GameState = (function (_super) {
        __extends(GameState, _super);
        function GameState() {
            var _this = this;
            _super.apply(this, arguments);
            this.gamePaused = false;
            this.connectToLocal = false;
            //Send the player properties to the server so it knows its name n' shit.
            this.InitialisePlayerPropertiesOnServer = function (socket) {
                console.log('Sending player properties to server');
                socket.emit("setserverplayerproperties", Date.now(), socket.id, _this.localPlayerName);
            };
            //Recieve player properties about a player in the game from server, and set it. Properties is like its metadata, name, etc
            this.RecievePlayerProperties = function (timeStamp, playerID, playerName, money) {
                console.log('Recieving Player Properties packet about player ID : ' + playerID + ' and name : ' + playerName);
                if (_this.localPlayer.GetPlayerNetworkID() == playerID) {
                    _this.localPlayer.UpdatePlayerPropertiesFromNetwork(playerName, money);
                }
                if (!_this.remotePlayers.containsKey(playerID)) {
                    console.log('Attempting to set player properties for player that does not exist on client, ID : ' + playerID + ' and name : ' + playerName);
                    return;
                }
                _this.remotePlayers.getValue(playerID).UpdatePlayerPropertiesFromNetwork(playerName, null); //Only give money to the local player as it's only used to update the UI
            };
            //The server pingback, makes the piece that is spawned for the player when they first connect
            this.ResponseToConnectionToServer = function (timeStamp, playerID, pieceType, pieceMapIndex, xSpawnIndex, ySpawnIndex) {
                console.log('Recieveing Connection response pingback, adding initial piece at X : ' + xSpawnIndex + ' Y : ' + ySpawnIndex + ' ID : ' + playerID);
                //Let the local player know what its server ID is
                _this.localPlayer.SetPlayerNetworkIDDirectly(playerID);
                _this.localPlayer.SetIsLocalPlayer();
                console.log('Setting local player ID : ' + playerID);
                //Add the initial spawn piece to the local player
                _this.localPlayer.GivePlayerNewPiece(_this.game, pieceType, pieceMapIndex, xSpawnIndex, ySpawnIndex, Date.now());
                _this.InitialisePlayerPropertiesOnServer(GameState.socket);
                _this.SortPieceZOrder();
            };
            //A packet about a specific piece. This can be though more of a "Set Piece Directly" packet. It also handles adding players if the piece packet dosent have an associcated player. This is a bit clumsy
            this.RecievePieceInformationPacket = function (timestamp, playerID, pieceMapIndex, pieceType, xPos, yPos, timestampOfLastCellLanding) {
                console.log('Recieving Piece information from player ID : ' + playerID + ' about piecetype ' + pieceType + ' at position : X : ' + xPos + ' Y : ' + yPos);
                _this.AddNewPlayerIfNeccesary(playerID);
                var player = _this.GetLocalOrRemotePlayerByID(playerID);
                if (player != null) {
                    player.UpdatePieceInformationFromNetwork(pieceMapIndex, pieceType, xPos, yPos, timestampOfLastCellLanding);
                }
                _this.SortPieceZOrder();
            };
            //A packet containing a movement command for a specific piece
            this.RecieveMovementCommand = function (timestamp, playerID, pieceMapIndex, pieceType, moveStartIndexX, moveStartIndexY, moveTargetIndexX, moveTargetIndexY, timestampToArriveAt) {
                console.log('Recieving movement command information from player ID : ' + playerID + ' about piecetype ' + pieceType + ' to move to : X : ' + moveTargetIndexX + ' Y : ' + moveTargetIndexY + ' should take : ' + (timestampToArriveAt - Date.now()).toString() + 'ms to arrive.');
                var player = _this.GetLocalOrRemotePlayerByID(playerID);
                if (player != null) {
                    player.MakePieceMove(pieceMapIndex, moveTargetIndexX, moveTargetIndexY, timestampToArriveAt);
                }
            };
            //A packet containing a notification that a piece has been taken
            this.RecievePieceTakenNotification = function (timestamp, takenPlayerID, takenPieceMapIndex, takerPlayerID, takerPieceMapIndex, takenSquareIndexX, takenSquareIndexY) {
                console.log('Recieving Taken Notification. PlayerID : ' + takerPlayerID + ' taking piece of PlayerID : ' + takenPlayerID + '.');
                var takingPlayer = _this.GetLocalOrRemotePlayerByID(takerPlayerID);
                var takenPlayer = _this.GetLocalOrRemotePlayerByID(takenPlayerID);
                takingPlayer.HasTakenPiece(takerPieceMapIndex, _this.grid.GetCellAtIndex(takenSquareIndexX, takenSquareIndexY));
                takenPlayer.PieceTaken(takenPieceMapIndex);
            };
            //The local and remote players are seperated, this function allows us to treat them as the same. Returns null if player dosent exist
            this.GetLocalOrRemotePlayerByID = function (playerID) {
                if (_this.remotePlayers.containsKey(playerID)) {
                    return _this.remotePlayers.getValue(playerID);
                }
                else if (_this.localPlayer.GetPlayerNetworkID() == playerID) {
                    return _this.localPlayer;
                }
                throw new Error('Attempting to get invalid player by ID');
            };
            //Adds a new player if the server has told us about it(or told us about a piece it has, can be used either way. Returns true if a new player is added
            this.AddNewPlayerIfNeccesary = function (playerNetworkID) {
                //Assuming we don't already know about this player, add it to the known players list
                if ((!_this.remotePlayers.containsKey(playerNetworkID)) && (_this.localPlayer.GetPlayerNetworkID() != playerNetworkID)) {
                    console.log('Adding New Player, ID : ' + playerNetworkID);
                    _this.remotePlayers.setValue(playerNetworkID, new InfiniChessClient.Player(_this.grid, _this, playerNetworkID, _this.uiGroup, _this.boardGroup, _this.pieceGroup, _this.ui));
                    return true;
                }
                return false;
            };
            //Receive a packet about a player disconnecting, and remove the client side representation of that player
            this.RecieveDisconnectionNotification = function (timestamp, playerNetworkID) {
                console.log('Recieving disconnect notification of ID : ' + playerNetworkID);
                //If the message is about this local player, treat it as though we've been kicked.
                if (_this.localPlayer.GetPlayerNetworkID() == playerNetworkID) {
                    console.log('You have been removed from server');
                    _this.game.state.start('ConnectionState', true, true);
                    return true;
                }
                //If we're getting a disconnection message about a remote player, remove it from errything.
                if (_this.remotePlayers.containsKey(playerNetworkID)) {
                    console.log('Removing Player, ID : ' + playerNetworkID);
                    _this.remotePlayers.getValue(playerNetworkID).PurgeFromGame();
                    _this.remotePlayers.remove(playerNetworkID);
                    return true;
                }
                return false;
            };
            //Sorts all the pieces in the game so they're rendered in the right order
            this.SortPieceZOrder = function () {
                //Scan through the grid sorting by the y axis
                for (var i = 0; i < _this.grid.GetGridWidth(); i++) {
                    for (var j = 0; j < _this.grid.GetGridHeight(); j++) {
                        var pieceOnCell = _this.grid.GetCellAtIndex(i, j).GetPieceOnCell();
                        if (pieceOnCell != null) {
                            pieceOnCell.bringToTop();
                            pieceOnCell.BringNameLableToTop();
                            pieceOnCell.BringCooldownLabelToTop();
                        }
                    }
                }
            };
            this.PausedGame = function () {
                _this.gamePaused = true;
            };
            this.UnPausedGame = function () {
                _this.gamePaused = false;
                _this.game.time.elapsed = 0.166;
            };
        }
        GameState.prototype.init = function (playerName, connectToLocal) {
            this.localPlayerName = playerName;
            this.connectToLocal = connectToLocal;
        };
        //Initialise the main game state
        GameState.prototype.create = function () {
            var gridWidth = 10;
            var gridHeight = 10;
            this.cam = new InfiniChessClient.Camera(this.game, -gridWidth * (InfiniChessClient.Grid.CELLXPIXELS / 8), -gridHeight * (InfiniChessClient.Grid.CELLYPIXELS / 8), window.innerWidth, window.innerHeight);
            this.grid = new InfiniChessClient.Grid(this.game, this, gridWidth, gridHeight);
            this.mousePos = new Phaser.Point(0, 0);
            this.boardGroup = new Phaser.Group(this.game);
            this.pieceGroup = new Phaser.Group(this.game);
            this.uiGroup = new Phaser.Group(this.game);
            this.uiGroup.fixedToCamera = true;
            this.add.existing(this.boardGroup);
            this.add.existing(this.pieceGroup);
            this.add.existing(this.uiGroup);
            this.game.onPause.add(this.PausedGame, this);
            this.game.onResume.add(this.UnPausedGame, this);
            this.ui = new InfiniChessClient.UI(this.game, this.localPlayer, this.uiGroup);
            this.localPlayer = new InfiniChessClient.Player(this.grid, this, 'local', this.uiGroup, this.boardGroup, this.pieceGroup, this.ui);
            this.localPlayer.UpdatePlayerPropertiesFromNetwork(this.localPlayerName, 0); //Pretend we got a packet back from the network concerning outselves with our name
            this.remotePlayers = new InfiniChessClient.Dictionary();
            if (this.connectToLocal) {
                GameState.socket = io.connect(GameState.SERVER_ADDRESS_LOCAL);
            }
            else {
                GameState.socket = io.connect(GameState.SERVER_ADDRESS_REMOTE);
            }
            GameState.socket.on('connectionresponse', this.ResponseToConnectionToServer); //Register listening to server connection response, which should come pretty immediately.
            GameState.socket.on('piecestate', this.RecievePieceInformationPacket); //Register listening for information on remote pieces.
            GameState.socket.on('setclientplayerproperties', this.RecievePlayerProperties); //Register listening for player properties.
            GameState.socket.on('disconnectnotify', this.RecieveDisconnectionNotification); //Register listening for other player disconnects.
            GameState.socket.on('movementcommand', this.RecieveMovementCommand); //Register listening to piece movement commands from the server.
            GameState.socket.on('piecetaken', this.RecievePieceTakenNotification); //Register listening to notifications about pieces 
            this.game.stage.disableVisibilityChange = true; //Important, game needs to keep responding to packet updates when not in focus
            this.input.enabled = true;
        };
        GameState.prototype.update = function () {
            if (this.gamePaused == false) {
                this.mousePos.x = this.input.x;
                this.mousePos.y = this.input.y;
                this.cam.Update(this.time.elapsedMS);
            }
        };
        GameState.SERVER_ADDRESS_LOCAL = 'http://localhost:3000';
        GameState.SERVER_ADDRESS_REMOTE = 'http://52.17.49.151:3000';
        return GameState;
    }(Phaser.State));
    InfiniChessClient.GameState = GameState;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    /**
     * Returns the position of the first occurrence of the specified item
     * within the specified array.4
     * @param {*} array the array in which to search the element.
     * @param {Object} item the element to search.
     * @param {function(Object,Object):boolean=} equalsFunction optional function used to
     * check equality between 2 elements.
     * @return {number} the position of the first occurrence of the specified element
     * within the specified array, or -1 if not found.
     */
    function indexOf(array, item, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        var length = array.length;
        for (var i = 0; i < length; i++) {
            if (equals(array[i], item)) {
                return i;
            }
        }
        return -1;
    }
    InfiniChessClient.indexOf = indexOf;
    /**
     * Returns the position of the last occurrence of the specified element
     * within the specified array.
     * @param {*} array the array in which to search the element.
     * @param {Object} item the element to search.
     * @param {function(Object,Object):boolean=} equalsFunction optional function used to
     * check equality between 2 elements.
     * @return {number} the position of the last occurrence of the specified element
     * within the specified array or -1 if not found.
     */
    function lastIndexOf(array, item, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        var length = array.length;
        for (var i = length - 1; i >= 0; i--) {
            if (equals(array[i], item)) {
                return i;
            }
        }
        return -1;
    }
    InfiniChessClient.lastIndexOf = lastIndexOf;
    /**
     * Returns true if the specified array contains the specified element.
     * @param {*} array the array in which to search the element.
     * @param {Object} item the element to search.
     * @param {function(Object,Object):boolean=} equalsFunction optional function to
     * check equality between 2 elements.
     * @return {boolean} true if the specified array contains the specified element.
     */
    function contains(array, item, equalsFunction) {
        return indexOf(array, item, equalsFunction) >= 0;
    }
    InfiniChessClient.contains = contains;
    /**
     * Removes the first ocurrence of the specified element from the specified array.
     * @param {*} array the array in which to search element.
     * @param {Object} item the element to search.
     * @param {function(Object,Object):boolean=} equalsFunction optional function to
     * check equality between 2 elements.
     * @return {boolean} true if the array changed after this call.
     */
    function remove(array, item, equalsFunction) {
        var index = indexOf(array, item, equalsFunction);
        if (index < 0) {
            return false;
        }
        array.splice(index, 1);
        return true;
    }
    InfiniChessClient.remove = remove;
    /**
     * Returns the number of elements in the specified array equal
     * to the specified object.
     * @param {Array} array the array in which to determine the frequency of the element.
     * @param {Object} item the element whose frequency is to be determined.
     * @param {function(Object,Object):boolean=} equalsFunction optional function used to
     * check equality between 2 elements.
     * @return {number} the number of elements in the specified array
     * equal to the specified object.
     */
    function frequency(array, item, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        var length = array.length;
        var freq = 0;
        for (var i = 0; i < length; i++) {
            if (equals(array[i], item)) {
                freq++;
            }
        }
        return freq;
    }
    InfiniChessClient.frequency = frequency;
    /**
     * Returns true if the two specified arrays are equal to one another.
     * Two arrays are considered equal if both arrays contain the same number
     * of elements, and all corresponding pairs of elements in the two
     * arrays are equal and are in the same order.
     * @param {Array} array1 one array to be tested for equality.
     * @param {Array} array2 the other array to be tested for equality.
     * @param {function(Object,Object):boolean=} equalsFunction optional function used to
     * check equality between elemements in the arrays.
     * @return {boolean} true if the two arrays are equal
     */
    function equals(array1, array2, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        if (array1.length !== array2.length) {
            return false;
        }
        var length = array1.length;
        for (var i = 0; i < length; i++) {
            if (!equals(array1[i], array2[i])) {
                return false;
            }
        }
        return true;
    }
    InfiniChessClient.equals = equals;
    /**
     * Returns shallow a copy of the specified array.
     * @param {*} array the array to copy.
     * @return {Array} a copy of the specified array
     */
    function copy(array) {
        return array.concat();
    }
    InfiniChessClient.copy = copy;
    /**
     * Swaps the elements at the specified positions in the specified array.
     * @param {Array} array The array in which to swap elements.
     * @param {number} i the index of one element to be swapped.
     * @param {number} j the index of the other element to be swapped.
     * @return {boolean} true if the array is defined and the indexes are valid.
     */
    function swap(array, i, j) {
        if (i < 0 || i >= array.length || j < 0 || j >= array.length) {
            return false;
        }
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        return true;
    }
    InfiniChessClient.swap = swap;
    function toString(array) {
        return '[' + array.toString() + ']';
    }
    InfiniChessClient.toString = toString;
    /**
     * Executes the provided function once for each element present in this array
     * starting from index 0 to length - 1.
     * @param {Array} array The array in which to iterate.
     * @param {function(Object):*} callback function to execute, it is
     * invoked with one argument: the element value, to break the iteration you can
     * optionally return false.
     */
    function forEach(array, callback) {
        for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
            var ele = array_1[_i];
            if (callback(ele) === false) {
                return;
            }
        }
    }
    InfiniChessClient.forEach = forEach;
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    var Dictionary = (function () {
        /**
         * Creates an empty dictionary.
         * @class <p>Dictionaries map keys to values; each key can map to at most one value.
         * This implementation accepts any kind of objects as keys.</p>
         *
         * <p>If the keys are custom objects a function which converts keys to unique
         * strings must be provided. Example:</p>
         * <pre>
         * function petToString(pet) {
         *  return pet.name;
         * }
         * </pre>
         * @constructor
         * @param {function(Object):string=} toStrFunction optional function used
         * to convert keys to strings. If the keys aren't strings or if toString()
         * is not appropriate, a custom function which receives a key and returns a
         * unique string must be provided.
         */
        function Dictionary(toStrFunction) {
            this.table = {};
            this.nElements = 0;
            this.toStr = toStrFunction || InfiniChessClient.defaultToString;
        }
        /**
         * Returns the value to which this dictionary maps the specified key.
         * Returns undefined if this dictionary contains no mapping for this key.
         * @param {Object} key key whose associated value is to be returned.
         * @return {*} the value to which this dictionary maps the specified key or
         * undefined if the map contains no mapping for this key.
         */
        Dictionary.prototype.getValue = function (key) {
            var pair = this.table['$' + this.toStr(key)];
            if (InfiniChessClient.isUndefined(pair)) {
                return undefined;
            }
            return pair.value;
        };
        /**
         * Associates the specified value with the specified key in this dictionary.
         * If the dictionary previously contained a mapping for this key, the old
         * value is replaced by the specified value.
         * @param {Object} key key with which the specified value is to be
         * associated.
         * @param {Object} value value to be associated with the specified key.
         * @return {*} previous value associated with the specified key, or undefined if
         * there was no mapping for the key or if the key/value are undefined.
         */
        Dictionary.prototype.setValue = function (key, value) {
            if (InfiniChessClient.isUndefined(key) || InfiniChessClient.isUndefined(value)) {
                return undefined;
            }
            var ret;
            var k = '$' + this.toStr(key);
            var previousElement = this.table[k];
            if (InfiniChessClient.isUndefined(previousElement)) {
                this.nElements++;
                ret = undefined;
            }
            else {
                ret = previousElement.value;
            }
            this.table[k] = {
                key: key,
                value: value
            };
            return ret;
        };
        /**
         * Removes the mapping for this key from this dictionary if it is present.
         * @param {Object} key key whose mapping is to be removed from the
         * dictionary.
         * @return {*} previous value associated with specified key, or undefined if
         * there was no mapping for key.
         */
        Dictionary.prototype.remove = function (key) {
            var k = '$' + this.toStr(key);
            var previousElement = this.table[k];
            if (!InfiniChessClient.isUndefined(previousElement)) {
                delete this.table[k];
                this.nElements--;
                return previousElement.value;
            }
            return undefined;
        };
        /**
         * Returns an array containing all of the keys in this dictionary.
         * @return {Array} an array containing all of the keys in this dictionary.
         */
        Dictionary.prototype.keys = function () {
            var array = [];
            for (var name_1 in this.table) {
                if (InfiniChessClient.has(this.table, name_1)) {
                    var pair = this.table[name_1];
                    array.push(pair.key);
                }
            }
            return array;
        };
        /**
         * Returns an array containing all of the values in this dictionary.
         * @return {Array} an array containing all of the values in this dictionary.
         */
        Dictionary.prototype.values = function () {
            var array = [];
            for (var name_2 in this.table) {
                if (InfiniChessClient.has(this.table, name_2)) {
                    var pair = this.table[name_2];
                    array.push(pair.value);
                }
            }
            return array;
        };
        /**
        * Executes the provided function once for each key-value pair
        * present in this dictionary.
        * @param {function(Object,Object):*} callback function to execute, it is
        * invoked with two arguments: key and value. To break the iteration you can
        * optionally return false.
        */
        Dictionary.prototype.forEach = function (callback) {
            for (var name_3 in this.table) {
                if (InfiniChessClient.has(this.table, name_3)) {
                    var pair = this.table[name_3];
                    var ret = callback(pair.key, pair.value);
                    if (ret === false) {
                        return;
                    }
                }
            }
        };
        /**
         * Returns true if this dictionary contains a mapping for the specified key.
         * @param {Object} key key whose presence in this dictionary is to be
         * tested.
         * @return {boolean} true if this dictionary contains a mapping for the
         * specified key.
         */
        Dictionary.prototype.containsKey = function (key) {
            return !InfiniChessClient.isUndefined(this.getValue(key));
        };
        /**
        * Removes all mappings from this dictionary.
        * @this {collections.Dictionary}
        */
        Dictionary.prototype.clear = function () {
            this.table = {};
            this.nElements = 0;
        };
        /**
         * Returns the number of keys in this dictionary.
         * @return {number} the number of key-value mappings in this dictionary.
         */
        Dictionary.prototype.size = function () {
            return this.nElements;
        };
        /**
         * Returns true if this dictionary contains no mappings.
         * @return {boolean} true if this dictionary contains no mappings.
         */
        Dictionary.prototype.isEmpty = function () {
            return this.nElements <= 0;
        };
        Dictionary.prototype.toString = function () {
            var toret = "{";
            this.forEach(function (k, v) {
                toret += "\n\t" + k + " : " + v;
            });
            return toret + "\n}";
        };
        return Dictionary;
    }());
    InfiniChessClient.Dictionary = Dictionary; // End of dictionary
})(InfiniChessClient || (InfiniChessClient = {}));
var InfiniChessClient;
(function (InfiniChessClient) {
    var _hasOwnProperty = Object.prototype.hasOwnProperty;
    InfiniChessClient.has = function (obj, prop) {
        return _hasOwnProperty.call(obj, prop);
    };
    /**
     * Default function to compare element order.
     * @function
     */
    function defaultCompare(a, b) {
        if (a < b) {
            return -1;
        }
        else if (a === b) {
            return 0;
        }
        else {
            return 1;
        }
    }
    InfiniChessClient.defaultCompare = defaultCompare;
    /**
     * Default function to test equality.
     * @function
     */
    function defaultEquals(a, b) {
        return a === b;
    }
    InfiniChessClient.defaultEquals = defaultEquals;
    /**
     * Default function to convert an object to a string.
     * @function
     */
    function defaultToString(item) {
        if (item === null) {
            return 'COLLECTION_NULL';
        }
        else if (isUndefined(item)) {
            return 'COLLECTION_UNDEFINED';
        }
        else if (isString(item)) {
            return '$s' + item;
        }
        else {
            return '$o' + item.toString();
        }
    }
    InfiniChessClient.defaultToString = defaultToString;
    /**
    * Joins all the properies of the object using the provided join string
    */
    function makeString(item, join) {
        if (join === void 0) { join = ","; }
        if (item === null) {
            return 'COLLECTION_NULL';
        }
        else if (isUndefined(item)) {
            return 'COLLECTION_UNDEFINED';
        }
        else if (isString(item)) {
            return item.toString();
        }
        else {
            var toret = "{";
            var first = true;
            for (var prop in item) {
                if (InfiniChessClient.has(item, prop)) {
                    if (first)
                        first = false;
                    else
                        toret = toret + join;
                    toret = toret + prop + ":" + item[prop];
                }
            }
            return toret + "}";
        }
    }
    InfiniChessClient.makeString = makeString;
    /**
     * Checks if the given argument is a function.
     * @function
     */
    function isFunction(func) {
        return (typeof func) === 'function';
    }
    InfiniChessClient.isFunction = isFunction;
    /**
     * Checks if the given argument is undefined.
     * @function
     */
    function isUndefined(obj) {
        return (typeof obj) === 'undefined';
    }
    InfiniChessClient.isUndefined = isUndefined;
    /**
     * Checks if the given argument is a string.
     * @function
     */
    function isString(obj) {
        return Object.prototype.toString.call(obj) === '[object String]';
    }
    InfiniChessClient.isString = isString;
    /**
     * Reverses a compare function.
     * @function
     */
    function reverseCompareFunction(compareFunction) {
        if (!isFunction(compareFunction)) {
            return function (a, b) {
                if (a < b) {
                    return 1;
                }
                else if (a === b) {
                    return 0;
                }
                else {
                    return -1;
                }
            };
        }
        else {
            return function (d, v) {
                return compareFunction(d, v) * -1;
            };
        }
    }
    InfiniChessClient.reverseCompareFunction = reverseCompareFunction;
    /**
     * Returns an equal function given a compare function.
     * @function
     */
    function compareToEquals(compareFunction) {
        return function (a, b) {
            return compareFunction(a, b) === 0;
        };
    }
    InfiniChessClient.compareToEquals = compareToEquals;
})(InfiniChessClient || (InfiniChessClient = {}));
//# sourceMappingURL=game.js.map