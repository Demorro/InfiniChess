var InfiniChessClient;
(function (InfiniChessClient) {
    var IdentifierPathPair = (function () {
        function IdentifierPathPair(_assetKey, _path) {
            this.assetKey = _assetKey;
            this.path = _path;
        }
        return IdentifierPathPair;
    }());
    InfiniChessClient.IdentifierPathPair = IdentifierPathPair;
    var Assets = (function () {
        function Assets() {
        }
        Assets.LoadAllAssets = function (state) {
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.ChessGrid);
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.ChessPieces);
            InfiniChessClient.Assets.LoadSound(state, InfiniChessClient.Assets.PlaceTrackAudio);
        };
        Assets.LoadImage = function (state, imageToLoad) {
            state.load.image(imageToLoad.assetKey, imageToLoad.path);
        };
        Assets.LoadSound = function (state, soundToLoad) {
            state.load.audio(soundToLoad.assetKey, soundToLoad.path);
        };
        Assets.LoadSpriteSheet = function (state, spriteSheetToLoad, frameWidth, frameHeight, frameNum) {
            if (frameNum === void 0) { frameNum = -1; }
            if (frameNum == -1) {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight);
            }
            else {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight, frameNum);
            }
        };
        Assets.LoadingBar = new InfiniChessClient.IdentifierPathPair('loadingBar', '../assets/sprites/loadingBar.png');
        Assets.ChessGrid = new InfiniChessClient.IdentifierPathPair('chessGrid', '../assets/sprites/ChessGrid.png');
        Assets.ChessPieces = new InfiniChessClient.IdentifierPathPair('chessPieces', '../assets/sprites/Pieces.png');
        Assets.PlaceTrackAudio = new InfiniChessClient.IdentifierPathPair('placeTrackAudio', '../assets/audio/PlaceTrack.mp3');
        return Assets;
    }());
    InfiniChessClient.Assets = Assets;
})(InfiniChessClient || (InfiniChessClient = {}));
