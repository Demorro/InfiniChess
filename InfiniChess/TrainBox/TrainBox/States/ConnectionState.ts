﻿module InfiniChessClient {

    export class ConnectionState extends Phaser.State {

        playerName: string;
        nameText: Phaser.Text;
        deleteKey: Phaser.Key;
        enterKey: Phaser.Key;

        instructionMessageString: string;
        instructionMessageText: Phaser.Text;

        create() {
            this.game.input.enabled = true;

            var style = { font: "65px Arial", fill: "#ffffff", align: "center" };

            this.playerName = "Elliot";
            this.nameText = new Phaser.Text(this.game, 50, 150, this.playerName, style);
            this.game.add.existing(this.nameText);

            this.instructionMessageString = "Type yo' name.";
            this.instructionMessageText = new Phaser.Text(this.game, 50, 50, this.instructionMessageString, style);
            this.game.add.existing(this.instructionMessageText);

            this.game.input.keyboard.addCallbacks(this.game, null, null, this.OnKeyboardKeyDown);
            this.deleteKey = this.game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
            this.deleteKey.onDown.add(this.DeletePressed, this);

            //Setup the text that displays the owning players name
            var style = { font: "32px Arial", fill: "#000", align: "center" };

            this.game.add.button(100, 500, Assets.Button.assetKey, this.JoinLocalServer, this, 1, 0, 2, 0);
            this.game.add.text(145, 540, "Join Local Server", style);

            this.game.add.button(600, 500, Assets.Button.assetKey, this.JoinRemoteServer, this, 1, 0, 2, 0);
            this.game.add.text(635, 540, "Join Remote Server", style);
        }

        JoinLocalServer = (): void => {
            this.connect(true);
        }

        JoinRemoteServer = (): void => {
            this.connect(false);
        }

        OnKeyboardKeyDown = (): void => {
            this.playerName += this.game.input.keyboard.lastChar;
            this.nameText.text = this.playerName;
        }

        DeletePressed = (): void => {
            if (this.playerName.length > 0) {
                this.playerName = this.playerName.slice(0, this.playerName.length - 1);
                this.nameText.text = this.playerName;
            }
        }

        update() {
        }

        connect = (connectToLocal: boolean = false): void => {
            this.game.state.start('GameState', true, false, this.playerName,connectToLocal);
        }

    }
} 