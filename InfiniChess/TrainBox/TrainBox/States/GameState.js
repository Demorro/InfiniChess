var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var GameState = (function (_super) {
        __extends(GameState, _super);
        function GameState() {
            var _this = this;
            _super.apply(this, arguments);
            this.gamePaused = false;
            this.InitialisePlayerPropertiesOnServer = function (socket) {
                console.log('Sending player properties to server');
                socket.emit("setserverplayerproperties", Date.now(), socket.id, _this.localPlayerName);
            };
            this.RecievePlayerProperties = function (timeStamp, playerID, playerName) {
                console.log('Recieving Player Properties packet about player ID : ' + playerID + ' and name : ' + playerName);
                if (!_this.remotePlayers.containsKey(playerID)) {
                    console.log('Attempting to set player properties for player that does not exist on client, ID : ' + playerID + ' and name : ' + playerName);
                    return;
                }
                _this.remotePlayers.getValue(playerID).UpdatePlayerPropertiesFromNetwork(playerName);
            };
            this.ResponseToConnectionToServer = function (timeStamp, pieceType, xSpawnIndex, ySpawnIndex) {
                console.log('Recieveing Connection response pingback, adding initial piece at X : ' + xSpawnIndex + ' Y : ' + ySpawnIndex);
                _this.localPlayer.GivePlayerNewPiece(_this.game, pieceType, xSpawnIndex, ySpawnIndex, _this.nameLabelGroup);
                _this.InitialisePlayerPropertiesOnServer(GameState.socket);
            };
            this.RecievePieceInformationPacket = function (timestamp, playerID, pieceMapIndex, pieceType, xPos, yPos) {
                console.log('Recieving Piece information from player ID : ' + playerID + ' about piecetype ' + pieceType + ' at position : X : ' + xPos + ' Y : ' + yPos);
                _this.AddNewPlayerIfNeccesary(playerID);
                _this.remotePlayers.getValue(playerID).UpdatePieceInformationFromNetwork(pieceMapIndex, pieceType, xPos, yPos, _this.nameLabelGroup);
                _this.SortPieceZOrder();
            };
            this.AddNewPlayerIfNeccesary = function (playerNetworkID) {
                if (!_this.remotePlayers.containsKey(playerNetworkID)) {
                    console.log('Adding New Player, ID : ' + playerNetworkID);
                    _this.remotePlayers.setValue(playerNetworkID, new InfiniChessClient.Player(_this.grid, _this, playerNetworkID));
                    return true;
                }
                return false;
            };
            this.SortPieceZOrder = function () {
                for (var i = 0; i < _this.grid.GetGridWidth(); i++) {
                    for (var j = 0; j < _this.grid.GetGridHeight(); j++) {
                        var pieceOnCell = _this.grid.GetCellAtIndex(new Phaser.Point(i, j)).GetPieceOnCell();
                        if (pieceOnCell != null) {
                            console.log('Sorting');
                            pieceOnCell.bringToTop();
                        }
                    }
                }
            };
            this.PausedGame = function () {
                _this.gamePaused = true;
            };
            this.UnPausedGame = function () {
                _this.gamePaused = false;
                _this.game.time.elapsed = 0.166;
            };
        }
        GameState.prototype.init = function (playerName) {
            this.localPlayerName = playerName;
        };
        GameState.prototype.create = function () {
            var gridWidth = 10;
            var gridHeight = 10;
            this.grid = new InfiniChessClient.Grid(this.game, this, gridWidth, gridHeight);
            this.cam = new InfiniChessClient.Camera(this.game, 200, 100, (InfiniChessClient.Grid.CELLXPIXELS * (gridWidth)), InfiniChessClient.Grid.CELLYPIXELS * (gridHeight));
            this.mousePos = new Phaser.Point(0, 0);
            this.game.onPause.add(this.PausedGame, this);
            this.game.onResume.add(this.UnPausedGame, this);
            this.localPlayer = new InfiniChessClient.Player(this.grid, this, 'local');
            this.remotePlayers = new InfiniChessClient.Dictionary();
            this.nameLabelGroup = new Phaser.Group(this.game);
            this.game.add.existing(this.nameLabelGroup);
            GameState.socket = io.connect(GameState.SERVER_ADDRESS);
            GameState.socket.on('connectionresponse', this.ResponseToConnectionToServer);
            GameState.socket.on('piecestate', this.RecievePieceInformationPacket);
            GameState.socket.on('setclientplayerproperties', this.RecievePlayerProperties);
            this.input.enabled = true;
        };
        GameState.prototype.update = function () {
            if (this.gamePaused == false) {
                this.mousePos.x = this.input.x;
                this.mousePos.y = this.input.y;
                this.cam.Update(this.time.elapsedMS);
            }
        };
        GameState.SERVER_ADDRESS = 'http://localhost:4000';
        return GameState;
    }(Phaser.State));
    InfiniChessClient.GameState = GameState;
})(InfiniChessClient || (InfiniChessClient = {}));
