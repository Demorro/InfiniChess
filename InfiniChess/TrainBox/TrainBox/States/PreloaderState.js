var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var PreloaderState = (function (_super) {
        __extends(PreloaderState, _super);
        function PreloaderState() {
            _super.apply(this, arguments);
        }
        PreloaderState.prototype.preload = function () {
            this.preloadBar = this.add.sprite(200, 250, InfiniChessClient.Assets.LoadingBar.assetKey);
            this.load.setPreloadSprite(this.preloadBar);
            InfiniChessClient.Assets.LoadAllAssets(this);
        };
        PreloaderState.prototype.create = function () {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);
        };
        PreloaderState.prototype.startGame = function () {
            this.game.state.start('ConnectionState', true, false);
        };
        return PreloaderState;
    }(Phaser.State));
    InfiniChessClient.PreloaderState = PreloaderState;
})(InfiniChessClient || (InfiniChessClient = {}));
