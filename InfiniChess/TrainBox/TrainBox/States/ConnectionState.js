var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var ConnectionState = (function (_super) {
        __extends(ConnectionState, _super);
        function ConnectionState() {
            var _this = this;
            _super.apply(this, arguments);
            this.OnKeyboardKeyDown = function () {
                _this.playerName += _this.game.input.keyboard.lastChar;
                _this.nameText.text = _this.playerName;
            };
            this.DeletePressed = function () {
                if (_this.playerName.length > 0) {
                    _this.playerName = _this.playerName.slice(0, _this.playerName.length - 1);
                    _this.nameText.text = _this.playerName;
                }
            };
            this.EnterPressed = function () {
                _this.connect();
            };
            this.connect = function () {
                _this.game.state.start('GameState', true, false, _this.playerName);
            };
        }
        ConnectionState.prototype.create = function () {
            this.game.input.enabled = true;
            var style = { font: "65px Arial", fill: "#ffffff", align: "center" };
            this.playerName = "Elliot";
            this.nameText = new Phaser.Text(this.game, 50, 150, this.playerName, style);
            this.game.add.existing(this.nameText);
            this.instructionMessageString = "Type yo' name. Push enter to connect plz.";
            this.instructionMessageText = new Phaser.Text(this.game, 50, 50, this.instructionMessageString, style);
            this.game.add.existing(this.instructionMessageText);
            this.game.input.keyboard.addCallbacks(this.game, null, null, this.OnKeyboardKeyDown);
            this.deleteKey = this.game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
            this.enterKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            this.deleteKey.onDown.add(this.DeletePressed, this);
            this.enterKey.onDown.add(this.EnterPressed, this);
        };
        ConnectionState.prototype.update = function () {
        };
        return ConnectionState;
    }(Phaser.State));
    InfiniChessClient.ConnectionState = ConnectionState;
})(InfiniChessClient || (InfiniChessClient = {}));
