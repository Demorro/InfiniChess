﻿module InfiniChessClient {
    export class GameState extends Phaser.State {
        grid: Grid;
        cam: Camera;
        private gamePaused: boolean = false;
        mousePos: Phaser.Point;

        static socket: SocketIOClient.Socket;
        static SERVER_ADDRESS_LOCAL: string = 'http://localhost:3000';
        static SERVER_ADDRESS_REMOTE: string = 'http://52.17.49.151:3000';
        connectToLocal: boolean = false;
        ui: UI;
        localPlayerName: string;
        localPlayer: Player; //The player that this client is represented by
        remotePlayers: Dictionary<string, Player> //A hashmap of the remote players, with the unique socket.io ID as a string and the player object as value, which controls the pieces.

        uiGroup: Phaser.Group;
        boardGroup: Phaser.Group;
        pieceGroup: Phaser.Group;

        init(playerName: string, connectToLocal: boolean) {
            this.localPlayerName = playerName;
            this.connectToLocal = connectToLocal;
        }

        //Initialise the main game state
        create() {
            var gridWidth: number = 10;
            var gridHeight: number = 10;
            this.cam = new Camera(this.game, -gridWidth * (Grid.CELLXPIXELS / 8), -gridHeight * (Grid.CELLYPIXELS / 8), window.innerWidth, window.innerHeight);
            this.grid = new Grid(this.game, this, gridWidth, gridHeight);

            this.mousePos = new Phaser.Point(0, 0);

            this.boardGroup = new Phaser.Group(this.game);
            this.pieceGroup = new Phaser.Group(this.game);
            this.uiGroup = new Phaser.Group(this.game);
            this.uiGroup.fixedToCamera = true;

            this.add.existing(this.boardGroup);
            this.add.existing(this.pieceGroup);
            this.add.existing(this.uiGroup);

            this.game.onPause.add(this.PausedGame, this);
            this.game.onResume.add(this.UnPausedGame, this);

            this.ui = new UI(this.game, this.localPlayer, this.uiGroup);

            this.localPlayer = new Player(this.grid, this, 'local', this.uiGroup, this.boardGroup, this.pieceGroup, this.ui);
            this.localPlayer.UpdatePlayerPropertiesFromNetwork(this.localPlayerName, 0); //Pretend we got a packet back from the network concerning outselves with our name

            this.remotePlayers = new Dictionary<string, Player>();

            if (this.connectToLocal) {
                GameState.socket = io.connect(GameState.SERVER_ADDRESS_LOCAL);
            }
            else {
                GameState.socket = io.connect(GameState.SERVER_ADDRESS_REMOTE);
            }
            GameState.socket.on('connectionresponse', this.ResponseToConnectionToServer); //Register listening to server connection response, which should come pretty immediately.
            GameState.socket.on('piecestate', this.RecievePieceInformationPacket); //Register listening for information on remote pieces.
            GameState.socket.on('setclientplayerproperties', this.RecievePlayerProperties); //Register listening for player properties.
            GameState.socket.on('disconnectnotify', this.RecieveDisconnectionNotification); //Register listening for other player disconnects.
            GameState.socket.on('movementcommand', this.RecieveMovementCommand); //Register listening to piece movement commands from the server.
            GameState.socket.on('piecetaken', this.RecievePieceTakenNotification); //Register listening to notifications about pieces 

            this.game.stage.disableVisibilityChange = true; //Important, game needs to keep responding to packet updates when not in focus
            this.input.enabled = true;
        }

        //Send the player properties to the server so it knows its name n' shit.
        private InitialisePlayerPropertiesOnServer = (socket: SocketIOClient.Socket): void => {
            console.log('Sending player properties to server');
            socket.emit("setserverplayerproperties", Date.now(), socket.id, this.localPlayerName);
        }

        //Recieve player properties about a player in the game from server, and set it. Properties is like its metadata, name, etc
        private RecievePlayerProperties = (timeStamp: number, playerID: string, playerName: string, money : number): void => {
            console.log('Recieving Player Properties packet about player ID : ' + playerID + ' and name : ' + playerName);

            if (this.localPlayer.GetPlayerNetworkID() == playerID) {
                this.localPlayer.UpdatePlayerPropertiesFromNetwork(playerName, money);
            }

            if (!this.remotePlayers.containsKey(playerID)) {
                console.log('Attempting to set player properties for player that does not exist on client, ID : ' + playerID + ' and name : ' + playerName);
                return;
            }
            this.remotePlayers.getValue(playerID).UpdatePlayerPropertiesFromNetwork(playerName, null); //Only give money to the local player as it's only used to update the UI
        }

        //The server pingback, makes the piece that is spawned for the player when they first connect
        private ResponseToConnectionToServer = (timeStamp : number, playerID : string, pieceType: PieceType, pieceMapIndex : number, xSpawnIndex: number, ySpawnIndex: number) => {
            console.log('Recieveing Connection response pingback, adding initial piece at X : ' + xSpawnIndex + ' Y : ' + ySpawnIndex + ' ID : ' + playerID);
            //Let the local player know what its server ID is
            this.localPlayer.SetPlayerNetworkIDDirectly(playerID);
            this.localPlayer.SetIsLocalPlayer();
            console.log('Setting local player ID : ' + playerID);
            //Add the initial spawn piece to the local player
            this.localPlayer.GivePlayerNewPiece(this.game, pieceType, pieceMapIndex, xSpawnIndex, ySpawnIndex, Date.now());
            this.InitialisePlayerPropertiesOnServer(GameState.socket);

            this.SortPieceZOrder();
        }

        //A packet about a specific piece. This can be though more of a "Set Piece Directly" packet. It also handles adding players if the piece packet dosent have an associcated player. This is a bit clumsy
        private RecievePieceInformationPacket = (timestamp: number, playerID: string, pieceMapIndex : number, pieceType: PieceType, xPos: number, yPos: number, timestampOfLastCellLanding : number) => {
            console.log('Recieving Piece information from player ID : ' + playerID + ' about piecetype ' + pieceType + ' at position : X : ' + xPos + ' Y : ' + yPos);

            this.AddNewPlayerIfNeccesary(playerID);

            var player: Player = this.GetLocalOrRemotePlayerByID(playerID);
            if (player != null) {
                player.UpdatePieceInformationFromNetwork(pieceMapIndex, pieceType, xPos, yPos, timestampOfLastCellLanding);
            }

            this.SortPieceZOrder();
        }

        //A packet containing a movement command for a specific piece
        private RecieveMovementCommand = (timestamp: number, playerID: string, pieceMapIndex: number, pieceType: PieceType, moveStartIndexX: number, moveStartIndexY: number, moveTargetIndexX: number, moveTargetIndexY: number, timestampToArriveAt: number) => {
            console.log('Recieving movement command information from player ID : ' + playerID + ' about piecetype ' + pieceType + ' to move to : X : ' + moveTargetIndexX + ' Y : ' + moveTargetIndexY + ' should take : ' + (timestampToArriveAt - Date.now()).toString() + 'ms to arrive.');
            var player: Player = this.GetLocalOrRemotePlayerByID(playerID);
            if (player != null) {
                player.MakePieceMove(pieceMapIndex, moveTargetIndexX, moveTargetIndexY, timestampToArriveAt);
            }  
        }

        //A packet containing a notification that a piece has been taken
        private RecievePieceTakenNotification = (timestamp: number, takenPlayerID: string, takenPieceMapIndex: number, takerPlayerID: string, takerPieceMapIndex: number, takenSquareIndexX: number, takenSquareIndexY: number) => {
            console.log('Recieving Taken Notification. PlayerID : ' + takerPlayerID + ' taking piece of PlayerID : ' + takenPlayerID + '.');
            var takingPlayer: Player = this.GetLocalOrRemotePlayerByID(takerPlayerID);
            var takenPlayer: Player = this.GetLocalOrRemotePlayerByID(takenPlayerID);

            takingPlayer.HasTakenPiece(takerPieceMapIndex, this.grid.GetCellAtIndex(takenSquareIndexX, takenSquareIndexY));
            takenPlayer.PieceTaken(takenPieceMapIndex);
        }

        //The local and remote players are seperated, this function allows us to treat them as the same. Returns null if player dosent exist
        private GetLocalOrRemotePlayerByID = (playerID: string): Player => {
            if (this.remotePlayers.containsKey(playerID)) {
                return this.remotePlayers.getValue(playerID);
            }
            else if (this.localPlayer.GetPlayerNetworkID() == playerID) {
                return this.localPlayer;
            }
            throw new Error('Attempting to get invalid player by ID');
        }

        //Adds a new player if the server has told us about it(or told us about a piece it has, can be used either way. Returns true if a new player is added
        private AddNewPlayerIfNeccesary = (playerNetworkID: string): boolean => {
            //Assuming we don't already know about this player, add it to the known players list
            if ((!this.remotePlayers.containsKey(playerNetworkID)) && (this.localPlayer.GetPlayerNetworkID() != playerNetworkID)) {
                console.log('Adding New Player, ID : ' + playerNetworkID);
                this.remotePlayers.setValue(playerNetworkID, new Player(this.grid, this, playerNetworkID, this.uiGroup, this.boardGroup, this.pieceGroup, this.ui));
                return true;
            }
            return false;
        }

        //Receive a packet about a player disconnecting, and remove the client side representation of that player
        private RecieveDisconnectionNotification = (timestamp: number, playerNetworkID: string): boolean => {

            console.log('Recieving disconnect notification of ID : ' + playerNetworkID);

            //If the message is about this local player, treat it as though we've been kicked.
            if (this.localPlayer.GetPlayerNetworkID() == playerNetworkID) {
                console.log('You have been removed from server');
                this.game.state.start('ConnectionState', true, true);
                return true;
            }

            //If we're getting a disconnection message about a remote player, remove it from errything.
            if (this.remotePlayers.containsKey(playerNetworkID)) {
                console.log('Removing Player, ID : ' + playerNetworkID);
                this.remotePlayers.getValue(playerNetworkID).PurgeFromGame();
                this.remotePlayers.remove(playerNetworkID);
                return true;
            }
            return false;
        }

        //Sorts all the pieces in the game so they're rendered in the right order
        private SortPieceZOrder = (): void => {
            //Scan through the grid sorting by the y axis
            for (var i = 0; i < this.grid.GetGridWidth(); i++) {
                for (var j = 0; j < this.grid.GetGridHeight(); j++) {
                    var pieceOnCell: Piece = this.grid.GetCellAtIndex(i, j).GetPieceOnCell();
                    if (pieceOnCell != null) {
                        pieceOnCell.bringToTop();
                        pieceOnCell.BringNameLableToTop();
                        pieceOnCell.BringCooldownLabelToTop();
                    }
                }
            }
        }

        update() {
            if (this.gamePaused == false) {
                this.mousePos.x = this.input.x;
                this.mousePos.y = this.input.y;

                this.cam.Update(this.time.elapsedMS);
            }
        }

        private PausedGame = () => {
            this.gamePaused = true;
        }

        private UnPausedGame = () => {
            this.gamePaused = false;
            this.game.time.elapsed = 0.166;
        }

    }
} 