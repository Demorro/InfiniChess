var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, window.innerWidth, window.innerHeight, Phaser.AUTO, 'content', null);
            this.state.add('BootState', InfiniChessClient.BootState, false);
            this.state.add('PreloaderState', InfiniChessClient.PreloaderState, false);
            this.state.add('ConnectionState', InfiniChessClient.ConnectionState, false);
            this.state.add('GameState', InfiniChessClient.GameState, false);
            this.state.start('BootState');
        }
        return Game;
    }(Phaser.Game));
    InfiniChessClient.Game = Game;
})(InfiniChessClient || (InfiniChessClient = {}));
