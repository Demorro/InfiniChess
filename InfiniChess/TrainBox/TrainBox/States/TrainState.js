var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TrainBox;
(function (TrainBox) {
    var TrainState = (function (_super) {
        __extends(TrainState, _super);
        function TrainState() {
            var _this = this;
            _super.apply(this, arguments);
            this.gamePaused = false;
            this.SpawnTrain = function () {
                _this.game.sound.play(TrainBox.Assets.TrainButtonAudio.assetKey);
                _this.selectionPanel.DisableTrainSpawnButtonForTime();
                var noOfCarraigesToSpawn = _this.game.rnd.integerInRange(2, 8);
                var spawnOffsetY = 30;
                var startCell = _this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1));
                var testTrain = new TrainBox.Train(_this.game, TrainBox.Assets.Train.assetKey, startCell, new Phaser.Point(380, startCell.position.y - spawnOffsetY));
                var lastSpawnObject = testTrain;
                testTrain.angle = 90;
                _this.trains.push(testTrain);
                for (var i = 0; i < noOfCarraigesToSpawn; i++) {
                    var testCarriage = new TrainBox.Carraige(_this.game, TrainBox.Assets.Carriage.assetKey, lastSpawnObject, testTrain);
                    testCarriage.position = new Phaser.Point(testTrain.position.x, testTrain.position.y);
                    testCarriage.angle = 90;
                    _this.game.add.existing(testCarriage);
                    lastSpawnObject = testCarriage;
                    _this.carriages.push(testCarriage);
                }
                _this.game.add.existing(testTrain);
                _this.selectionPanel.BringPanelToTop();
            };
            this.PausedGame = function () {
                _this.gamePaused = true;
            };
            this.UnPausedGame = function () {
                _this.gamePaused = false;
                _this.game.time.elapsed = 0.166;
            };
        }
        TrainState.prototype.create = function () {
            var gridWidth = 36;
            var gridHeight = 36;
            this.constructionGrid = new TrainBox.Grid(this.game, gridWidth, gridHeight);
            this.background1 = this.add.sprite(0, 0, TrainBox.Assets.Background.assetKey);
            this.background2 = this.add.sprite(this.background1.width, 0, TrainBox.Assets.Background.assetKey);
            this.background3 = this.add.sprite(0, this.background1.height, TrainBox.Assets.Background.assetKey);
            this.background4 = this.add.sprite(this.background1.width, this.background1.height, TrainBox.Assets.Background.assetKey);
            this.background1.body = null;
            this.background2.body = null;
            this.background3.body = null;
            this.background4.body = null;
            this.startTrackNotificationArrow = this.add.sprite(0, 0, TrainBox.Assets.Arrow.assetKey);
            this.cam = new TrainBox.Camera(this.game, TrainBox.Grid.CELLXPIXELS * 2, TrainBox.Grid.CELLYPIXELS * 2, (TrainBox.Grid.CELLXPIXELS * (gridWidth - 4)), TrainBox.Grid.CELLYPIXELS * (gridHeight - 4));
            this.gridSelectionCursor = new TrainBox.Cursor(this.game, this.constructionGrid.GetCellAtIndex(new Phaser.Point(5, 0)), this.constructionGrid);
            this.selectionPanel = new TrainBox.SelectionPanel(this.game, false, this.gridSelectionCursor);
            this.gridSelectionCursor.SetSelectionPanelReference(this.selectionPanel);
            this.game.add.existing(this.gridSelectionCursor);
            this.game.add.existing(this.selectionPanel);
            this.selectionPanel.PopulateSelectableTracks(this.game, this.SpawnTrain);
            this.mousePos = new Phaser.Point(0, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 0)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 0)).GetOccupyingTrack().SetCannotBeModified();
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 1)).GetOccupyingTrack().SetCannotBeModified();
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 2)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 2)).GetOccupyingTrack().SetCannotBeModified();
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 3)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 3)).GetOccupyingTrack().SetCannotBeModified();
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).PlaceTrack(this.selectionPanel.GetItem(0).track, 0);
            this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).GetOccupyingTrack().SetCannotBeModified();
            this.startTrackNotificationArrow.position = new Phaser.Point(this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).position.x, this.constructionGrid.GetCellAtIndex(new Phaser.Point(8, 4)).position.y);
            this.startTrackNotificationArrow.position.x -= 145;
            this.startTrackNotificationArrow.position.y += 0;
            this.trains = [];
            this.carriages = [];
            this.game.onPause.add(this.PausedGame, this);
            this.game.onResume.add(this.UnPausedGame, this);
        };
        TrainState.prototype.update = function () {
            if (this.gamePaused == false) {
                this.mousePos.x = this.input.x;
                this.mousePos.y = this.input.y;
                this.cam.Update(this.time.elapsedMS);
                this.gridSelectionCursor.Update(this.cam.ScreenToWorldSpace(this.mousePos), this.cam);
                this.selectionPanel.Update(this.mousePos);
                for (var i = 0; i < this.trains.length; i++) {
                    if (this.trains[i].alive != false) {
                        this.trains[i].DoMovement();
                    }
                    else {
                        delete this.trains[i];
                        this.trains.splice(i, 1);
                    }
                }
                for (var i = 0; i < this.carriages.length; i++) {
                    if (this.carriages[i].alive != false) {
                        this.carriages[i].Update();
                    }
                    else {
                        delete this.carriages[i];
                        this.carriages.splice(i, 1);
                    }
                }
                for (var i = 0; i < this.trains.length; i++) {
                    for (var j = 0; j < this.trains.length; j++) {
                        if ((this.trains[i] != null) && (this.trains[j] != null)) {
                            if ((this.trains[i].alive) && (this.trains[j].alive)) {
                                if (this.trains[i].overlap(this.trains[j])) {
                                    if (this.trains[i] != this.trains[j]) {
                                        this.trains[j].CrashTrain();
                                    }
                                }
                            }
                        }
                    }
                }
                for (var i = 0; i < this.carriages.length; i++) {
                    for (var j = 0; j < this.trains.length; j++) {
                        if ((this.carriages[i] != null) && (this.trains[j] != null)) {
                            if ((this.carriages[i].alive) && (this.trains[j].alive)) {
                                if (this.carriages[i].overlap(this.trains[j])) {
                                    if (this.carriages[i].belongsToTrain != this.trains[j]) {
                                        this.trains[j].CrashTrain();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        return TrainState;
    })(Phaser.State);
    TrainBox.TrainState = TrainState;
})(TrainBox || (TrainBox = {}));
