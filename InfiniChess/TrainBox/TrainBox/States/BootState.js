var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            _super.apply(this, arguments);
        }
        BootState.prototype.preload = function () {
            InfiniChessClient.Assets.LoadImage(this, InfiniChessClient.Assets.LoadingBar);
        };
        BootState.prototype.create = function () {
            this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); };
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = false;
            if (this.game.device.desktop) {
            }
            else {
            }
            this.game.state.start('PreloaderState', true, false);
        };
        return BootState;
    }(Phaser.State));
    InfiniChessClient.BootState = BootState;
})(InfiniChessClient || (InfiniChessClient = {}));
