﻿module InfiniChessClient {

    export class BootState extends Phaser.State {

        preload() {
            InfiniChessClient.Assets.LoadImage(this, InfiniChessClient.Assets.LoadingBar);
        }

        create() {

            //Disable the right click context menu, cause we want to use right click for rotating
            this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); }
               
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
 
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = false;

            
            if (this.game.device.desktop) {
                //  If you have any desktop specific settings, they can go in here
                //this.stage.scale.pageAlignHorizontally = true;
            }
            else {
                //  Same goes for mobile settings.
            }

            this.game.state.start('PreloaderState', true, false);

        }

    }

} 