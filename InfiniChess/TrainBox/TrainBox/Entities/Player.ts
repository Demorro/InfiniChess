﻿module InfiniChessClient {
    //Represents a player. Basically a storage class for pieces, and allowing for good differentiation between the local and remote players
    export class Player{

        private gameState: Phaser.State;
        private grid: Grid;

        private playerID: string; //The network ID of this player provided via socket.io
        private playerName: string = ""; //The name of the player

        private isLocalPlayer: boolean = false; //Whether or not this player is the local player

        //REMEMBER. This array gets bigger and bigger, pieces arnt removed from their index even if they are taken. So you NEED to clear this when the player dies
        private pieces: Dictionary<number,Piece>; //A list of the pieces that this player controls

        private uiGroup: Phaser.Group; //The name/timers are put on the ui layer, over everything.
        private boardGroup: Phaser.Group; //the board highlights are in this group. Could also be called midground
        private pieceGroup: Phaser.Group; //The foreground, where the pieces are.

        private ui: UI; //A reference to the UI object, only for the local player really.

        constructor(grid: Grid, gameState: Phaser.State, playerID: string, uiGroup: Phaser.Group, boardGroup: Phaser.Group, pieceGroup: Phaser.Group, ui : UI) {
            this.gameState = gameState;
            this.grid = grid;
            this.playerID = playerID;

            this.uiGroup = uiGroup;
            this.boardGroup = boardGroup;
            this.pieceGroup = pieceGroup;

            this.ui = ui;

            this.pieces = new Dictionary<number, Piece>();
        }

        //Takes information from the piece update packet, and adds a piece if it dosent exist, or updates the piece if it does
        public UpdatePieceInformationFromNetwork = (pieceMapIndex: number, pieceType: PieceType, xPos: number, yPos: number, timestampOfLastCellLanding: number): boolean => {
            if (!this.pieces.containsKey(pieceMapIndex)) {
                //we want to add a piece as it dosent exist
                this.GivePlayerNewPiece(this.gameState.game, pieceType, pieceMapIndex, xPos, yPos, timestampOfLastCellLanding);
                return true;
            }
            else {
                //just update the piece
                this.pieces.getValue(pieceMapIndex).PlaceOnCellAtIndex(xPos, yPos);
                //this.pieces.getValue(pieceMapIndex).SetTimestampOfLastCellLanding(timestampOfLastCellLanding);
                return false;
            }
        }

        //Update the player properties on the server
        public UpdatePlayerPropertiesFromNetwork = (playerName: string, money : number): void => {
            for (var piece of this.pieces.values()) {
                if (piece != null) {
                    piece.UpdatePieceNameLabel(playerName);
                }
            }
            if (this.IsLocalPlayer) {
                this.ui.UpdateMoney(money);
            }
        }

        //Completely remove everything about this player from the game, mainly the pieces it owns
        public PurgeFromGame = (): void => {
            for (var piece of this.pieces.values()) {
                if (piece != null) {
                    piece.PurgePiece();
                    piece.destroy();
                }
            }
        }

        //Get the name of the player
        public GetPlayerName = (): string => {
            return this.playerName;
        }

        //Directly set what the player ID is. Dangerous, used just to hook up the local player ID.
        public SetPlayerNetworkIDDirectly = (playerID: string): void => {
            this.playerID = playerID;
        }

        //Get the players network ID
        public GetPlayerNetworkID = (): string => {
            return this.playerID;
        }

        //Let this player know directly that it's the local one, from the gamestate
        public SetIsLocalPlayer = (): void => {
            this.isLocalPlayer = true;
        }

        //Whether or not this player is the one that the local client is controlling
        public IsLocalPlayer = (): boolean => {
            return this.isLocalPlayer;
        }

        //Gives this player a new piece on the grid at the specified index
        public GivePlayerNewPiece = (game: Phaser.Game, piece: PieceType, serverPieceIndexMapping: number, pieceXIndex: number, pieceYIndex: number, timestampOfLastLandingOnCell: number): Piece => {
            var pieceToAdd: Piece;
            if (piece == PieceType.Pawn) { pieceToAdd = new Pawn(game, this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), this, this.pieceGroup, this.boardGroup, this.uiGroup); }
            else if (piece == PieceType.Knight) { pieceToAdd = new Knight(game, this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), this, this.pieceGroup, this.boardGroup, this.uiGroup); }
            else if (piece == PieceType.Bishop) { pieceToAdd = new Bishop(game, this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), this, this.pieceGroup, this.boardGroup, this.uiGroup); }
            else if (piece == PieceType.Rook) { pieceToAdd = new Rook(game, this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), this, this.pieceGroup, this.boardGroup, this.uiGroup); }
            else if (piece == PieceType.Queen) { pieceToAdd = new Queen(game, this.grid, serverPieceIndexMapping, new Phaser.Point(pieceXIndex, pieceYIndex), this, this.pieceGroup, this.boardGroup, this.uiGroup); }
            else { throw new Error('Incorrect Piece Type, GivePlayerNewPiece, Player.ts'); }

            pieceToAdd.SetTimestampOfLastCellLanding(timestampOfLastLandingOnCell);
            this.pieces.setValue(serverPieceIndexMapping, pieceToAdd);

            return pieceToAdd;
        }

        //In response to a movement command from server, move a specific piece
        public MakePieceMove = (pieceIndex: number, cellToMoveToX: number, cellToMoveToY: number, timestampToArriveAt: number) => {

            var cellToMoveTo: Cell = this.grid.GetCellAtIndex(cellToMoveToX, cellToMoveToY);
            if (cellToMoveTo == null) {
                throw new ReferenceError('Attempting to make move to invalid cell');
            }

             //Check that the piece exists
            if (this.pieces.containsKey(pieceIndex)) {
                this.pieces.getValue(pieceIndex).MakeMove(cellToMoveTo, timestampToArriveAt);
            }
            else {
                throw new ReferenceError('Attempting to make move on piece that does not exist');
            }
        }

        //Get a specific piece at the provided map index
        public GetPieceAtIndex = (pieceIndex: number): Piece => {
            if (this.pieces.containsKey(pieceIndex)) {
                return this.pieces.getValue(pieceIndex);
            }
            console.log('Attemping to get null piece at index : ' + pieceIndex + 'on player ID : '+ this.GetPlayerNetworkID());
            return null;
        }

        //In response to a message from the server concerning one of this players pieces being taken. Removes the taken piece from the game.
        public PieceTaken = (takenPieceIndex: number): void => {
            var takenPiece: Piece = this.GetPieceAtIndex(takenPieceIndex); 
            takenPiece.RemoveFromGame();
            this.pieces.remove(takenPieceIndex);
        }

        //In response to a message from the server concerning one of this player pieces taking another. At the moment just places the taking piece on the taking cell
        public HasTakenPiece = (takingPieceIndex: number, takeCell: Cell): void => {
            if (takeCell == null) {throw new Error('Invalid take cell');}
            var takingPiece: Piece = this.GetPieceAtIndex(takingPieceIndex);
            takingPiece.PlaceOnCell(takeCell);
        }
    }
}