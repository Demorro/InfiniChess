﻿module InfiniChessClient {
    //A knight piece.
    export class Knight extends Piece {
        constructor(game: Phaser.Game, grid: Grid, serverPieceIndexMapping: number, spawnIndexOnGrid: Phaser.Point, owningPlayer: Player, pieceGroup: Phaser.Group, boardGroup: Phaser.Group, uiGroup: Phaser.Group) {
            super(game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, Assets.ChessPieces.assetKey, new Phaser.Rectangle(212, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);

            this.pieceType = PieceType.Knight;
            this.movementCoolDownInSeconds = 5;
        }

        protected GetCandidateMovementIndices(): Array<Phaser.Point> {
            var pts: Array<Phaser.Point> = [];

            //Load all of the possible knight movement squares
            pts.push(new Phaser.Point(2, 1));
            pts.push(new Phaser.Point(1, 2));

            pts.push(new Phaser.Point(-2, 1));
            pts.push(new Phaser.Point(-1, 2));

            pts.push(new Phaser.Point(2, -1));
            pts.push(new Phaser.Point(1, -2));

            pts.push(new Phaser.Point(-2, -1));
            pts.push(new Phaser.Point(-1, -2));

            return pts;
        }
    }
}