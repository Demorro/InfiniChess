var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    var Queen = (function (_super) {
        __extends(Queen, _super);
        function Queen(game, grid, spawnIndexOnGrid, owningPlayer, nameLabelGroup) {
            _super.call(this, game, grid, spawnIndexOnGrid.x, spawnIndexOnGrid.y, InfiniChessClient.Assets.ChessPieces.assetKey, new Phaser.Rectangle(402, 0, 80, 126), owningPlayer, nameLabelGroup);
        }
        return Queen;
    }(InfiniChessClient.Piece));
    InfiniChessClient.Queen = Queen;
})(InfiniChessClient || (InfiniChessClient = {}));
