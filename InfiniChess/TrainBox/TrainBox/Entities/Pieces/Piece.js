var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InfiniChessClient;
(function (InfiniChessClient) {
    (function (PieceType) {
        PieceType[PieceType["Pawn"] = 0] = "Pawn";
        PieceType[PieceType["Rook"] = 1] = "Rook";
        PieceType[PieceType["Knight"] = 2] = "Knight";
        PieceType[PieceType["Bishop"] = 3] = "Bishop";
        PieceType[PieceType["Queen"] = 4] = "Queen";
    })(InfiniChessClient.PieceType || (InfiniChessClient.PieceType = {}));
    var PieceType = InfiniChessClient.PieceType;
    var Piece = (function (_super) {
        __extends(Piece, _super);
        function Piece(game, grid, xGridSpawnIndex, yGridSpawnIndex, pieceSpriteKey, texSubRect, owningPlayer, nameLabelGroup) {
            var _this = this;
            _super.call(this, game, grid.GetCellAtIndex(new Phaser.Point(xGridSpawnIndex, yGridSpawnIndex)).GetCenter().x, grid.GetCellAtIndex(new Phaser.Point(xGridSpawnIndex, yGridSpawnIndex)).GetCenter().y, pieceSpriteKey);
            this.UpdatePieceNameLabel = function (name) {
                _this.nameLabel.text = name;
            };
            this.PlaceOnCellAtIndex = function (xIndex, yIndex) {
                _this.PlaceOnCell(_this.grid.GetCellAtIndex(new Phaser.Point(xIndex, yIndex)));
            };
            this.PlaceOnCell = function (cell) {
                if (_this.currentCell != null) {
                    _this.currentCell.NotifyCellIsNowEmpty();
                }
                cell.NotifyPieceOnCell(_this);
                _this.currentCell = cell;
                _this.SetCellAllignedPosition();
            };
            this.SetCellAllignedPosition = function () {
                _this.position.x = _this.currentCell.GetCenter().x;
                _this.position.y = _this.currentCell.GetCenter().y;
                if (_this.nameLabel != null) {
                    _this.nameLabel.position = _this.anchor;
                }
            };
            this.cropRect = texSubRect;
            this.updateCrop();
            this.anchor.setTo(1.68, 1.465);
            this.grid = grid;
            this.PlaceOnCellAtIndex(xGridSpawnIndex, yGridSpawnIndex);
            this.owningPlayer = owningPlayer;
            this.nameLabel = new Phaser.Text(game, this.x, this.y, this.owningPlayer.GetPlayerName());
            this.nameLabel.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            nameLabelGroup.add(this.nameLabel);
        }
        return Piece;
    }(Phaser.Sprite));
    InfiniChessClient.Piece = Piece;
})(InfiniChessClient || (InfiniChessClient = {}));
