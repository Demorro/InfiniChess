﻿module InfiniChessClient {

    export enum PieceType {
        Pawn,
        Rook,
        Knight,
        Bishop,
        Queen
    }

    //The base piece class, has subclasses for each individual piece
    export abstract class Piece extends Phaser.Sprite {

        protected grid: Grid;
        protected currentCell: Cell;

        private owningPlayer: Player
        private nameLabel: Phaser.Text;
        private serverPieceIndexMapping: number;

        protected pieceType: PieceType; //What type of chess piece this is, set in derived class

        private static blackPieceSubRectYOffset: number = 126; //How much to offset the sprite sheet rect if we want to be a black piece as opposed to a while. Assumes the black piece is directly below the white on the spritesheet

        private lastTimeStampLandedOnTile: number = 0; //The last timestamp that this piece was placed on a tile ACCORDING TO THE SERVER
        protected movementCoolDownInSeconds: number = 5; //How many seconds must pass between the piece landing and it being able to move again. Set in derived class. Shouldn't really do anything but effect the look of the timer/allow selection, validation done server side.
        private coolDownLabel: Phaser.Text; //Label displayed under the player name notifying how long you must wait before moving this piece again.

        private uiGroup: Phaser.Group; //The name/timers are put on the ui layer, over everything.
        private boardGroup: Phaser.Group; //the board highlights are in this group. Could also be called midground
        private pieceGroup: Phaser.Group; //The foreground, where the pieces are.

        protected movementPatternIndices: Array<Phaser.Point>; //Array of point indices that specifiy where this piece can move to, relative to piece current position, ie 0,-1 will be one above the piece. Set polymorphically in the derived class.
        private movementPatternHighlights: Array<CellHighlight>; //Array of sprites used to overlay squares for movement highlight. Places at the movement pattern indices relative to the piece position.
        private isSelected: boolean = false;

        private targetCell: Cell = null; //Set when the MakeMove function is called, and the player tweens towards its new cell, used as the player stays in motion for a while and dosent arrive directly.

        //Setup Piece
        constructor(game: Phaser.Game, grid: Grid, serverPieceIndexMapping: number, xGridSpawnIndex: number, yGridSpawnIndex: number, pieceSpriteKey: string, texSubRect: Phaser.Rectangle, owningPlayer: Player, pieceGroup: Phaser.Group, boardGroup: Phaser.Group, uiGroup: Phaser.Group) {
            super(game, grid.GetCellAtIndex(xGridSpawnIndex, yGridSpawnIndex).GetCenter().x, grid.GetCellAtIndex(xGridSpawnIndex, yGridSpawnIndex).GetCenter().y, pieceSpriteKey);

            //Set the correct texture subrect for the piece
            this.cropRect = texSubRect;
            if (!owningPlayer.IsLocalPlayer()) {
                this.cropRect.offset(0, Piece.blackPieceSubRectYOffset);
                this.body = null;
            }
            this.updateCrop();

            this.grid = grid;
            this.owningPlayer = owningPlayer;
            this.serverPieceIndexMapping = serverPieceIndexMapping;

            //Setup the text that displays the owning players name
            var style = { font: "18px Arial", fill: "#fff", align: "center" };
            this.nameLabel = new Phaser.Text(game, this.x, this.y, this.owningPlayer.GetPlayerName(), style);
            this.nameLabel.stroke = '#000000';
            this.nameLabel.strokeThickness = 4;
            this.nameLabel.anchor.set(0.5);

            this.pieceGroup = pieceGroup;
            this.boardGroup = boardGroup;
            this.uiGroup = uiGroup;
            this.pieceGroup.add(this.nameLabel);


            //Setup the text that displays the piece move cooldown timer
            this.coolDownLabel = new Phaser.Text(game, this.x, this.y, this.owningPlayer.GetPlayerName(), style);
            this.coolDownLabel.stroke = '#000000';
            this.coolDownLabel.strokeThickness = 4;
            this.coolDownLabel.anchor.set(0.5);
            this.pieceGroup.add(this.coolDownLabel);

            //Should be over-set by network later
            this.lastTimeStampLandedOnTile = Date.now();

            //Put the piece on the spawn point.
            this.PlaceOnCellAtIndex(xGridSpawnIndex, yGridSpawnIndex);

            //Set the sprite anchor so that the piece appears to sit on the cell
            this.anchor = new Phaser.Point(0.5, 0.86);

            //Initialised movement pattern array, uses derived class implementation of base method
            this.movementPatternIndices = this.GetCandidateMovementIndices();

            //Setup mouse selection, if this is a local piece
            if (this.owningPlayer.IsLocalPlayer()) {
                this.inputEnabled = true;
                this.events.onInputDown.add(this.ToggleSelection, this);
            }

            //Add this piece to the piece group
            this.pieceGroup.add(this);
        }

        //Frame by frame logic update
        update() {
            //Update the piece specific UI
            if (this.nameLabel != null) {
                this.nameLabel.position.x = this.position.x;
                this.nameLabel.position.y = this.position.y + this.height / 8;
            }

            if (this.coolDownLabel != null) {
                this.coolDownLabel.position.x = this.position.x;
                this.coolDownLabel.position.y = this.position.y + this.height / 4;
                this.coolDownLabel.text = Math.round(this.GetMovmentCooldownTimerValue()).toString(); //Cooldown label based on timestamp
            }

        }

        //Called when piece is clicked on, toggles selection grid on/off
        private ToggleSelection = (): void => {
            //Only do selection/highlights if we're local
            if (!this.owningPlayer.IsLocalPlayer()) {
                return;
            }

            if (this.IsSelectable()) {
                if (this.IsSelected()) {
                    this.DeSelectPiece();
                }
                else {
                    this.SelectPiece();
                }
            }
        }

        //Called when the player clicks the piece to highlight it for movement
        private SelectPiece = (): void => {
            //Only do selection/highlights if we're local
            if (!this.owningPlayer.IsLocalPlayer()) {
                return;
            }

            this.isSelected = true;
            this.ShowMovementHighlightSquares();
        }

        //Unselects the piece, hides the movement
        private DeSelectPiece = (): void => {
            //Only do selection/highlights if we're local
            if (!this.owningPlayer.IsLocalPlayer()) {
                return;
            }

            this.isSelected = false;
            this.HideMovementHighlightSquares();
        }

        //Whether or not this piece is currently selected for movemetn
        private IsSelected = (): boolean => {
            return this.isSelected;
        }

        //Shows the squares this piece can move to by placing and enabling overlay squares on the grid
        private ShowMovementHighlightSquares = (): void => {

            //Only do selection/highlights if we're local
            if (!this.owningPlayer.IsLocalPlayer()) {
                return;
            }

            //The movement highlights are lazily created, so they, (or even their containing array,) may not exist when we get to the function, so we just create them.
            if (this.movementPatternHighlights == null) {
                this.movementPatternHighlights = [];
            }

            //If the movement pattern dosent have enough sprites for the indices, make em. This should really only happen the first time this is called, as pieces dont change their movement pattern, but even if it happens more, it's no biggie.
            for (var i = 0; i < this.movementPatternIndices.length; i++) {
                if (this.movementPatternHighlights.length <= i) {
                    this.movementPatternHighlights.push(new CellHighlight(this.game, this));
                    this.boardGroup.add(this.movementPatternHighlights[i]);
                    this.movementPatternHighlights[i].kill();
                }
            }

            //Set the now definately existing movement tile highlights to be visible and be in the correct position
            for (var i = 0; i < this.movementPatternIndices.length; i++) {
                try {
                    var cellToPlaceOn: Cell = this.grid.GetCellAtIndex(this.currentCell.GetGridIndex().x + this.movementPatternIndices[i].x, this.currentCell.GetGridIndex().y + this.movementPatternIndices[i].y);
                    if (cellToPlaceOn != null) {
                        this.movementPatternHighlights[i].PlaceOnCell(cellToPlaceOn);
                        this.movementPatternHighlights[i].revive();
                    }
                }
                catch (error){//Ignore the highlight if we cant find a cell, as we just want to not highlight it
                }
            }
        }

        //Hides the movement highlights for when the piece is deselected. Tiles arnt deleted, as they get re-used, just disabled
        private HideMovementHighlightSquares = (): void => {
            //Kill all the movement pattern highlights, they get revived in the ShowMovementHighlightSquares method
            for (var i = 0; i < this.movementPatternHighlights.length; i++) {
                this.movementPatternHighlights[i].kill();
            }
        }

        //Called from the CellHighlights in response to mouse clicks
        public SelectionResponse = (cellSelected: Cell): void => {
            if (this.IsSelected()) {
                this.RequestMovement(GameState.socket, cellSelected);
                this.DeSelectPiece();
            }
        }

        //Send a packet to the server and request movement. This is basically calling "Move()" over the network, with verification n shiz.
        public RequestMovement = (connectionSocket: SocketIOClient.Socket, movementRequestCell: Cell): void => {
            connectionSocket.emit('movementrequest', Date.now(), this.owningPlayer.GetPlayerNetworkID(), this.serverPieceIndexMapping, this.GetPieceType(), movementRequestCell.GetGridIndex().x, movementRequestCell.GetGridIndex().y);
        }

        //Purge all the piece data to prepare for destruction
        public PurgePiece = (): void => {
            if (this.currentCell != null) {
                this.currentCell.NotifyCellIsNowEmpty();
            }

            this.body = null;
            this.nameLabel.destroy();
            this.coolDownLabel.destroy();
            this.owningPlayer = null;
        }

        //Set the text of the piece name label
        public UpdatePieceNameLabel = (name: string): void => {
            this.nameLabel.text = name;
        }

        //Do a move to a certain cell over a certain time, normally in response to a movement packet
        public MakeMove = (cellToMoveTo: Cell, timestampToArrive: number) => {

            //Do sanity checks
            if (cellToMoveTo == null) {
                throw new ReferenceError('Null Cell to move to');
            }
            if (this.targetCell != null) {
                throw new Error('Target cell needs to be null to make a move, is target cell is not null, piece should be in motion');
            }

            this.DeSelectPiece();
            this.RemoveFromCell();

            var movementTween: Phaser.Tween = this.game.add.tween(this.position);
            console.log('Move Timestamp : ' + Date.now());
            var timeMoveTakes = timestampToArrive - Date.now();
            if (timeMoveTakes < 0) {
                timeMoveTakes = 0;
            }
            movementTween.to({ x: cellToMoveTo.GetCenter().x, y: cellToMoveTo.GetCenter().y }, timeMoveTakes);

            this.targetCell = cellToMoveTo;
            movementTween.onComplete.add(this.ArriveAtTargetCell, this);
            movementTween.start();
        }

        //Called from make move as a tween callback, places to piece on the cell.
        private ArriveAtTargetCell = () => {
            this.PlaceOnCell(this.targetCell);
            this.SetCellAllignedPosition();
            this.targetCell = null;
        }

        //Put the piece on a cell at a specified index
        public PlaceOnCellAtIndex = (xIndex: number, yIndex: number): void => {
            this.PlaceOnCell(this.grid.GetCellAtIndex(xIndex, yIndex));
        }

        //Put the piece on the cell, letting the cell and piece know about the other
        public PlaceOnCell = (cell: Cell): void => {
            if (this.currentCell != null) {
                this.currentCell.NotifyCellIsNowEmpty();
            }
            cell.NotifyPieceOnCell(this);
            this.currentCell = cell;
            this.SetCellAllignedPosition();

            this.SetTimestampOfLastCellLanding(Date.now());
        }

        //Remove this piece from whatever cell it's on
        public RemoveFromCell = (): void => {
            if (this.currentCell != null) {
                this.currentCell.NotifyCellIsNowEmpty();
                this.currentCell = null;
            }
        }

        //Remove this piece from the grid, and destroy the sprite.
        public RemoveFromGame = (): void => {
            this.RemoveFromCell();
            this.nameLabel.destroy();
            this.coolDownLabel.destroy();
            this.destroy();
        }

        //Puts the piece at the correct screen position according to what cell it's sitting on
        private SetCellAllignedPosition = (): void => {
            this.position.x = this.currentCell.GetCenter().x;
            this.position.y = this.currentCell.GetCenter().y;   
        }

        //Sets the last time that this piece landed on a cell, from the server normally. Used for movement colldowns
        public SetTimestampOfLastCellLanding = (timeStamp: number): void => {
            this.lastTimeStampLandedOnTile = timeStamp;
        }

        //Get the value of the cooldown timer, based on the current timestamp and the last placed on cell timestamp
        private GetMovmentCooldownTimerValue = (): number => {
            var coolDownTimerVal: number = ((this.lastTimeStampLandedOnTile + (this.movementCoolDownInSeconds * 1000)) - Date.now()) / 1000; //Is ms by default
            if (coolDownTimerVal < 0) {
                coolDownTimerVal = 0;
            }
            return coolDownTimerVal;
        }

        //Return whether or not the local cooldown timer thinks the piece is selectable, ie the timer is <= 0 and we're not moving
        public IsSelectable = (): boolean => {
            if ((this.GetMovmentCooldownTimerValue() <= 0) && (this.currentCell != null)) {
                return true;
            }
            return false;
        }

        //Bring the name label to the top of the view
        public BringNameLableToTop = (): void => {
            this.nameLabel.bringToTop();
        }

        //Bring the cooldown label to the top of the view
        public BringCooldownLabelToTop = (): void => {
            this.coolDownLabel.bringToTop();
        }

        //Get what type of piece this is
        public GetPieceType = (): PieceType => {
            return this.pieceType;
        }

        //Get the piece map index as it is known to the server
        public GetServerPieceMapIndex = (): number => {
            return this.serverPieceIndexMapping;
        }

        //Base class must implement a method to set the specific piece type movement indices, relative to piece current position. 
        protected abstract GetCandidateMovementIndices () : Array<Phaser.Point>;

    }
}