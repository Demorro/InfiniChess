﻿module InfiniChessClient {
    //A pawn piece.
    export class Pawn extends Piece {
        constructor(game: Phaser.Game, grid: Grid, serverPieceIndexMapping: number, spawnIndexOnGrid: Phaser.Point, owningPlayer: Player, pieceGroup: Phaser.Group, boardGroup: Phaser.Group, uiGroup: Phaser.Group) {
            super(game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, Assets.ChessPieces.assetKey, new Phaser.Rectangle(21, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);

            this.pieceType = PieceType.Pawn;
            this.movementCoolDownInSeconds = 3;
        }

        protected GetCandidateMovementIndices(): Array<Phaser.Point> {
            var pts: Array<Phaser.Point> = [];

            //Load all of the possible pawn movement squares
            pts.push(new Phaser.Point(0, 1));
            pts.push(new Phaser.Point(0, -1));
            pts.push(new Phaser.Point(1, 0));
            pts.push(new Phaser.Point(-1, 0));

            return pts;
        }
    }
}