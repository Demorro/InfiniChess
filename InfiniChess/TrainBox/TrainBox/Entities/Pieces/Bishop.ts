﻿module InfiniChessClient {
    //A bishop piece.
    export class Bishop extends Piece {
        constructor(game: Phaser.Game, grid: Grid, serverPieceIndexMapping: number, spawnIndexOnGrid: Phaser.Point, owningPlayer: Player, pieceGroup: Phaser.Group, boardGroup: Phaser.Group, uiGroup: Phaser.Group) {
            super(game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, Assets.ChessPieces.assetKey, new Phaser.Rectangle(307, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);

            this.pieceType = PieceType.Bishop;
            this.movementCoolDownInSeconds = 5;
        }

        protected GetCandidateMovementIndices(): Array<Phaser.Point> {
            var pts: Array<Phaser.Point> = [];

            var range: number = 10;

            //Load all of the possible bishop movement squares
            for (var i = 1; i <= range; i++) {
                pts.push(new Phaser.Point(i, i));
            }
            for (var j = 1; j <= range; j++) {
                pts.push(new Phaser.Point(-j, j));
            }
            for (var k = 1; k <= range; k++) {
                pts.push(new Phaser.Point(k, -k));
            }
            for (var l = 1; l <= range; l++) {
                pts.push(new Phaser.Point(-l, -l));
            }

            return pts;
        }
    }
}