﻿module InfiniChessClient {
    //A queen piece.
    export class Queen extends Piece {
        constructor(game: Phaser.Game, grid: Grid, serverPieceIndexMapping: number, spawnIndexOnGrid: Phaser.Point, owningPlayer: Player, pieceGroup: Phaser.Group, boardGroup: Phaser.Group, uiGroup: Phaser.Group) {
            super(game, grid, serverPieceIndexMapping, spawnIndexOnGrid.x, spawnIndexOnGrid.y, Assets.ChessPieces.assetKey, new Phaser.Rectangle(402, 0, 80, 126), owningPlayer, pieceGroup, boardGroup, uiGroup);

            this.pieceType = PieceType.Queen;
            this.movementCoolDownInSeconds = 8;
        }

        protected GetCandidateMovementIndices(): Array<Phaser.Point> {
            var pts: Array<Phaser.Point> = [];

            var range: number = 10;

            //Load all of the possible queen movement squares
            for (var i = 1; i <= range; i++) {
                pts.push(new Phaser.Point(0, i));
            }
            for (var j = 1; j <= range; j++) {
                pts.push(new Phaser.Point(0, -j));
            }
            for (var k = 1; k <= range; k++) {
                pts.push(new Phaser.Point(k, 0));
            }
            for (var l = 1; l <= range; l++) {
                pts.push(new Phaser.Point(-l, 0));
            }

            for (var a = 1; a <= range; a++) {
                pts.push(new Phaser.Point(a, a));
            }
            for (var b = 1; b <= range; b++) {
                pts.push(new Phaser.Point(-b, b));
            }
            for (var c = 1; c <= range; c++) {
                pts.push(new Phaser.Point(c, -c));
            }
            for (var d = 1; d <= range; d++) {
                pts.push(new Phaser.Point(-d, -d));
            }

            return pts;
        }
    }
}