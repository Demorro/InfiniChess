var InfiniChessClient;
(function (InfiniChessClient) {
    var Player = (function () {
        function Player(grid, gameState, playerID) {
            var _this = this;
            this.playerName = "";
            this.UpdatePieceInformationFromNetwork = function (pieceMapIndex, pieceType, xPos, yPos, playerNameLabelGroup) {
                if (_this.pieces.length <= pieceMapIndex) {
                    _this.GivePlayerNewPiece(_this.gameState.game, pieceType, xPos, yPos, playerNameLabelGroup);
                    return true;
                }
                else {
                    _this.pieces[pieceMapIndex].PlaceOnCellAtIndex(xPos, yPos);
                    return false;
                }
            };
            this.UpdatePlayerPropertiesFromNetwork = function (playerName) {
                for (var _i = 0, _a = _this.pieces; _i < _a.length; _i++) {
                    var piece = _a[_i];
                    if (piece != null) {
                        piece.UpdatePieceNameLabel(playerName);
                    }
                }
            };
            this.GetPlayerName = function () {
                return _this.playerName;
            };
            this.GivePlayerNewPiece = function (game, piece, pieceXIndex, pieceYIndex, nameLabelGroup) {
                var pieceToAdd;
                if (piece == InfiniChessClient.PieceType.Pawn) {
                    pieceToAdd = new InfiniChessClient.Pawn(game, _this.grid, new Phaser.Point(pieceXIndex, pieceYIndex), _this, nameLabelGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Knight) {
                    pieceToAdd = new InfiniChessClient.Knight(game, _this.grid, new Phaser.Point(pieceXIndex, pieceYIndex), _this, nameLabelGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Bishop) {
                    pieceToAdd = new InfiniChessClient.Bishop(game, _this.grid, new Phaser.Point(pieceXIndex, pieceYIndex), _this, nameLabelGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Rook) {
                    pieceToAdd = new InfiniChessClient.Rook(game, _this.grid, new Phaser.Point(pieceXIndex, pieceYIndex), _this, nameLabelGroup);
                }
                else if (piece == InfiniChessClient.PieceType.Queen) {
                    pieceToAdd = new InfiniChessClient.Queen(game, _this.grid, new Phaser.Point(pieceXIndex, pieceYIndex), _this, nameLabelGroup);
                }
                else {
                    throw new Error('Incorrect Piece Type, GivePlayerNewPiece, Player.ts');
                }
                game.add.existing(pieceToAdd);
                _this.pieces.push(pieceToAdd);
            };
            this.gameState = gameState;
            this.grid = grid;
            this.playerID = playerID;
            this.pieces = [];
        }
        return Player;
    }());
    InfiniChessClient.Player = Player;
})(InfiniChessClient || (InfiniChessClient = {}));
