﻿module InfiniChessClient
{
    export class IdentifierPathPair{
        
        assetKey: string;
        path: string;

        constructor(_assetKey: string, _path: string) {
            this.assetKey = _assetKey;
            this.path = _path;
        }
    }

    //Class to handle the management and loading of assets. Assets need to be preloaded before they are added to the canvas
    export class Assets {
        //List of the asset keys paired to their reletive paths
        //Images
        public static LoadingBar: IdentifierPathPair = new InfiniChessClient.IdentifierPathPair('loadingBar', 'assets/sprites/loadingBar.png');
        public static ChessGrid: IdentifierPathPair = new InfiniChessClient.IdentifierPathPair('chessGrid', 'assets/sprites/ChessGrid.png');
        public static CellHighlight: IdentifierPathPair = new InfiniChessClient.IdentifierPathPair('cellHighlight', 'assets/sprites/CellHighlight.png');
        public static ChessPieces: IdentifierPathPair = new InfiniChessClient.IdentifierPathPair('chessPieces', 'assets/sprites/Pieces.png');

        public static Button: IdentifierPathPair = new InfiniChessClient.IdentifierPathPair('button', 'assets/sprites/Button.png');

        //Audio
        public static PlaceTrackAudio: IdentifierPathPair = new InfiniChessClient.IdentifierPathPair('placeTrackAudio', 'assets/audio/PlaceTrack.mp3');

        //Load all the assets into the game, except for the loading bar. For use in the preloadedstate. Needs to be kept updated when a new asset is added
        public static LoadAllAssets(state: Phaser.State)
        {
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.ChessGrid);
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.CellHighlight); 
            InfiniChessClient.Assets.LoadImage(state, InfiniChessClient.Assets.ChessPieces);
            InfiniChessClient.Assets.LoadSpriteSheet(state, InfiniChessClient.Assets.Button, 350,120);
            InfiniChessClient.Assets.LoadSound(state, InfiniChessClient.Assets.PlaceTrackAudio);
        }

        //static functions for preloading images into memory before they are stage added.
        public static LoadImage(state: Phaser.State, imageToLoad: IdentifierPathPair)
        {
            state.load.image(imageToLoad.assetKey, imageToLoad.path);
        }
        public static LoadSound(state: Phaser.State, soundToLoad: IdentifierPathPair)
        {
            state.load.audio(soundToLoad.assetKey, soundToLoad.path);
        }
        public static LoadSpriteSheet(state: Phaser.State, spriteSheetToLoad: IdentifierPathPair, frameWidth : number, frameHeight : number, frameNum : number = -1)
        {
            if (frameNum == -1) {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight);
            }
            else {
                state.load.spritesheet(spriteSheetToLoad.assetKey, spriteSheetToLoad.path, frameWidth, frameHeight, frameNum);
            }
        }
    }
}