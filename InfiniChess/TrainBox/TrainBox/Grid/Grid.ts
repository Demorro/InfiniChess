﻿module InfiniChessClient {
    //The square grid, contains an array of cells and accesor methods for um ... accesing them.
    export class Grid {

        private cells: Cell[][];
        gridImage: Phaser.TileSprite;

        //The width/height of the cells, in pixels (hopefully).
        public static CELLXPIXELS = 94;
        public static CELLYPIXELS = 50;


        constructor(game: Phaser.Game, state: Phaser.State, xGridWidth: number, yGridWidth: number) {
            this.PopulateGridWithCells(game, xGridWidth, yGridWidth);

            this.gridImage = state.add.tileSprite(0, 0, xGridWidth * Grid.CELLXPIXELS, yGridWidth * Grid.CELLYPIXELS,  InfiniChessClient.Assets.ChessGrid.assetKey);
            //Set the background logic disabled for performance
            this.gridImage.body = null;
        }

        //Called from constructor, so we can bind 'this' reference properly
        private PopulateGridWithCells = (game: Phaser.Game, xGridWidth: number, yGridWidth: number) => {

            this.cells = [];

            //The provided x and y numbers are rounded so they're ints, as javascript dosen't "do" ints, the high maintenance bitch.
            xGridWidth = Math.round(xGridWidth);
            yGridWidth = Math.round(yGridWidth);

            //Shove the cells in, creating Y-arrays as we go. Cells are given reference to this, and we use the CELLX/YPIXEL constants to hook up cell positions
            for (var i = 0; i < xGridWidth; i++) {
                this.cells[i] = [];
                for (var j = 0; j < yGridWidth; j++) {
                    var newCell: Cell = new Cell(game, this, new Phaser.Point(i, j), new Phaser.Point(i * Grid.CELLXPIXELS, j * Grid.CELLYPIXELS));
                    this.cells[i].push(newCell);
                }
            }
        }

        //Directly access cell at specified index
        public GetCellAtIndex = (indexX: number, indexY: number): Cell => {

            //Sanity, also stops move highlights going off the board
            if ((indexX < 0) || (indexY < 0)) {return null;}

            //Round numbers to ints
            indexX = Math.floor(indexX);
            indexY = Math.floor(indexY);

            if (this.cells.length > indexX) {
                if (this.cells[indexX].length > indexY) {
                    return this.cells[indexX][indexY];
                }
            }

            //If we got here, we tried to access out of array bounds
            throw new Error('Attempted to access out of bounds cell');
        }

        //Access the cell under a world position, most useful for getting a cell at the mouse position I'd imagine, although if theres camera translation screen->worldspace will need to be done.
        public GetCellAtWorldPoint = (worldPosition: Phaser.Point) => {
            //Derive x/y indices from world positions
            var xIndex : number = worldPosition.x / Grid.CELLXPIXELS;
            var yIndex : number = worldPosition.y / Grid.CELLYPIXELS;

            //Round the indices down.
            xIndex = Math.floor(xIndex);
            yIndex = Math.floor(yIndex);

            if (this.cells.length > xIndex) {
                if (this.cells[xIndex].length > yIndex) {
                    return this.cells[xIndex][yIndex];
                }
            }
            
            //If we got here, we tried to access out of array bounds, and should error.
            throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtWorldPoint()");
        }

        //Gets how many tiles the grid has horizontally
        public GetGridWidth = (): number => {
            return this.cells.length;
        }

        //Gets how many tiles the grid has vertically
        public GetGridHeight = (): number => {
            if (this.cells.length == 0) {
                return 0;
            }
            return this.cells[0].length;
        }
    }
}