﻿module InfiniChessClient {
    //A highlight that is placed on the cell when deciding to move a piece. Can then be clicked on for selection.
    export class CellHighlight extends Phaser.Sprite {

        private highlightingPiece: Piece;
        private highlightingCell: Cell;

        constructor(_game: Phaser.Game, highlightPiece: Piece) {
            super(_game, 0, 0);

            this.loadTexture(Assets.CellHighlight.assetKey);
            this.anchor = new Phaser.Point(0.5, 0.5);

            this.highlightingPiece = highlightPiece;

            this.inputEnabled = true;
            this.events.onInputDown.add(this.SelectCell, this);
        }

        public PlaceOnCell = (highlightCell: Cell): void => {
            this.highlightingCell = highlightCell;
            this.x = this.highlightingCell.GetCenter().x;
            this.y = this.highlightingCell.GetCenter().y;
        }

        //The player has just clicked this highlight, and this cell was selected
        private SelectCell = (): void => {
            this.highlightingPiece.SelectionResponse(this.highlightingCell);
        }

    }
}