var InfiniChessClient;
(function (InfiniChessClient) {
    var Cell = (function () {
        function Cell(_game, _parentGrid, _gridIndex, _worldPosition) {
            var _this = this;
            this.sittingPiece = null;
            this.NotifyPieceOnCell = function (piece) {
                _this.sittingPiece = piece;
            };
            this.NotifyCellIsNowEmpty = function () {
                _this.sittingPiece = null;
            };
            this.GetPieceOnCell = function () {
                return _this.sittingPiece;
            };
            this.GetCenter = function () {
                var center = _this.position;
                center.x += InfiniChessClient.Grid.CELLXPIXELS / 2;
                center.y += InfiniChessClient.Grid.CELLYPIXELS / 2;
                return center;
            };
            this.GetNorthCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x, _this.gridIndex.y - 1));
            };
            this.GetEastCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x + 1, _this.gridIndex.y));
            };
            this.GetSouthCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x, _this.gridIndex.y + 1));
            };
            this.GetWestCell = function () {
                return _this.parentGrid.GetCellAtIndex(new Phaser.Point(_this.gridIndex.x - 1, _this.gridIndex.y));
            };
            this.GetEdgePoint = function (direction) {
                direction = Math.floor(direction);
                if (direction < 0) {
                    direction = 0;
                }
                if (direction > 3) {
                    direction = 3;
                }
                if (direction == 0) {
                    return new Phaser.Point(_this.position.x + InfiniChessClient.Grid.CELLXPIXELS / 2, _this.position.y);
                }
                else if (direction == 1) {
                    return new Phaser.Point(_this.position.x + InfiniChessClient.Grid.CELLXPIXELS, _this.position.y + InfiniChessClient.Grid.CELLYPIXELS / 2);
                }
                else if (direction == 2) {
                    return new Phaser.Point(_this.position.x + InfiniChessClient.Grid.CELLXPIXELS / 2, _this.position.y + InfiniChessClient.Grid.CELLYPIXELS);
                }
                else if (direction == 3) {
                    return new Phaser.Point(_this.position.x, _this.position.y + InfiniChessClient.Grid.CELLYPIXELS / 2);
                }
                else {
                    throw new Error("Incorrect Direction. GetEdgePoint(), Cell.ts");
                }
            };
            this.parentGrid = _parentGrid;
            this.gridIndex = _gridIndex;
            this.position = _worldPosition;
            this.game = _game;
            this.sittingPiece = null;
        }
        return Cell;
    }());
    InfiniChessClient.Cell = Cell;
})(InfiniChessClient || (InfiniChessClient = {}));
