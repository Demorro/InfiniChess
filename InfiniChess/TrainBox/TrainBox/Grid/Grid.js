var InfiniChessClient;
(function (InfiniChessClient) {
    var Grid = (function () {
        function Grid(game, state, xGridWidth, yGridWidth) {
            var _this = this;
            this.PopulateGridWithCells = function (game, xGridWidth, yGridWidth) {
                _this.cells = [];
                xGridWidth = Math.round(xGridWidth);
                yGridWidth = Math.round(yGridWidth);
                for (var i = 0; i < xGridWidth; i++) {
                    _this.cells[i] = [];
                    for (var j = 0; j < yGridWidth; j++) {
                        var newCell = new InfiniChessClient.Cell(game, _this, new Phaser.Point(i, j), new Phaser.Point(i * Grid.CELLXPIXELS, j * Grid.CELLYPIXELS));
                        _this.cells[i].push(newCell);
                    }
                }
            };
            this.GetCellAtIndex = function (index) {
                index = index.floor();
                if (_this.cells.length > index.x) {
                    if (_this.cells[index.x].length > index.y) {
                        return _this.cells[index.x][index.y];
                    }
                }
                return null;
            };
            this.GetCellAtWorldPoint = function (worldPosition) {
                var xIndex = worldPosition.x / Grid.CELLXPIXELS;
                var yIndex = worldPosition.y / Grid.CELLYPIXELS;
                xIndex = Math.floor(xIndex);
                yIndex = Math.floor(yIndex);
                if (_this.cells.length > xIndex) {
                    if (_this.cells[xIndex].length > yIndex) {
                        return _this.cells[xIndex][yIndex];
                    }
                }
                throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtWorldPoint()");
            };
            this.GetGridWidth = function () {
                return _this.cells.length;
            };
            this.GetGridHeight = function () {
                if (_this.cells.length == 0) {
                    return 0;
                }
                return _this.cells[0].length;
            };
            this.PopulateGridWithCells(game, xGridWidth, yGridWidth);
            this.gridImage = state.add.tileSprite(0, 0, xGridWidth * Grid.CELLXPIXELS, yGridWidth * Grid.CELLYPIXELS, InfiniChessClient.Assets.ChessGrid.assetKey);
            this.gridImage.body = null;
        }
        Grid.CELLXPIXELS = 94;
        Grid.CELLYPIXELS = 50;
        return Grid;
    }());
    InfiniChessClient.Grid = Grid;
})(InfiniChessClient || (InfiniChessClient = {}));
