﻿module InfiniChessClient {
    //A single cell on the grid, can hold a reference to a track.
    export class Cell {

        private parentGrid: Grid; //The grid that this cell is contained in.
        private gridIndex: Phaser.Point; //The index of this Cell in its parent grid container
        private position: Phaser.Point; //The position of the center of this cell in world space

        private sittingPiece: Piece = null; //Whatever piece is on the cell, dont use this for game logic. Null if its nothing.

        private game: Phaser.Game; //Reference to the game

        constructor(_game: Phaser.Game, _parentGrid: Grid, _gridIndex: Phaser.Point, _worldPosition: Phaser.Point) {
            this.parentGrid = _parentGrid;
            this.gridIndex = _gridIndex;
            this.position = _worldPosition;
            this.game = _game;
            this.sittingPiece = null;
        }

        public NotifyPieceOnCell = (piece: Piece): void => {
            this.sittingPiece = piece;
        }

        public NotifyCellIsNowEmpty = (): void => {
            this.sittingPiece = null;
        }

        public GetPieceOnCell = (): Piece => {
            return this.sittingPiece;
        }

        public GetCenter = (): Phaser.Point => {
            var center = new Phaser.Point(this.position.x, this.position.y);
            center.x += Grid.CELLXPIXELS / 2;
            center.y += Grid.CELLYPIXELS / 2;
            return center;
        }

        public GetNorthCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(this.gridIndex.x, this.gridIndex.y - 1);
        }
        public GetEastCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(this.gridIndex.x + 1, this.gridIndex.y);
        }
        public GetSouthCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(this.gridIndex.x, this.gridIndex.y + 1);
        }
        public GetWestCell = (): Cell => {
            return this.parentGrid.GetCellAtIndex(this.gridIndex.x - 1, this.gridIndex.y);
        }

        public GetGridIndex = (): Phaser.Point => {
            return this.gridIndex;
        }

        //Returns the position in the center of one edge, as defined by direction (0-3, N-W)
        public GetEdgePoint = (direction: number): Phaser.Point => {
            direction = Math.floor(direction);
            if (direction < 0) {
                direction = 0;
            }
            if (direction > 3) {
                direction = 3;
            }

            if (direction == 0) {
                return new Phaser.Point(this.position.x + Grid.CELLXPIXELS/2, this.position.y);
            }
            else if (direction == 1) {
                return new Phaser.Point(this.position.x + Grid.CELLXPIXELS, this.position.y + Grid.CELLYPIXELS/2);
            }
            else if (direction == 2) {
                return new Phaser.Point(this.position.x + Grid.CELLXPIXELS/2, this.position.y + Grid.CELLYPIXELS);
            }
            else if (direction == 3) {
                return new Phaser.Point(this.position.x, this.position.y + Grid.CELLYPIXELS/2);
            }
            else {
                throw new Error("Incorrect Direction. GetEdgePoint(), Cell.ts");
            }
        }
    }
}