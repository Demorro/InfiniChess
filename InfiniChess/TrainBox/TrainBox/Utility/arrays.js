var InfiniChessClient;
(function (InfiniChessClient) {
    function indexOf(array, item, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        var length = array.length;
        for (var i = 0; i < length; i++) {
            if (equals(array[i], item)) {
                return i;
            }
        }
        return -1;
    }
    InfiniChessClient.indexOf = indexOf;
    function lastIndexOf(array, item, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        var length = array.length;
        for (var i = length - 1; i >= 0; i--) {
            if (equals(array[i], item)) {
                return i;
            }
        }
        return -1;
    }
    InfiniChessClient.lastIndexOf = lastIndexOf;
    function contains(array, item, equalsFunction) {
        return indexOf(array, item, equalsFunction) >= 0;
    }
    InfiniChessClient.contains = contains;
    function remove(array, item, equalsFunction) {
        var index = indexOf(array, item, equalsFunction);
        if (index < 0) {
            return false;
        }
        array.splice(index, 1);
        return true;
    }
    InfiniChessClient.remove = remove;
    function frequency(array, item, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        var length = array.length;
        var freq = 0;
        for (var i = 0; i < length; i++) {
            if (equals(array[i], item)) {
                freq++;
            }
        }
        return freq;
    }
    InfiniChessClient.frequency = frequency;
    function equals(array1, array2, equalsFunction) {
        var equals = equalsFunction || InfiniChessClient.defaultEquals;
        if (array1.length !== array2.length) {
            return false;
        }
        var length = array1.length;
        for (var i = 0; i < length; i++) {
            if (!equals(array1[i], array2[i])) {
                return false;
            }
        }
        return true;
    }
    InfiniChessClient.equals = equals;
    function copy(array) {
        return array.concat();
    }
    InfiniChessClient.copy = copy;
    function swap(array, i, j) {
        if (i < 0 || i >= array.length || j < 0 || j >= array.length) {
            return false;
        }
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        return true;
    }
    InfiniChessClient.swap = swap;
    function toString(array) {
        return '[' + array.toString() + ']';
    }
    InfiniChessClient.toString = toString;
    function forEach(array, callback) {
        for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
            var ele = array_1[_i];
            if (callback(ele) === false) {
                return;
            }
        }
    }
    InfiniChessClient.forEach = forEach;
})(InfiniChessClient || (InfiniChessClient = {}));
