var InfiniChessClient;
(function (InfiniChessClient) {
    var Camera = (function () {
        function Camera(game, startX, startY, stageWidth, stageHeight) {
            var _this = this;
            this.camMoveSpeed = 0.3;
            this.isScrolling = false;
            this.Update = function (deltaTime) {
                _this.isScrolling = false;
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.W)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.UP))) {
                    _this.gameRef.camera.y -= _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.S)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.DOWN))) {
                    _this.gameRef.camera.y += _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.A)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.LEFT))) {
                    _this.gameRef.camera.x -= _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
                if ((_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.D)) || (_this.gameRef.input.keyboard.isDown(Phaser.Keyboard.RIGHT))) {
                    _this.gameRef.camera.x += _this.camMoveSpeed * deltaTime;
                    _this.isScrolling = true;
                }
            };
            this.ScreenToWorldSpace = function (screenPos) {
                var worldPos = new Phaser.Point(screenPos.x, screenPos.y);
                worldPos.x += _this.gameRef.camera.x;
                worldPos.y += _this.gameRef.camera.y;
                return worldPos;
            };
            this.IsScrolling = function () {
                return _this.isScrolling;
            };
            game.camera.setPosition(startX * 2, startY * 2);
            game.camera.bounds = new Phaser.Rectangle(startX * 2, startY * 2, stageWidth, stageHeight);
            this.gameRef = game;
        }
        return Camera;
    }());
    InfiniChessClient.Camera = Camera;
})(InfiniChessClient || (InfiniChessClient = {}));
