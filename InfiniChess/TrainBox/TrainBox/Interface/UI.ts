﻿module InfiniChessClient {
    //Display the UI for the local player
    export class UI {

        private game: Phaser.Game;
        private localPlayer: Player;
        private uiGroup: Phaser.Group;

        private moneyLabel: Phaser.Text;

        constructor(game: Phaser.Game, localPlayer: Player, uiGroup: Phaser.Group) {
            this.game = game;
            this.localPlayer = localPlayer;
            this.uiGroup = uiGroup;

            var style = { font: "32px Arial", fill: "#fff", align: "center" };
            this.moneyLabel = new Phaser.Text(game, 50, 35, "$0", style);
            this.moneyLabel.stroke = '#000000';
            this.moneyLabel.strokeThickness = 4;
            this.moneyLabel.anchor.set(0.5);
            
            uiGroup.add(this.moneyLabel);
        }

        public UpdateMoney = (newMoney: number): void => {
            this.moneyLabel.text = "$" + Math.round(newMoney).toString();
        }
    }
}