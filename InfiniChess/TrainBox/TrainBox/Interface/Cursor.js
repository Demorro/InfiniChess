var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TrainBox;
(function (TrainBox) {
    var Cursor = (function (_super) {
        __extends(Cursor, _super);
        function Cursor(game, startSelectingCell, gameGrid) {
            var _this = this;
            _super.call(this, game, startSelectingCell.position.x, startSelectingCell.position.y, TrainBox.Assets.CrossHair.assetKey);
            this.TWEEN_TO_CELL_SPEED = 80;
            this.isTweening = false;
            this.currentRot = 0;
            this.isActive = true;
            this.SetSelectionPanelReference = function (_panel) {
                _this.selectionPanel = _panel;
            };
            this.SnapToSelectingCell = function (cell) {
                _this.position = cell.position;
                _this.currentSelectedCell = cell;
            };
            this.TweenToSelectingCell = function (cell) {
                _this.isTweening = true;
                _this.currentSelectedCell = cell;
                _this.moveTween.to({ x: cell.position.x, y: cell.position.y }, _this.TWEEN_TO_CELL_SPEED, Phaser.Easing.Quadratic.Out, true, 0);
                _this.moveTween.onComplete.add(_this.tweenComplete, _this);
                _this.moveTween.start();
            };
            this.Enable = function () {
                _this.isActive = true;
                _this.visible = true;
                _this.trackSelectionGhost.visible = true;
            };
            this.Disable = function () {
                _this.isActive = false;
                _this.visible = false;
                _this.trackSelectionGhost.visible = false;
            };
            this.Update = function (mousePos, cam) {
                var selectedCell = _this.gridReference.GetCellAtWorldPoint(new Phaser.Point(mousePos.x, mousePos.y));
                _this.SnapToSelectingCell(selectedCell);
                if (cam.IsScrolling()) {
                    _this.Disable();
                }
                else {
                    _this.Enable();
                }
                if (_this.deleteKey.isDown) {
                    _this.EraseTrack();
                }
                _this.RenderPlacementGhost();
            };
            this.EraseTrack = function () {
                if (_this.currentSelectedCell != null) {
                    if (_this.selectionPanel.GetSelectedItem() != null) {
                        if (_this.currentSelectedCell.GetOccupyingTrack() != null) {
                            if (_this.currentSelectedCell.GetOccupyingTrack().CanBeModified() == false) {
                                return;
                            }
                        }
                        if (_this.currentSelectedCell.EraseTrack()) {
                            _this.placeRailSound = _this.game.sound.play(TrainBox.Assets.PlaceTrackAudio.assetKey);
                        }
                    }
                }
            };
            this.PlaceTrack = function () {
                if (_this.game.input.activePointer.isDown) {
                    if (_this.game.input.activePointer.button == Phaser.Mouse.LEFT_BUTTON) {
                        if (_this.currentSelectedCell != null) {
                            if (_this.selectionPanel.GetSelectedItem() != null) {
                                if (_this.currentSelectedCell.GetOccupyingTrack() != null) {
                                    if (_this.currentSelectedCell.GetOccupyingTrack().CanBeModified() == false) {
                                        return;
                                    }
                                }
                                if (_this.currentSelectedCell.PlaceTrack(_this.selectionPanel.GetSelectedItem().track, _this.currentRot)) {
                                    _this.placeRailSound = _this.game.sound.play(TrainBox.Assets.PlaceTrackAudio.assetKey);
                                }
                            }
                        }
                    }
                }
            };
            this.RotateTrack = function () {
                if (_this.game.input.activePointer.isDown) {
                    if (_this.game.input.activePointer.button == Phaser.Mouse.RIGHT_BUTTON) {
                        if (_this.currentSelectedCell != null) {
                            if (_this.selectionPanel.GetSelectedItem() != null) {
                                _this.currentRot += 1.0;
                                _this.currentRot = Math.floor(_this.currentRot);
                                if (_this.currentRot > 3) {
                                    _this.currentRot = 0;
                                }
                                _this.trackSelectionGhost.rotation = _this.currentRot * Math.PI / 2;
                            }
                        }
                    }
                }
            };
            this.RenderPlacementGhost = function () {
                if (_this.selectionPanel != null) {
                    if (_this.selectionPanel.GetSelectedItem() != null) {
                        if (_this.selectionPanel.GetSelectedItem().track.GetSmallKey() != _this.trackSelectionGhost.key) {
                            if (_this.selectionPanel.GetSelectedItem().track.GetSmallKey() != null) {
                                _this.trackSelectionGhost.loadTexture(_this.selectionPanel.GetSelectedItem().track.GetSmallKey());
                                _this.trackSelectionGhost.alpha = 0.7;
                                _this.trackSelectionGhost.visible = true;
                            }
                            else {
                                _this.trackSelectionGhost.visible = false;
                            }
                        }
                        _this.trackSelectionGhost.position.x = _this.position.x + _this.width / 2;
                        _this.trackSelectionGhost.position.y = _this.position.y + _this.height / 2;
                    }
                }
            };
            this.DoTweenCursorMovement = function (selectedCell) {
                if (_this.isTweening == false) {
                    if (selectedCell != _this.currentSelectedCell) {
                        _this.moveTween = _this.game.add.tween(_this);
                        if (Math.abs(selectedCell.gridIndex.x - _this.currentSelectedCell.gridIndex.x) >= (Math.abs(selectedCell.gridIndex.y - _this.currentSelectedCell.gridIndex.y))) {
                            if (selectedCell.gridIndex.x < _this.currentSelectedCell.gridIndex.x) {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetWestCell());
                            }
                            else {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetEastCell());
                            }
                        }
                        else {
                            if (selectedCell.gridIndex.y < _this.currentSelectedCell.gridIndex.y) {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetNorthCell());
                            }
                            else {
                                _this.TweenToSelectingCell(_this.currentSelectedCell.GetSouthCell());
                            }
                        }
                    }
                }
            };
            this.currentSelectedCell = startSelectingCell;
            this.gridReference = gameGrid;
            this.tweenComplete = function () {
                this.isTweening = false;
            };
            this.trackSelectionGhost = this.game.add.sprite(0, 0);
            this.trackSelectionGhost.anchor = new Phaser.Point(0.5, 0.5);
            this.inputEnabled = true;
            this.events.onInputDown.add(this.PlaceTrack, this);
            this.events.onInputOver.add(this.PlaceTrack, this);
            this.events.onInputDown.add(this.RotateTrack, this);
            this.deleteKey = game.input.keyboard.addKey(Phaser.Keyboard.DELETE);
        }
        return Cursor;
    })(Phaser.Sprite);
    TrainBox.Cursor = Cursor;
})(TrainBox || (TrainBox = {}));
