﻿module InfiniChessClient {
    //A wrapper around the camera controls, so we can limit them if we want, and have controls
    export class Camera {
        private gameRef: Phaser.Game; //Reference to the game so we can control the camera
        
        private camMoveSpeed: number = 0.3; //How fast the camera moves aboot'
        private isScrolling: boolean = false; //Whether or not the camera is moving

        //Pass in start positions
        constructor(game: Phaser.Game, startX: number, startY: number, stageWidth : number, stageHeight : number) {
            game.camera.setPosition(startX * 2, startY * 2);
            game.camera.bounds = new Phaser.Rectangle(startX * 2, startY * 2, stageWidth, stageHeight);
            this.gameRef = game;
        }

        Update = (deltaTime: number) => {
            this.isScrolling = false;

            //Move camera according to keyboard controls
            //Up, W and UpArrow
            if ((this.gameRef.input.keyboard.isDown(Phaser.Keyboard.W)) || (this.gameRef.input.keyboard.isDown(Phaser.Keyboard.UP))) {
                this.gameRef.camera.y -= this.camMoveSpeed * deltaTime;
                this.isScrolling = true;
            }
            //Down, S and DownArrow
            if ((this.gameRef.input.keyboard.isDown(Phaser.Keyboard.S)) || (this.gameRef.input.keyboard.isDown(Phaser.Keyboard.DOWN))) {
                this.gameRef.camera.y += this.camMoveSpeed * deltaTime;
                this.isScrolling = true;
            }
            //Up, A and LeftArrow
            if ((this.gameRef.input.keyboard.isDown(Phaser.Keyboard.A)) || (this.gameRef.input.keyboard.isDown(Phaser.Keyboard.LEFT))) {
                this.gameRef.camera.x -= this.camMoveSpeed * deltaTime;
                this.isScrolling = true;
            }
            //Up, D and RightArrow
            if ((this.gameRef.input.keyboard.isDown(Phaser.Keyboard.D)) || (this.gameRef.input.keyboard.isDown(Phaser.Keyboard.RIGHT))) {
                this.gameRef.camera.x += this.camMoveSpeed * deltaTime;
                this.isScrolling = true;
            }
        }

        public ScreenToWorldSpace = (screenPos: Phaser.Point): Phaser.Point => {

            var worldPos: Phaser.Point = new Phaser.Point(screenPos.x, screenPos.y);
            worldPos.x += this.gameRef.camera.x;
            worldPos.y += this.gameRef.camera.y;
            return worldPos;
        }

        public IsScrolling = (): boolean => {
            return this.isScrolling;
        }

        
    }
}