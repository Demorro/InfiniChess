var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TrainBox;
(function (TrainBox) {
    var PanelTrackSelectionItem = (function () {
        function PanelTrackSelectionItem(game, _track, _panel, parentSelectionPanel) {
            var _this = this;
            this.isSelected = false;
            this.BringPanelToTop = function () {
                _this.panel.bringToTop();
                _this.track.bringToTop();
            };
            this.OnMouseOver = function () {
                if (_this.isSelected == false) {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsertHighlight.assetKey);
                }
                else {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsertGreySelected.assetKey);
                }
            };
            this.OnMouseOff = function () {
                if (_this.isSelected == false) {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsert.assetKey);
                }
                else {
                    _this.panel.loadTexture(TrainBox.Assets.PanelInsertGrey.assetKey);
                }
            };
            this.OnMouseClick = function () {
                _this.selectionPanel.NotifyOfSelection(_this);
                _this.isSelected = true;
                _this.panel.loadTexture(TrainBox.Assets.PanelInsertGreySelected.assetKey);
            };
            this.Deselect = function () {
                _this.isSelected = false;
                _this.panel.loadTexture(TrainBox.Assets.PanelInsert.assetKey);
            };
            this.SetPosition = function (position) {
                console.log(position);
                _this.panel.cameraOffset.x = position.x;
                _this.panel.cameraOffset.y = position.y;
                _this.track.cameraOffset.x = position.x;
                _this.track.cameraOffset.y = position.y;
                if ((_this.track.key == TrainBox.Assets.CurveTrack1Large.assetKey) || (_this.track.key == TrainBox.Assets.CurveTrack1.assetKey)) {
                    _this.track.cameraOffset.x -= _this.track.width / 8;
                    _this.track.cameraOffset.y -= _this.track.height / 8;
                }
            };
            this.TweenToX = function (game, position, tweenSpeed) {
                game.add.tween(_this.panel.cameraOffset).to({ x: position }, tweenSpeed, Phaser.Easing.Back.InOut, true);
                if ((_this.track.key == TrainBox.Assets.CurveTrack1Large.assetKey) || (_this.track.key == TrainBox.Assets.CurveTrack1.assetKey)) {
                    game.add.tween(_this.track.cameraOffset).to({ x: position - (_this.track.width / 8) }, tweenSpeed, Phaser.Easing.Back.InOut, true);
                }
                else {
                    game.add.tween(_this.track.cameraOffset).to({ x: position }, tweenSpeed, Phaser.Easing.Back.InOut, true);
                }
            };
            this.track = _track;
            this.panel = _panel;
            this.track.fixedToCamera = true;
            this.panel.fixedToCamera = true;
            this.track.anchor = new Phaser.Point(0.5, 0.5);
            this.panel.anchor = new Phaser.Point(0.5, 0.5);
            this.panel.bringToTop();
            this.track.bringToTop();
            this.panel = game.add.existing(this.panel);
            this.track.loadTexture(this.track.GetSmallKey());
            this.track = game.add.existing(this.track);
            this.panel.inputEnabled = true;
            this.panel.events.onInputOver.add(this.OnMouseOver, this);
            this.panel.events.onInputOut.add(this.OnMouseOff, this);
            this.panel.events.onInputDown.add(this.OnMouseClick, this);
            this.selectionPanel = parentSelectionPanel;
        }
        PanelTrackSelectionItem.PANELXOFFSET = 82;
        PanelTrackSelectionItem.PANELYOFFSET = 70;
        PanelTrackSelectionItem.PANELYSPACING = 125;
        return PanelTrackSelectionItem;
    })();
    TrainBox.PanelTrackSelectionItem = PanelTrackSelectionItem;
    var SelectionPanel = (function (_super) {
        __extends(SelectionPanel, _super);
        function SelectionPanel(game, startClosed, _cursor) {
            var _this = this;
            _super.call(this, game, 0, 0, TrainBox.Assets.SidePanel.assetKey);
            this.isOpen = true;
            this.tweenSpeed = 520;
            this.isCurrentlyTweening = false;
            this.buttonStripWidth = 44;
            this.panelOffset = -190;
            this.selectedItem = null;
            this.SpawnNewTrain = function () {
            };
            this.DisableCursor = function () {
                _this.cursor.Disable();
            };
            this.EnableCursor = function () {
                _this.cursor.Enable();
            };
            this.BringPanelToTop = function () {
                _this.bringToTop();
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].BringPanelToTop();
                }
                _this.trainButton.bringToTop();
            };
            this.PopulateSelectableTracks = function (game, trainButtonCallBack) {
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.STRAIT), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.CORNER), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.CROSS), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.LRIGHT), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.selectableTracks.push(new PanelTrackSelectionItem(game, new TrainBox.Track(game, TrainBox.TrackType.LLEFT), new Phaser.Sprite(_this.game, 0, 0, TrainBox.Assets.PanelInsert.assetKey), _this));
                _this.trainButton = _this.game.add.button(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT, _this.game.height - 150, TrainBox.Assets.MakeTrainButton.assetKey, trainButtonCallBack, _this, 0, 2, 1, 3);
                _this.trainButton.fixedToCamera = true;
                _this.trainButton.cameraOffset.x = SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT - _this.width + _this.buttonStripWidth + _this.trainButton.width;
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
                }
                if (_this.isOpen) {
                    _this.SetDirectlyOpen();
                }
                else {
                    _this.SetDirectlyClosed();
                }
            };
            this.Update = function (mousePos) {
                if (_this.game.input.mousePointer.isDown) {
                    if (_this.selectionArea.contains(mousePos.x, mousePos.y)) {
                        _this.Toggle();
                    }
                }
            };
            this.Toggle = function () {
                if (_this.isCurrentlyTweening == false) {
                    if (_this.isOpen) {
                        _this.TweenOut();
                    }
                    else {
                        _this.TweenIn();
                    }
                }
            };
            this.SetDirectlyOpen = function () {
                _this.isCurrentlyTweening = false;
                _this.selectionArea.x = _this.width - _this.buttonStripWidth + _this.panelOffset;
                _this.isOpen = true;
                _this.cameraOffset.x = _this.panelOffset;
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
                }
                if (_this.trainButton != null) {
                    _this.trainButton.cameraOffset = new Phaser.Point(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT, _this.game.height - SelectionPanel.TRAINBUTTONYFROMBOTTOM - (_this.trainButton.height / 2));
                }
            };
            this.SetDirectlyClosed = function () {
                _this.isCurrentlyTweening = false;
                _this.isOpen = false;
                _this.selectionArea.x = 0;
                _this.cameraOffset.x = -_this.width + _this.buttonStripWidth;
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].SetPosition(new Phaser.Point(PanelTrackSelectionItem.PANELXOFFSET - _this.width + _this.buttonStripWidth + _this.selectableTracks[i].panel.width, PanelTrackSelectionItem.PANELYOFFSET + (i * PanelTrackSelectionItem.PANELYSPACING)));
                }
                if (_this.trainButton != null) {
                    _this.trainButton.position = new Phaser.Point(SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT - _this.width + _this.buttonStripWidth + _this.trainButton.width, _this.game.height - SelectionPanel.TRAINBUTTONYFROMBOTTOM);
                }
            };
            this.TweenOut = function () {
                _this.game.add.tween(_this.cameraOffset).to({ x: 0 - _this.width + _this.buttonStripWidth }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true).onComplete.add(_this.TweenOutComplete, _this);
                _this.isCurrentlyTweening = true;
                _this.slideAudio = _this.game.sound.play(TrainBox.Assets.SlideAudio.assetKey);
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].TweenToX(_this.game, PanelTrackSelectionItem.PANELXOFFSET - _this.width + _this.buttonStripWidth + _this.selectableTracks[i].panel.width, _this.tweenSpeed);
                }
                _this.game.add.tween(_this.trainButton.cameraOffset).to({
                    x: 0 - (_this.trainButton.width * 2) + _this.buttonStripWidth
                }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true);
            };
            this.TweenIn = function () {
                _this.game.add.tween(_this.cameraOffset).to({ x: 0 + _this.panelOffset }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true).onComplete.add(_this.TweenInComplete, _this);
                _this.isCurrentlyTweening = true;
                _this.slideAudio = _this.game.sound.play(TrainBox.Assets.SlideAudio.assetKey);
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    _this.selectableTracks[i].TweenToX(_this.game, PanelTrackSelectionItem.PANELXOFFSET, _this.tweenSpeed);
                }
                _this.game.add.tween(_this.trainButton.cameraOffset).to({
                    x: SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT
                }, _this.tweenSpeed, Phaser.Easing.Back.InOut, true);
            };
            this.TweenOutComplete = function () {
                _this.isCurrentlyTweening = false;
                _this.isOpen = false;
                _this.selectionArea.x = 0;
            };
            this.TweenInComplete = function () {
                _this.isCurrentlyTweening = false;
                _this.selectionArea.x = _this.width - _this.buttonStripWidth + _this.panelOffset;
                _this.isOpen = true;
            };
            this.NotifyOfSelection = function (selectedButton) {
                for (var i = 0; i < _this.selectableTracks.length; i++) {
                    if (_this.selectableTracks[i].isSelected) {
                        _this.selectableTracks[i].Deselect();
                    }
                }
                _this.selectedItem = selectedButton;
            };
            this.GetSelectedItem = function () {
                return _this.selectedItem;
            };
            this.GetItem = function (index) {
                index = Math.floor(index);
                if (_this.selectableTracks == null) {
                    return null;
                }
                if ((index < 0) || (index >= _this.selectableTracks.length)) {
                    return null;
                }
                return _this.selectableTracks[index];
            };
            this.DisableTrainSpawnButtonForTime = function () {
                _this.trainButton.inputEnabled = false;
                _this.trainButton.alpha = 0.5;
                _this.game.time.events.add(Phaser.Timer.SECOND * SelectionPanel.SPAWNTRAINBUTTONCOOLDOWN, _this.EnableTrainButton, _this);
            };
            this.EnableTrainButton = function () {
                _this.trainButton.inputEnabled = true;
                _this.trainButton.alpha = 1.0;
            };
            this.x += this.panelOffset;
            this.selectionArea = new Phaser.Rectangle(this.width - this.buttonStripWidth + this.panelOffset, 0, this.buttonStripWidth, this.height);
            this.cursor = _cursor;
            this.selectableTracks = [];
            this.fixedToCamera = true;
            if (startClosed) {
                this.SetDirectlyClosed();
            }
            this.inputEnabled = true;
            this.cursor = _cursor;
            this.events.onInputOver.add(this.DisableCursor, this);
            this.events.onInputOut.add(this.EnableCursor, this);
        }
        SelectionPanel.TRAINBUTTONXOFFSETFROMLEFT = 19;
        SelectionPanel.TRAINBUTTONYFROMBOTTOM = 80;
        SelectionPanel.SPAWNTRAINBUTTONCOOLDOWN = 5;
        return SelectionPanel;
    })(Phaser.Sprite);
    TrainBox.SelectionPanel = SelectionPanel;
})(TrainBox || (TrainBox = {}));
