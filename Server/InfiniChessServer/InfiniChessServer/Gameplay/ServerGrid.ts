﻿//The server representation of all the players on the serve, and where they are in the world.
import * as ServerCell from './ServerCell';
export class ServerGrid {

    private cells: ServerCell.ServerCell[][];

    static XGRIDTILES: number = 10;
    static YGRIDTILES: number = 10;

    constructor() {
        this.PopulateGridWithCells(ServerGrid.XGRIDTILES, ServerGrid.YGRIDTILES);
    }

    //Called from constructor, so we can bind 'this' reference properly
    private PopulateGridWithCells = (xGridWidth: number, yGridWidth: number) => {

        this.cells = [];

        //The provided x and y numbers are rounded so they're ints, as javascript dosen't "do" ints, the high maintenance bitch.
        xGridWidth = Math.round(xGridWidth);
        yGridWidth = Math.round(yGridWidth);

        //Shove the cells in, creating Y-arrays as we go. Cells are given reference to this, and we use the CELLX/YPIXEL constants to hook up cell positions
        for (var i = 0; i < xGridWidth; i++) {
            this.cells[i] = [];
            for (var j = 0; j < yGridWidth; j++) {
                var newCell: ServerCell.ServerCell = new ServerCell.ServerCell(i,j,this);
                this.cells[i].push(newCell);
            }
        }
    }

    //Directly access cell at specified index
    public GetCellAtIndex = (indexX: number, indexY: number): ServerCell.ServerCell => {
        //Round numbers to ints
        indexX = Math.floor(indexX);
        indexY = Math.floor(indexY);

        if (this.cells.length > indexX) {
            if (this.cells[indexX].length > indexY) {
                return this.cells[indexX][indexY];
            }
        }

        //If we got here, we tried to access out of array bounds, and should error.
        //throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtIndex()");
        return null;
    }

    //Returns the first free cell, i.e, valid cell without a piece on it
    public GetFirstFreeCell = (): ServerCell.ServerCell => {
        for (var i = 0; i < ServerGrid.XGRIDTILES; i++) {
            for (var j = 0; j < ServerGrid.YGRIDTILES; j++) {
                if (this.GetCellAtIndex(i, j).GetPieceOnCell() == null) {
                    return this.GetCellAtIndex(i, j);
                }
            }
        }
        return null;
    }

}
