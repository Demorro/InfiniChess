"use strict";
//The server representation of all the players on the serve, and where they are in the world.
var ServerCell = require('./ServerCell');
var ServerGrid = (function () {
    function ServerGrid() {
        var _this = this;
        //Called from constructor, so we can bind 'this' reference properly
        this.PopulateGridWithCells = function (xGridWidth, yGridWidth) {
            _this.cells = [];
            //The provided x and y numbers are rounded so they're ints, as javascript dosen't "do" ints, the high maintenance bitch.
            xGridWidth = Math.round(xGridWidth);
            yGridWidth = Math.round(yGridWidth);
            //Shove the cells in, creating Y-arrays as we go. Cells are given reference to this, and we use the CELLX/YPIXEL constants to hook up cell positions
            for (var i = 0; i < xGridWidth; i++) {
                _this.cells[i] = [];
                for (var j = 0; j < yGridWidth; j++) {
                    var newCell = new ServerCell.ServerCell(i, j, _this);
                    _this.cells[i].push(newCell);
                }
            }
        };
        //Directly access cell at specified index
        this.GetCellAtIndex = function (indexX, indexY) {
            //Round numbers to ints
            indexX = Math.floor(indexX);
            indexY = Math.floor(indexY);
            if (_this.cells.length > indexX) {
                if (_this.cells[indexX].length > indexY) {
                    return _this.cells[indexX][indexY];
                }
            }
            //If we got here, we tried to access out of array bounds, and should error.
            //throw new Error("Attempting To Access out of range Cell. Grid.ts, GetCellAtIndex()");
            return null;
        };
        //Returns the first free cell, i.e, valid cell without a piece on it
        this.GetFirstFreeCell = function () {
            for (var i = 0; i < ServerGrid.XGRIDTILES; i++) {
                for (var j = 0; j < ServerGrid.YGRIDTILES; j++) {
                    if (_this.GetCellAtIndex(i, j).GetPieceOnCell() == null) {
                        return _this.GetCellAtIndex(i, j);
                    }
                }
            }
            return null;
        };
        this.PopulateGridWithCells(ServerGrid.XGRIDTILES, ServerGrid.YGRIDTILES);
    }
    ServerGrid.XGRIDTILES = 10;
    ServerGrid.YGRIDTILES = 10;
    return ServerGrid;
}());
exports.ServerGrid = ServerGrid;
//# sourceMappingURL=ServerGrid.js.map