﻿import * as ServerCell from '../ServerCell';
import * as ServerGrid from '../ServerGrid';
import * as ServerPlayer from '../ServerPlayer';
import * as ServerPiece from './ServerPiece';
import * as Collections from '../../TSCollections/collections';

//The server representation of a pawn
export class ServerPawn extends ServerPiece.ServerPiece {
    constructor(grid: ServerGrid.ServerGrid, owningPlayer: ServerPlayer.ServerPlayer, indexInStorageMap: number) {
        super(ServerPiece.PieceType.Pawn, grid, owningPlayer, indexInStorageMap);

        this.movementCoolDownInSeconds = 3;
    }

     //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    protected GetCandidateMovementIndices(): Collections.Set<[number, number]> {
        var pts: Collections.Set<[number, number]> = new Collections.Set<[number, number]>();

        var curX: number = 0;
        var curY: number = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }

        //Load all of the possible pawn movement squares
        pts.add([curX + 0, curY + 1]);
        pts.add([curX + 0, curY - 1]);
        pts.add([curX + 1, curY + 0]);
        pts.add([curX - 1, curY + 0]);

        return pts;
    }
}