"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerPiece = require('./ServerPiece');
var Collections = require('../../TSCollections/collections');
//The server representation of a rook
var ServerRook = (function (_super) {
    __extends(ServerRook, _super);
    function ServerRook(grid, owningPlayer, indexInStorageMap) {
        _super.call(this, ServerPiece.PieceType.Rook, grid, owningPlayer, indexInStorageMap);
        this.movementCoolDownInSeconds = 5;
    }
    //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    ServerRook.prototype.GetCandidateMovementIndices = function () {
        var pts = new Collections.Set();
        var range = 10;
        var curX = 0;
        var curY = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }
        //Load all of the possible rook movement squares
        for (var i = 1; i <= range; i++) {
            pts.add([curX + 0, curY + i]);
        }
        for (var j = 1; j <= range; j++) {
            pts.add([curX + 0, curY - j]);
        }
        for (var k = 1; k <= range; k++) {
            pts.add([curX + k, curY + 0]);
        }
        for (var l = 1; l <= range; l++) {
            pts.add([curX - l, curY + 0]);
        }
        return pts;
    };
    return ServerRook;
}(ServerPiece.ServerPiece));
exports.ServerRook = ServerRook;
//# sourceMappingURL=ServerRook.js.map