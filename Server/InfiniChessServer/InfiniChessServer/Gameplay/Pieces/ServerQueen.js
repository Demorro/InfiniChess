"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerPiece = require('./ServerPiece');
var Collections = require('../../TSCollections/collections');
//The server representation of a queen
var ServerQueen = (function (_super) {
    __extends(ServerQueen, _super);
    function ServerQueen(grid, owningPlayer, indexInStorageMap) {
        _super.call(this, ServerPiece.PieceType.Queen, grid, owningPlayer, indexInStorageMap);
        this.movementCoolDownInSeconds = 8;
    }
    //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    ServerQueen.prototype.GetCandidateMovementIndices = function () {
        var pts = new Collections.Set();
        var range = 10;
        var curX = 0;
        var curY = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }
        //Load all of the possible queen movement squares
        for (var i = 1; i <= range; i++) {
            pts.add([curX + 0, curY + i]);
        }
        for (var j = 1; j <= range; j++) {
            pts.add([curX + 0, curY - j]);
        }
        for (var k = 1; k <= range; k++) {
            pts.add([curX + k, curY + 0]);
        }
        for (var l = 1; l <= range; l++) {
            pts.add([curX - l, curY + 0]);
        }
        for (var a = 1; a <= range; a++) {
            pts.add([curX + a, curY + a]);
        }
        for (var b = 1; b <= range; b++) {
            pts.add([curX - b, curY + b]);
        }
        for (var c = 1; c <= range; c++) {
            pts.add([curX + c, curY - c]);
        }
        for (var d = 1; d <= range; d++) {
            pts.add([curX - d, curY - d]);
        }
        return pts;
    };
    return ServerQueen;
}(ServerPiece.ServerPiece));
exports.ServerQueen = ServerQueen;
//# sourceMappingURL=ServerQueen.js.map