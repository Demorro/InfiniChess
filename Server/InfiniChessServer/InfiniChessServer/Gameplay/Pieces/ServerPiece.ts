﻿import * as ServerCell from '../ServerCell';
import * as ServerGrid from '../ServerGrid';
import * as ServerPlayer from '../ServerPlayer';
import * as Collections from '../../TSCollections/collections';

export enum PieceType {
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen
}

//The server representation of a piece on the server, base class to pawn/knight/bishop/queen
export abstract class ServerPiece {

    protected grid: ServerGrid.ServerGrid//Store a reference to the main game grid
    protected sittingCell: ServerCell.ServerCell = null;  //Store a reference to the cell the server thinks this piece is sitting on
    private targetCell: ServerCell.ServerCell = null; //This points to the target movement cell. This differs from the sittingCell in that it isn't null when the piece is moving. When the player is stationary this will equal the sittingCell. There should always be a target cell
    protected owningPlayer: ServerPlayer.ServerPlayer; //Store a reference to the player that owns this piece

    private pieceType: PieceType;
    private indexInMap: number; //The index that this piece is in its containing map, which itself is stored in ServerPlayer

    private timeStampOfLastLanding: number = 0; //The timestamp when this piece last landed on a tile, used for the cooldown timers where the player cant move the piece
    protected movementCoolDownInSeconds: number = 5; //How many seconds must pass between the piece landing and it being able to move again. Set in derived class.

    protected msMoveTimePerCell: number = 250; //A piece takes time to move, this value is how long it should take. Server sends this value, multiplied by the length of the move in cells, back to the client as an "expected arrival" timestamp.

    constructor(pieceType: PieceType, grid: ServerGrid.ServerGrid, owningPlayer: ServerPlayer.ServerPlayer, indexInMap: number) {
        this.pieceType = pieceType;
        this.grid = grid;
        this.owningPlayer = owningPlayer;
        this.indexInMap = indexInMap;

        this.timeStampOfLastLanding = Date.now();
    }

    //Intented to be called from Cell, with Cell.PlacePieceOnCell being the actual way to put a piece on a cell
    public RegisterSittingOnCell = (cell: ServerCell.ServerCell): void => {
        this.timeStampOfLastLanding = Date.now();
        this.sittingCell = cell;
        this.targetCell = cell;
    }

    //Returns the enum type of the piece for public use
    public GetPieceType = (): PieceType => {
        return this.pieceType;
    }

    //Get the cell this piece is sitting on
    public GetCellSatOn = (): ServerCell.ServerCell => {
        return this.sittingCell;
    }

    //Set that this piece is not sitting on a cell. Called from the cell when somethings removed, shouldnt really be called elsewhere.
    public SetNotSittingOnCell = (): void => {
        this.sittingCell = null;
    }

    //Get the cell that this piece is moving too
    public GetTargetCell = (): ServerCell.ServerCell => {
        return this.targetCell;
    }

    //Get the index that this piece is stored at in the containing player-pieces dictionary
    public GetPieceMapIndex = (): number => {
        return Math.round(this.indexInMap);
    }

    //Get the player this piece belongs too.
    public GetOwningPlayer = (): ServerPlayer.ServerPlayer => {
        return this.owningPlayer;
    }

    //The time that the piece landed on the cell it's on. Returns -1 if it's not currently on a cell
    public GetLastTimeStampOfCellLanding = (): number => {
        if (this.sittingCell == null) {
            return -1;
        }

        return this.timeStampOfLastLanding;
    }

    //Move the piece to a cell on the server. Will remove the piece from the current cell, then invoke a callback to place the piece on the target cell after specified time.
    public MovePieceToCell = (cell: ServerCell.ServerCell, timeToArriveAtCell: number): void => {
        this.targetCell = cell;
        this.GetCellSatOn().RemovePieceFromCell();
        this.timeStampOfLastLanding = timeToArriveAtCell;
        setTimeout(cell.PlacePieceOnCell, timeToArriveAtCell - Date.now(), this); //We're using time to arrive everywhere else, but setTimeout wants an interval, so just take off the current date
    }

    //Notify this piece that it's just been taken, called from Cell normally when another piece lands on a spot you on
    public NotifyTaken = (pieceThatTookYou: ServerPiece): void => {
        var takingCell: ServerCell.ServerCell = this.GetCellSatOn();
        this.GetCellSatOn().RemovePieceFromCell();
        this.owningPlayer.ReactToPieceTaken(this, pieceThatTookYou, takingCell);
    }

    //Used for when the player disconnects, mainly just remove the piece from the grid
    public PurgeAllPlayData = (): void => {
        if (this.GetCellSatOn() != null) {
            this.GetCellSatOn().RemovePieceFromCell();
        }
    }

    //Returns true if the movement request is to a valid tile
    public IsValidMove = (tile: ServerCell.ServerCell): boolean => {
        if (this.GetCandidateMovementIndices().contains([tile.GetIndexX(), tile.GetIndexY()])) {
            return true;
        }
        return false;
    }

    //Returns true if the timestamp/cooldown works out that the piece should be able to move. Has a little bit of inbuild leeway to account for network latency
    public MoveValidAccordingToCooldown = (leewaySeconds: number = 0.5): boolean => {
        var timeSinceLastMoveInSecs: number = ((Date.now() - this.timeStampOfLastLanding) / 1000);
        return ((timeSinceLastMoveInSecs + leewaySeconds) > this.movementCoolDownInSeconds);
    }

    //Returns the timestamp that the piece should arrive at target cell if it we're to leave right now.
    public GetProjectedTimeToArriveAtCell = (tile: ServerCell.ServerCell): number => {
        var xMov: number = Math.abs(this.GetCellSatOn().GetIndexX() - tile.GetIndexX());
        var yMov: number = Math.abs(this.GetCellSatOn().GetIndexY() - tile.GetIndexY());

        var movDistance = Math.sqrt((xMov * xMov) + (yMov * yMov));
        return Date.now() + (movDistance * this.msMoveTimePerCell);
    }

    //Derived class must implement a method to set the specific piece type movement indices, relative to piece current position. returns a tuple of indices
    protected abstract GetCandidateMovementIndices(): Collections.Set<[number, number]>

}