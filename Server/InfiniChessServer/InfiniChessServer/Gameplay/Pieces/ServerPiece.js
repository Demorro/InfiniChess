"use strict";
(function (PieceType) {
    PieceType[PieceType["Pawn"] = 0] = "Pawn";
    PieceType[PieceType["Rook"] = 1] = "Rook";
    PieceType[PieceType["Knight"] = 2] = "Knight";
    PieceType[PieceType["Bishop"] = 3] = "Bishop";
    PieceType[PieceType["Queen"] = 4] = "Queen";
})(exports.PieceType || (exports.PieceType = {}));
var PieceType = exports.PieceType;
//The server representation of a piece on the server, base class to pawn/knight/bishop/queen
var ServerPiece = (function () {
    function ServerPiece(pieceType, grid, owningPlayer, indexInMap) {
        var _this = this;
        this.sittingCell = null; //Store a reference to the cell the server thinks this piece is sitting on
        this.targetCell = null; //This points to the target movement cell. This differs from the sittingCell in that it isn't null when the piece is moving. When the player is stationary this will equal the sittingCell. There should always be a target cell
        this.timeStampOfLastLanding = 0; //The timestamp when this piece last landed on a tile, used for the cooldown timers where the player cant move the piece
        this.movementCoolDownInSeconds = 5; //How many seconds must pass between the piece landing and it being able to move again. Set in derived class.
        this.msMoveTimePerCell = 250; //A piece takes time to move, this value is how long it should take. Server sends this value, multiplied by the length of the move in cells, back to the client as an "expected arrival" timestamp.
        //Intented to be called from Cell, with Cell.PlacePieceOnCell being the actual way to put a piece on a cell
        this.RegisterSittingOnCell = function (cell) {
            _this.timeStampOfLastLanding = Date.now();
            _this.sittingCell = cell;
            _this.targetCell = cell;
        };
        //Returns the enum type of the piece for public use
        this.GetPieceType = function () {
            return _this.pieceType;
        };
        //Get the cell this piece is sitting on
        this.GetCellSatOn = function () {
            return _this.sittingCell;
        };
        //Set that this piece is not sitting on a cell. Called from the cell when somethings removed, shouldnt really be called elsewhere.
        this.SetNotSittingOnCell = function () {
            _this.sittingCell = null;
        };
        //Get the cell that this piece is moving too
        this.GetTargetCell = function () {
            return _this.targetCell;
        };
        //Get the index that this piece is stored at in the containing player-pieces dictionary
        this.GetPieceMapIndex = function () {
            return Math.round(_this.indexInMap);
        };
        //Get the player this piece belongs too.
        this.GetOwningPlayer = function () {
            return _this.owningPlayer;
        };
        //The time that the piece landed on the cell it's on. Returns -1 if it's not currently on a cell
        this.GetLastTimeStampOfCellLanding = function () {
            if (_this.sittingCell == null) {
                return -1;
            }
            return _this.timeStampOfLastLanding;
        };
        //Move the piece to a cell on the server. Will remove the piece from the current cell, then invoke a callback to place the piece on the target cell after specified time.
        this.MovePieceToCell = function (cell, timeToArriveAtCell) {
            _this.targetCell = cell;
            _this.GetCellSatOn().RemovePieceFromCell();
            _this.timeStampOfLastLanding = timeToArriveAtCell;
            setTimeout(cell.PlacePieceOnCell, timeToArriveAtCell - Date.now(), _this); //We're using time to arrive everywhere else, but setTimeout wants an interval, so just take off the current date
        };
        //Notify this piece that it's just been taken, called from Cell normally when another piece lands on a spot you on
        this.NotifyTaken = function (pieceThatTookYou) {
            var takingCell = _this.GetCellSatOn();
            _this.GetCellSatOn().RemovePieceFromCell();
            _this.owningPlayer.ReactToPieceTaken(_this, pieceThatTookYou, takingCell);
        };
        //Used for when the player disconnects, mainly just remove the piece from the grid
        this.PurgeAllPlayData = function () {
            if (_this.GetCellSatOn() != null) {
                _this.GetCellSatOn().RemovePieceFromCell();
            }
        };
        //Returns true if the movement request is to a valid tile
        this.IsValidMove = function (tile) {
            if (_this.GetCandidateMovementIndices().contains([tile.GetIndexX(), tile.GetIndexY()])) {
                return true;
            }
            return false;
        };
        //Returns true if the timestamp/cooldown works out that the piece should be able to move. Has a little bit of inbuild leeway to account for network latency
        this.MoveValidAccordingToCooldown = function (leewaySeconds) {
            if (leewaySeconds === void 0) { leewaySeconds = 0.5; }
            var timeSinceLastMoveInSecs = ((Date.now() - _this.timeStampOfLastLanding) / 1000);
            return ((timeSinceLastMoveInSecs + leewaySeconds) > _this.movementCoolDownInSeconds);
        };
        //Returns the timestamp that the piece should arrive at target cell if it we're to leave right now.
        this.GetProjectedTimeToArriveAtCell = function (tile) {
            var xMov = Math.abs(_this.GetCellSatOn().GetIndexX() - tile.GetIndexX());
            var yMov = Math.abs(_this.GetCellSatOn().GetIndexY() - tile.GetIndexY());
            var movDistance = Math.sqrt((xMov * xMov) + (yMov * yMov));
            return Date.now() + (movDistance * _this.msMoveTimePerCell);
        };
        this.pieceType = pieceType;
        this.grid = grid;
        this.owningPlayer = owningPlayer;
        this.indexInMap = indexInMap;
        this.timeStampOfLastLanding = Date.now();
    }
    return ServerPiece;
}());
exports.ServerPiece = ServerPiece;
//# sourceMappingURL=ServerPiece.js.map