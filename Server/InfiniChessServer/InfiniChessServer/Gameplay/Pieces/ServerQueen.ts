﻿import * as ServerCell from '../ServerCell';
import * as ServerGrid from '../ServerGrid';
import * as ServerPlayer from '../ServerPlayer';
import * as ServerPiece from './ServerPiece';
import * as Collections from '../../TSCollections/collections';

//The server representation of a queen
export class ServerQueen extends ServerPiece.ServerPiece {
    constructor(grid: ServerGrid.ServerGrid, owningPlayer: ServerPlayer.ServerPlayer, indexInStorageMap: number) {
        super(ServerPiece.PieceType.Queen, grid, owningPlayer, indexInStorageMap);

        this.movementCoolDownInSeconds = 8;
    }

     //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    protected GetCandidateMovementIndices(): Collections.Set<[number, number]> {
        var pts: Collections.Set<[number, number]> = new Collections.Set<[number, number]>();

        var range: number = 10;

        var curX: number = 0;
        var curY: number = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }

        //Load all of the possible queen movement squares
        for (var i = 1; i <= range; i++) {
            pts.add([curX + 0, curY + i]);
        }
        for (var j = 1; j <= range; j++) {
            pts.add([curX + 0, curY -j]);
        }
        for (var k = 1; k <= range; k++) {
            pts.add([curX + k, curY +0]);
        }
        for (var l = 1; l <= range; l++) {
            pts.add([curX - l, curY +0]);
        }

        for (var a = 1; a <= range; a++) {
            pts.add([curX + a, curY + a]);
        }
        for (var b = 1; b <= range; b++) {
            pts.add([curX - b, curY + b]);
        }
        for (var c = 1; c <= range; c++) {
            pts.add([curX + c, curY -c]);
        }
        for (var d = 1; d <= range; d++) {
            pts.add([curX - d, curY -d]);
        }

        return pts;
    }
}