"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerPiece = require('./ServerPiece');
var Collections = require('../../TSCollections/collections');
//The server representation of a bishop
var ServerBishop = (function (_super) {
    __extends(ServerBishop, _super);
    function ServerBishop(grid, owningPlayer, indexInStorageMap) {
        _super.call(this, ServerPiece.PieceType.Bishop, grid, owningPlayer, indexInStorageMap);
        this.movementCoolDownInSeconds = 5;
    }
    //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    ServerBishop.prototype.GetCandidateMovementIndices = function () {
        var pts = new Collections.Set();
        var range = 10;
        var curX = 0;
        var curY = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }
        //Load all of the possible bishop movement squares
        for (var i = 1; i <= range; i++) {
            pts.add([curX + i, curY + i]);
        }
        for (var j = 1; j <= range; j++) {
            pts.add([curX - j, curY + j]);
        }
        for (var k = 1; k <= range; k++) {
            pts.add([curX + k, curY - k]);
        }
        for (var l = 1; l <= range; l++) {
            pts.add([curX - l, curY - l]);
        }
        return pts;
    };
    return ServerBishop;
}(ServerPiece.ServerPiece));
exports.ServerBishop = ServerBishop;
//# sourceMappingURL=ServerBishop.js.map