"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerPiece = require('./ServerPiece');
var Collections = require('../../TSCollections/collections');
//The server representation of a knight
var ServerKnight = (function (_super) {
    __extends(ServerKnight, _super);
    function ServerKnight(grid, owningPlayer, indexInStorageMap) {
        _super.call(this, ServerPiece.PieceType.Knight, grid, owningPlayer, indexInStorageMap);
        this.movementCoolDownInSeconds = 5;
    }
    //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    ServerKnight.prototype.GetCandidateMovementIndices = function () {
        var pts = new Collections.Set();
        var curX = 0;
        var curY = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }
        //Load all of the possible knight movement squares
        pts.add([curX + 2, curY + 1]);
        pts.add([curX + 1, curY + 2]);
        pts.add([curX - 2, curY + 1]);
        pts.add([curX - 1, curY + 2]);
        pts.add([curX + 2, curY - 1]);
        pts.add([curX + 1, curY - 2]);
        pts.add([curX - 2, curY - 1]);
        pts.add([curX - 1, curY - 2]);
        return pts;
    };
    return ServerKnight;
}(ServerPiece.ServerPiece));
exports.ServerKnight = ServerKnight;
//# sourceMappingURL=ServerKnight.js.map