"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerPiece = require('./ServerPiece');
var Collections = require('../../TSCollections/collections');
//The server representation of a pawn
var ServerPawn = (function (_super) {
    __extends(ServerPawn, _super);
    function ServerPawn(grid, owningPlayer, indexInStorageMap) {
        _super.call(this, ServerPiece.PieceType.Pawn, grid, owningPlayer, indexInStorageMap);
        this.movementCoolDownInSeconds = 3;
    }
    //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    ServerPawn.prototype.GetCandidateMovementIndices = function () {
        var pts = new Collections.Set();
        var curX = 0;
        var curY = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }
        //Load all of the possible pawn movement squares
        pts.add([curX + 0, curY + 1]);
        pts.add([curX + 0, curY - 1]);
        pts.add([curX + 1, curY + 0]);
        pts.add([curX - 1, curY + 0]);
        return pts;
    };
    return ServerPawn;
}(ServerPiece.ServerPiece));
exports.ServerPawn = ServerPawn;
//# sourceMappingURL=ServerPawn.js.map