﻿import * as ServerCell from '../ServerCell';
import * as ServerGrid from '../ServerGrid';
import * as ServerPlayer from '../ServerPlayer';
import * as ServerPiece from './ServerPiece';
import * as Collections from '../../TSCollections/collections';

//The server representation of a knight
export class ServerKnight extends ServerPiece.ServerPiece {
    constructor(grid: ServerGrid.ServerGrid, owningPlayer: ServerPlayer.ServerPlayer, indexInStorageMap: number) {
        super(ServerPiece.PieceType.Knight, grid, owningPlayer, indexInStorageMap);

        this.movementCoolDownInSeconds = 5;
    }

     //Get the movement indices of tiles that this piece could theorectically move to, which is the current grid indices + the possible movement ranges
    protected GetCandidateMovementIndices(): Collections.Set<[number, number]> {
        var pts: Collections.Set<[number, number]> = new Collections.Set<[number, number]>();

        var curX: number = 0;
        var curY: number = 0;
        if (this.sittingCell != null) {
            curX = this.sittingCell.GetIndexX();
            curY = this.sittingCell.GetIndexY();
        }

        //Load all of the possible knight movement squares
        pts.add([curX + 2, curY + 1]);
        pts.add([curX + 1, curY + 2]);

        pts.add([curX -2, curY + 1]);
        pts.add([curX - 1, curY + 2]);

        pts.add([curX + 2, curY -1]);
        pts.add([curX + 1, curY -2]);

        pts.add([curX - 2, curY -1]);
        pts.add([curX - 1, curY -2]);

        return pts;
    }
}