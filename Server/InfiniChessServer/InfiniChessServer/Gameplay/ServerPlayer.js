"use strict";
var ServerKnight = require('./Pieces/ServerKnight');
var ServerBishop = require('./Pieces/ServerBishop');
var ServerRook = require('./Pieces/ServerRook');
var ServerQueen = require('./Pieces/ServerQueen');
var Collections = require('../TSCollections/collections');
require('socket.io');
//The server representation of a player. The player can own multiple pieces
var ServerPlayer = (function () {
    function ServerPlayer(grid, networkClient, connector) {
        var _this = this;
        this.numOfPiecesEverOwned = 0; //Max index to above map, incremented on adding a new piece
        this.timeStampConnectedAt = 0; //When the player connection message got to the server
        this.playerName = "NoNameSet"; //The name the player has given themselves
        this.money = 0; //The amount of money the player has
        //Remove all the pieces/player data that this player has, used for disconnection
        this.PurgeAllPlayData = function () {
            //Remove all of this disconnecting players pieces from the server representation of the cell.
            for (var _i = 0, _a = _this.pieces.values(); _i < _a.length; _i++) {
                var piece = _a[_i];
                piece.PurgeAllPlayData();
            }
            _this.playerName = "NoNameSet";
            _this.timeStampConnectedAt = 0;
            _this.numOfPiecesEverOwned = 0;
            _this.pieces.clear();
        };
        //Removes any current pieces the player has (Shouldnt really have any,) then creates a brand new spawn piece, (which should be a knight), then places this knight on the board somewhere
        this.SpawnFirstPiece = function () {
            _this.numOfPiecesEverOwned++;
            var pieceToAdd;
            var pieceTypeToSpawn = Math.floor(Math.random() * 3.999);
            if (pieceTypeToSpawn == 0) {
                pieceToAdd = new ServerKnight.ServerKnight(_this.grid, _this, _this.numOfPiecesEverOwned);
            }
            else if (pieceTypeToSpawn == 1) {
                pieceToAdd = new ServerBishop.ServerBishop(_this.grid, _this, _this.numOfPiecesEverOwned);
            }
            else if (pieceTypeToSpawn == 2) {
                pieceToAdd = new ServerRook.ServerRook(_this.grid, _this, _this.numOfPiecesEverOwned);
            }
            else {
                pieceToAdd = new ServerQueen.ServerQueen(_this.grid, _this, _this.numOfPiecesEverOwned);
            }
            _this.grid.GetFirstFreeCell().PlacePieceOnCell(pieceToAdd); //Place the new piece on the first available cell
            _this.pieces.setValue(_this.numOfPiecesEverOwned, pieceToAdd);
            _this.timeStampConnectedAt = Date.now();
            return pieceToAdd;
        };
        //Send the current state of all the pieces the player owns to either everyone, or just the player
        this.NetworkSendAllPieceState = function (serverIO, broadcastToAll) {
            if (broadcastToAll === void 0) { broadcastToAll = false; }
            console.log('Sending Player ID : ' + _this.networkClient.id + ' piece state information. Broadcast to all : ' + broadcastToAll);
            for (var _i = 0, _a = _this.pieces.values(); _i < _a.length; _i++) {
                var piece = _a[_i];
                if (piece != null) {
                    if (broadcastToAll) {
                        serverIO.broadcast.emit('piecestate', Date.now(), _this.networkClient.id, piece.GetPieceMapIndex(), piece.GetPieceType(), piece.GetTargetCell().GetIndexX(), piece.GetTargetCell().GetIndexY(), piece.GetLastTimeStampOfCellLanding());
                    }
                    else {
                        serverIO.emit('piecestate', Date.now(), _this.networkClient.id, piece.GetPieceMapIndex(), piece.GetPieceType(), piece.GetTargetCell().GetIndexX(), piece.GetTargetCell().GetIndexY(), piece.GetLastTimeStampOfCellLanding());
                    }
                }
            }
        };
        //Set any properties that the player derives from input from the client to the server object, then broadcast them to the other clients. Mainly for the name.
        this.SetPlayerProperties = function (playerName, server) {
            _this.playerName = playerName;
            _this.BroadcastPlayerProperties(server);
        };
        //Send the player properties to one client
        this.SendPlayerProperties = function (socket) {
            socket.emit('setclientplayerproperties', Date.now(), _this.GetNetworkClient().id, _this.playerName, _this.money);
        };
        //Send the player properties to all clients
        this.BroadcastPlayerProperties = function (server) {
            server.emit('setclientplayerproperties', Date.now(), _this.GetNetworkClient().id, _this.playerName, _this.money);
        };
        //After recieving a movement request, we can send an order to the client so it will actually move, or we can reject it. If a movement order is sent, we will also move the piece on the server
        this.ValidateAndRespondToMovementRequest = function (timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY) {
            if (_this.PieceMovementValid(timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY)) {
                _this.TriggerMove(timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY);
            }
        };
        //Check whether a specific movement of a piece would be valid
        this.PieceMovementValid = function (timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY) {
            var targetCell = _this.grid.GetCellAtIndex(moveRequestIndexX, moveRequestIndexY);
            //If the piece dosent exist, reject request.
            if (!_this.pieces.containsKey(pieceIndexMap)) {
                console.log('rejecting move request for playerID : ' + _this.GetNetworkClient().id + ' because player dosent have piece and piecemapindex');
                return false;
            }
            //If the target cell dosent exist, reject request
            if (targetCell == null) {
                console.log('rejecting move request for playerID : ' + _this.GetNetworkClient().id + ' because target cell dosent exist');
                return false;
            }
            //If the movement isn't valid according to piece movement rules, reject request
            if (!_this.pieces.getValue(pieceIndexMap).IsValidMove(targetCell)) {
                console.log('rejecting move request to X: ' + targetCell.GetIndexX() + ' Y: ' + targetCell.GetIndexY() + ' for playerID : ' + _this.GetNetworkClient().id + ' because piece move is illegal');
                return false;
            }
            //If the piece cooldown hasnt expired, reject movement
            if (!_this.pieces.getValue(pieceIndexMap).MoveValidAccordingToCooldown()) {
                console.log('rejecting move request to X: ' + targetCell.GetIndexX() + ' Y: ' + targetCell.GetIndexY() + ' for playerID : ' + _this.GetNetworkClient().id + ' because piece cooldown time has not expired');
                return false;
            }
            return true;
        };
        //Send move order of a piece out over the network, and move on the server
        this.TriggerMove = function (requestRecievedTimestamp, pieceIndexMap, pieceType, moveOrderIndexX, moveOrderIndexY) {
            var targetCell = _this.grid.GetCellAtIndex(moveOrderIndexX, moveOrderIndexY);
            var piece = _this.pieces.getValue(pieceIndexMap);
            if (targetCell == null) {
                throw new ReferenceError('Invalid target cell');
            }
            if (piece == null) {
                throw new ReferenceError('Invalid movment piece');
            }
            var timeToArriveAt = piece.GetProjectedTimeToArriveAtCell(targetCell);
            console.log('Sending move command to player ID : ' + _this.GetNetworkClient().id + ' to cell X : ' + targetCell.GetIndexX() + ' Y : ' + targetCell.GetIndexY() + ' current timestamp : ' + Date.now() + ', projected arrival timestamp : ' + timeToArriveAt);
            _this.networkClient.server.emit('movementcommand', Date.now(), _this.GetNetworkClient().id, pieceIndexMap, pieceType, _this.pieces.getValue(pieceIndexMap).GetCellSatOn().GetIndexX(), _this.pieces.getValue(pieceIndexMap).GetCellSatOn().GetIndexY(), moveOrderIndexX, moveOrderIndexY, timeToArriveAt);
            piece.MovePieceToCell(targetCell, timeToArriveAt);
        };
        //Called from piece->NotifyTaken(). Removes said piece from the player, and sends a network message notifying of the taking. Also will remove the player from the game if they're out of pieces
        this.ReactToPieceTaken = function (takenPiece, takingPiece, cellTakeHappenedOn) {
            console.log('Piece taken. ' + takingPiece.GetPieceType().toString() + ' takes ' + takenPiece.GetPieceType().toString() + 'at cell X : ' + cellTakeHappenedOn.GetIndexX() + ' Y : ' + cellTakeHappenedOn.GetIndexY());
            _this.networkClient.server.emit('piecetaken', Date.now(), _this.GetNetworkClient().id, takenPiece.GetPieceMapIndex(), takingPiece.GetOwningPlayer().GetNetworkClient().id, takingPiece.GetPieceMapIndex(), cellTakeHappenedOn.GetIndexX(), cellTakeHappenedOn.GetIndexY());
            _this.pieces.remove(takenPiece.GetPieceMapIndex()); //Remove the piece taken from the piece map
            if (_this.pieces.size() <= 0) {
                console.log('Player ID : ' + _this.GetNetworkClient().id + 'is out of pieces. Kicking out of game');
                _this.Kick(); //Remove the player from the game if they're out of pieces
            }
        };
        //Kick the player out of the game, also removes it from the player map of the connector.
        this.Kick = function () {
            for (var _i = 0, _a = _this.pieces.keys(); _i < _a.length; _i++) {
                var pieceKey = _a[_i];
                _this.pieces.remove(pieceKey);
            }
            _this.networkClient.server.emit('disconnectnotify', Date.now(), _this.GetNetworkClient().id);
            _this.connector.GetPlayerMap().remove(_this.GetNetworkClient().id);
        };
        //Get the network client of this player
        this.GetNetworkClient = function () {
            return _this.networkClient;
        };
        //Get the amount of money the player has at the moment
        this.GetMoney = function () {
            return _this.money;
        };
        this.grid = grid;
        this.networkClient = networkClient;
        this.connector = connector;
        this.pieces = new Collections.Dictionary();
        this.money = ServerPlayer.startMoney;
    }
    ServerPlayer.startMoney = 100; //The amount of money the player starts with
    return ServerPlayer;
}());
exports.ServerPlayer = ServerPlayer;
//# sourceMappingURL=ServerPlayer.js.map