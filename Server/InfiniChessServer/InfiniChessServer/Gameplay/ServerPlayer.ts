﻿import * as ServerPiece from './Pieces/ServerPiece';
import * as ServerKnight from './Pieces/ServerKnight';
import * as ServerBishop from './Pieces/ServerBishop';
import * as ServerPawn from './Pieces/ServerPawn';
import * as ServerRook from './Pieces/ServerRook';
import * as ServerQueen from './Pieces/ServerQueen';
import * as ServerGrid from './ServerGrid';
import * as ServerCell from './ServerCell';
import * as Connector from '../Connections/Connector';
import * as Collections from '../TSCollections/collections';
import 'socket.io'

//The server representation of a player. The player can own multiple pieces
export class ServerPlayer{

    private connector: Connector.Connector; //Reference to the connector, which is like main.
    private grid: ServerGrid.ServerGrid; //Reference to the main grid representation
    private networkClient: SocketIO.Client; //Unique client structure the player gets when connecting

    private pieces: Collections.Dictionary<number, ServerPiece.ServerPiece>; //The map of pieces that the player owns. Each player stores a counter and each new piece increments this. Once a piece has been added to the map, the entry at the key should never be reused
    private numOfPiecesEverOwned: number = 0; //Max index to above map, incremented on adding a new piece

    private timeStampConnectedAt: number = 0; //When the player connection message got to the server
    private playerName: string = "NoNameSet"; //The name the player has given themselves

    private money: number = 0; //The amount of money the player has
    private static startMoney = 100; //The amount of money the player starts with

    constructor(grid: ServerGrid.ServerGrid, networkClient: SocketIO.Client, connector: Connector.Connector) {
        this.grid = grid;
        this.networkClient = networkClient;
        this.connector = connector;
        this.pieces = new Collections.Dictionary<number, ServerPiece.ServerPiece>();
        this.money = ServerPlayer.startMoney;
    }

    //Remove all the pieces/player data that this player has, used for disconnection
    public PurgeAllPlayData = (): void => {

        //Remove all of this disconnecting players pieces from the server representation of the cell.
        for (var piece of this.pieces.values()) {
            piece.PurgeAllPlayData();
        }

        this.playerName = "NoNameSet";
        this.timeStampConnectedAt = 0
        this.numOfPiecesEverOwned = 0;
        this.pieces.clear();
    }

    //Removes any current pieces the player has (Shouldnt really have any,) then creates a brand new spawn piece, (which should be a knight), then places this knight on the board somewhere
    public SpawnFirstPiece = (): ServerPiece.ServerPiece => {

        this.numOfPiecesEverOwned++;
        var pieceToAdd: ServerPiece.ServerPiece;

        var pieceTypeToSpawn: number = Math.floor(Math.random() * 3.999);

        if (pieceTypeToSpawn == 0) {
            pieceToAdd = new ServerKnight.ServerKnight(this.grid, this, this.numOfPiecesEverOwned);
        }
        else if (pieceTypeToSpawn == 1) {
            pieceToAdd = new ServerBishop.ServerBishop(this.grid, this, this.numOfPiecesEverOwned);
        }
        else if (pieceTypeToSpawn == 2) {
            pieceToAdd = new ServerRook.ServerRook(this.grid, this, this.numOfPiecesEverOwned);
        }
        else {
            pieceToAdd = new ServerQueen.ServerQueen(this.grid, this, this.numOfPiecesEverOwned);
        }

        this.grid.GetFirstFreeCell().PlacePieceOnCell(pieceToAdd); //Place the new piece on the first available cell
        this.pieces.setValue(this.numOfPiecesEverOwned, pieceToAdd);

        this.timeStampConnectedAt = Date.now();

        return pieceToAdd;
    }

    //Send the current state of all the pieces the player owns to either everyone, or just the player
    public NetworkSendAllPieceState = (serverIO: SocketIO.Socket, broadcastToAll: boolean = false) => {
        console.log('Sending Player ID : ' + this.networkClient.id + ' piece state information. Broadcast to all : ' + broadcastToAll);
        for (var piece of this.pieces.values()) {
            if (piece != null) {

                if (broadcastToAll) {
                    serverIO.broadcast.emit('piecestate', Date.now(), this.networkClient.id, piece.GetPieceMapIndex(), piece.GetPieceType(), piece.GetTargetCell().GetIndexX(), piece.GetTargetCell().GetIndexY(), piece.GetLastTimeStampOfCellLanding());
                }
                else {
                    serverIO.emit('piecestate', Date.now(), this.networkClient.id, piece.GetPieceMapIndex(), piece.GetPieceType(), piece.GetTargetCell().GetIndexX(), piece.GetTargetCell().GetIndexY(), piece.GetLastTimeStampOfCellLanding());
                }
            }
        }
    }

    //Set any properties that the player derives from input from the client to the server object, then broadcast them to the other clients. Mainly for the name.
    public SetPlayerProperties = (playerName: string, server: SocketIO.Server) => {
        this.playerName = playerName;
        this.BroadcastPlayerProperties(server);
    }

    //Send the player properties to one client
    public SendPlayerProperties = (socket: SocketIO.Socket): void => {
        socket.emit('setclientplayerproperties', Date.now(), this.GetNetworkClient().id, this.playerName, this.money);
    }

    //Send the player properties to all clients
    public BroadcastPlayerProperties = (server: SocketIO.Server): void => {
        server.emit('setclientplayerproperties', Date.now(), this.GetNetworkClient().id, this.playerName, this.money);
    }

    //After recieving a movement request, we can send an order to the client so it will actually move, or we can reject it. If a movement order is sent, we will also move the piece on the server
    public ValidateAndRespondToMovementRequest = (timestamp: number, pieceIndexMap: number, pieceType: ServerPiece.PieceType, moveRequestIndexX: number, moveRequestIndexY: number): void => {

        if (this.PieceMovementValid(timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY)) {
            this.TriggerMove(timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY);
        }
      
    }

    //Check whether a specific movement of a piece would be valid
    private PieceMovementValid = (timestamp: number, pieceIndexMap: number, pieceType: ServerPiece.PieceType, moveRequestIndexX: number, moveRequestIndexY: number): boolean => {

        var targetCell: ServerCell.ServerCell = this.grid.GetCellAtIndex(moveRequestIndexX, moveRequestIndexY);

        //If the piece dosent exist, reject request.
        if (!this.pieces.containsKey(pieceIndexMap)) { console.log('rejecting move request for playerID : ' + this.GetNetworkClient().id + ' because player dosent have piece and piecemapindex'); return false; }
        
        //If the target cell dosent exist, reject request
        if (targetCell == null) { console.log('rejecting move request for playerID : ' + this.GetNetworkClient().id + ' because target cell dosent exist'); return false; }

        //If the movement isn't valid according to piece movement rules, reject request
        if (!this.pieces.getValue(pieceIndexMap).IsValidMove(targetCell)) { console.log('rejecting move request to X: ' + targetCell.GetIndexX() + ' Y: ' + targetCell.GetIndexY() + ' for playerID : ' + this.GetNetworkClient().id + ' because piece move is illegal'); return false; }

        //If the piece cooldown hasnt expired, reject movement
        if (!this.pieces.getValue(pieceIndexMap).MoveValidAccordingToCooldown()) { console.log('rejecting move request to X: ' + targetCell.GetIndexX() + ' Y: ' + targetCell.GetIndexY() + ' for playerID : ' + this.GetNetworkClient().id + ' because piece cooldown time has not expired'); return false; }
        return true;
    }

    //Send move order of a piece out over the network, and move on the server
    private TriggerMove = (requestRecievedTimestamp: number, pieceIndexMap: number, pieceType: ServerPiece.PieceType, moveOrderIndexX: number, moveOrderIndexY: number): void => {

        var targetCell: ServerCell.ServerCell = this.grid.GetCellAtIndex(moveOrderIndexX, moveOrderIndexY);
        var piece: ServerPiece.ServerPiece = this.pieces.getValue(pieceIndexMap);
        if (targetCell == null) {throw new ReferenceError('Invalid target cell'); }
        if (piece == null) { throw new ReferenceError('Invalid movment piece'); }

        var timeToArriveAt = piece.GetProjectedTimeToArriveAtCell(targetCell);
        console.log('Sending move command to player ID : ' + this.GetNetworkClient().id + ' to cell X : ' + targetCell.GetIndexX() + ' Y : ' + targetCell.GetIndexY() + ' current timestamp : ' + Date.now() + ', projected arrival timestamp : ' + timeToArriveAt);
        this.networkClient.server.emit('movementcommand', Date.now(), this.GetNetworkClient().id, pieceIndexMap, pieceType, this.pieces.getValue(pieceIndexMap).GetCellSatOn().GetIndexX(), this.pieces.getValue(pieceIndexMap).GetCellSatOn().GetIndexY(), moveOrderIndexX, moveOrderIndexY, timeToArriveAt);
        piece.MovePieceToCell(targetCell, timeToArriveAt);
    }

    //Called from piece->NotifyTaken(). Removes said piece from the player, and sends a network message notifying of the taking. Also will remove the player from the game if they're out of pieces
    public ReactToPieceTaken = (takenPiece: ServerPiece.ServerPiece, takingPiece: ServerPiece.ServerPiece, cellTakeHappenedOn: ServerCell.ServerCell): void => {

        console.log('Piece taken. ' + takingPiece.GetPieceType().toString() + ' takes ' + takenPiece.GetPieceType().toString() + 'at cell X : ' + cellTakeHappenedOn.GetIndexX() + ' Y : ' + cellTakeHappenedOn.GetIndexY());
        this.networkClient.server.emit('piecetaken', Date.now(), this.GetNetworkClient().id, takenPiece.GetPieceMapIndex(), takingPiece.GetOwningPlayer().GetNetworkClient().id, takingPiece.GetPieceMapIndex(), cellTakeHappenedOn.GetIndexX(), cellTakeHappenedOn.GetIndexY());
        this.pieces.remove(takenPiece.GetPieceMapIndex()); //Remove the piece taken from the piece map

        if (this.pieces.size() <= 0) {
            console.log('Player ID : ' + this.GetNetworkClient().id + 'is out of pieces. Kicking out of game');
            this.Kick(); //Remove the player from the game if they're out of pieces
        }
    }

    //Kick the player out of the game, also removes it from the player map of the connector.
    public Kick = (): void => {
        for (var pieceKey of this.pieces.keys()) {
            this.pieces.remove(pieceKey);
        }
        this.networkClient.server.emit('disconnectnotify', Date.now(), this.GetNetworkClient().id);
        this.connector.GetPlayerMap().remove(this.GetNetworkClient().id);
    }

    //Get the network client of this player
    public GetNetworkClient = (): SocketIO.Client => {
        return this.networkClient;
    }

    //Get the amount of money the player has at the moment
    public GetMoney = (): number => {
        return this.money;
    }

}