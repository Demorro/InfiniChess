"use strict";
//Representation of a single grid cell on the server
var ServerCell = (function () {
    function ServerCell(gridIndexX, gridIndexY, parentGrid) {
        var _this = this;
        this.pieceOnCell = null; //The piece that is currently on this cell
        //Places a piece on the cell, displacing other pieces. Should return true if we've displaced a piece.
        this.PlacePieceOnCell = function (piece) {
            var wasPiecePreviouslyOnCell = false;
            if (_this.GetPieceOnCell() != null) {
                wasPiecePreviouslyOnCell = true;
                _this.pieceOnCell.NotifyTaken(piece); //Let the previous piece here know that it's just been taken
            }
            //Put the new piece on this cell
            _this.pieceOnCell = piece;
            _this.pieceOnCell.RegisterSittingOnCell(_this);
            return wasPiecePreviouslyOnCell;
        };
        //Returns the piece currently on the cell, if there is one, otherwise returns null
        this.GetPieceOnCell = function () {
            return _this.pieceOnCell;
        };
        this.RemovePieceFromCell = function () {
            if (_this.pieceOnCell != null) {
                _this.pieceOnCell.SetNotSittingOnCell();
                _this.pieceOnCell = null;
            }
        };
        this.GetNorthCell = function () {
            return _this.parentGrid.GetCellAtIndex(_this.gridIndexX, _this.gridIndexY - 1);
        };
        this.GetEastCell = function () {
            return _this.parentGrid.GetCellAtIndex(_this.gridIndexX + 1, _this.gridIndexY);
        };
        this.GetSouthCell = function () {
            return _this.parentGrid.GetCellAtIndex(_this.gridIndexX, _this.gridIndexY + 1);
        };
        this.GetWestCell = function () {
            return _this.parentGrid.GetCellAtIndex(_this.gridIndexX - 1, _this.gridIndexY);
        };
        this.GetIndexX = function () { return _this.gridIndexX; };
        this.GetIndexY = function () { return _this.gridIndexY; };
        this.gridIndexX = gridIndexX;
        this.gridIndexY = gridIndexY;
        this.parentGrid = parentGrid;
        this.pieceOnCell = null;
    }
    return ServerCell;
}());
exports.ServerCell = ServerCell;
//# sourceMappingURL=ServerCell.js.map