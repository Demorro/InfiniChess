﻿import * as ServerGrid from './ServerGrid';
import * as ServerPiece from './Pieces/ServerPiece';
//Representation of a single grid cell on the server
export class ServerCell {

    private gridIndexX: number;
    private gridIndexY: number;
    private parentGrid: ServerGrid.ServerGrid;

    private pieceOnCell: ServerPiece.ServerPiece = null; //The piece that is currently on this cell

    constructor(gridIndexX: number, gridIndexY: number, parentGrid: ServerGrid.ServerGrid) {
        this.gridIndexX = gridIndexX;
        this.gridIndexY = gridIndexY;
        this.parentGrid = parentGrid;

        this.pieceOnCell = null;
    }

    //Places a piece on the cell, displacing other pieces. Should return true if we've displaced a piece.
    public PlacePieceOnCell = (piece: ServerPiece.ServerPiece): boolean => {
        var wasPiecePreviouslyOnCell: boolean = false;
        if (this.GetPieceOnCell() != null) {
            wasPiecePreviouslyOnCell = true;
            this.pieceOnCell.NotifyTaken(piece); //Let the previous piece here know that it's just been taken
        }

        //Put the new piece on this cell
        this.pieceOnCell = piece;
        this.pieceOnCell.RegisterSittingOnCell(this);
        return wasPiecePreviouslyOnCell;
    }

    //Returns the piece currently on the cell, if there is one, otherwise returns null
    public GetPieceOnCell = (): ServerPiece.ServerPiece => {
        return this.pieceOnCell;
    }

    public RemovePieceFromCell = (): void => {
        if (this.pieceOnCell != null) {
            this.pieceOnCell.SetNotSittingOnCell();
            this.pieceOnCell = null;
        }
    }

    public GetNorthCell = (): ServerCell => {
        return this.parentGrid.GetCellAtIndex(this.gridIndexX, this.gridIndexY - 1);
    }
    public GetEastCell = (): ServerCell => {
        return this.parentGrid.GetCellAtIndex(this.gridIndexX + 1, this.gridIndexY);
    }
    public GetSouthCell = (): ServerCell => {
        return this.parentGrid.GetCellAtIndex(this.gridIndexX, this.gridIndexY + 1);
    }
    public GetWestCell = (): ServerCell => {
        return this.parentGrid.GetCellAtIndex(this.gridIndexX - 1, this.gridIndexY);
    }

    public GetIndexX = (): number => { return this.gridIndexX; }
    public GetIndexY = (): number => { return this.gridIndexY; }
}
