﻿/// <reference path="Connections/Connector.ts" /> 

var nodeStatic: any = require('node-static');
var http: any = require('http');
var socketIO: any = require('socket.io');

import * as ServerGrid from './Gameplay/ServerGrid';
var serverGrid: ServerGrid.ServerGrid = new ServerGrid.ServerGrid();

import * as Connector from './Connections/Connector';
var connector: Connector.Connector = new Connector.Connector(nodeStatic, http, socketIO, serverGrid);
connector.StartListeningForConnections();
