/// <reference path="Connections/Connector.ts" /> 
"use strict";
var nodeStatic = require('node-static');
var http = require('http');
var socketIO = require('socket.io');
var ServerGrid = require('./Gameplay/ServerGrid');
var serverGrid = new ServerGrid.ServerGrid();
var Connector = require('./Connections/Connector');
var connector = new Connector.Connector(nodeStatic, http, socketIO, serverGrid);
connector.StartListeningForConnections();
//# sourceMappingURL=app.js.map