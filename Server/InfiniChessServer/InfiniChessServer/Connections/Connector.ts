﻿import * as ServerGrid from '../Gameplay/ServerGrid';
import * as ServerPiece from '../Gameplay/Pieces/ServerPiece';
import * as ServerPlayer from '../Gameplay/ServerPlayer';
import * as Collections from '../TSCollections/collections';
//Connector that handles listening for clients and dealing with adding/removing them from play
export class Connector {

    //Connection primitives
    static IO: SocketIO.Server; //The socketIO input output object
    file: any;
    httpServer: any;
    static LISTENPORT: number = 3000;

    //A reference to the server grid so we can add incoming players
    serverGrid: ServerGrid.ServerGrid;
    //Keep a list of all the players currently in the game, the key is the socketIO assigned id
    players: Collections.Dictionary<string, ServerPlayer.ServerPlayer>;

    constructor(nodeStatic: any, http: any, socketIO: any, serverGrid: ServerGrid.ServerGrid)
    {
        this.players = new Collections.Dictionary<string, ServerPlayer.ServerPlayer>();

        // create our file server config
        this.file = new nodeStatic.Server('bin', { // bin is the folder containing our html, etc
            cache: 0,	// don't cache
            gzip: true	// gzip our assets
        });

        // create our server
        this.httpServer = http.createServer(function (request : any, response : any) {
            request.addListener('end', function () {
                this.file.serve(request, response);
            });
            request.resume();
        }).listen(Connector.LISTENPORT);

        Connector.IO = socketIO();
        Connector.IO.serveClient(false); // the server will not serve the client js file
        Connector.IO.attach(this.httpServer);
        this.serverGrid = serverGrid; //Store a reference to the server grid for adding players as they join
    }

    public StartListeningForConnections = (): void => {
        // listen for a connection|
        Connector.IO.on('connection', this.ConnectPlayer);
        console.log('Server Listening on port : ' + Connector.LISTENPORT);
    }

    public StartListeningForGameUpdatesFromPlayer = (playerSocket: SocketIO.Socket): void => {
        playerSocket.on('setserverplayerproperties', this.SetPlayerProperties);
        playerSocket.on('movementrequest', this.RecieveMovementRequest);
        playerSocket.on('disconnect', this.InformOfPlayerDisconnects);
        console.log('Server Listening for game updates from player id : ' + playerSocket.id);
    }

    //Called on a player connecting
    private ConnectPlayer = (socket: SocketIO.Socket): boolean => {

        this.StartListeningForGameUpdatesFromPlayer(socket);

        //Create a new player object and call initialistation functions
        var playerToAdd: ServerPlayer.ServerPlayer = new ServerPlayer.ServerPlayer(this.serverGrid, socket.client, this);
        this.players.setValue(playerToAdd.GetNetworkClient().id, playerToAdd);
        //Create the first piece for the player.
        var firstSpawnedPiece: ServerPiece.ServerPiece = playerToAdd.SpawnFirstPiece();

        //Output information about the new player to server console
        console.log('Player ID : ' + socket.client.id + ' Connected, IP : ' + socket.client.conn.remoteAddress);
        console.log('Spawning First Player Piece at Index. X : ' + firstSpawnedPiece.GetCellSatOn().GetIndexX() + ', Y : ' + firstSpawnedPiece.GetCellSatOn().GetIndexY());

        //Send the connection response back to the connected player
        console.log('Sending Connection Response');
        console.log('');
        this.SendConnectionResponse(socket, firstSpawnedPiece);

        return true;
    }


    //After recieving a disconnect message from a player, let the rest of the room know about this disconnection
    private InformOfPlayerDisconnects = (): void => {

        for (var clientID of this.players.keys()) {
            if ((this.players.getValue(clientID).GetNetworkClient().conn.readyState == 'closed') || (this.players.getValue(clientID).GetNetworkClient().conn.readyState == 'closing')) {
                Connector.IO.emit('disconnectnotify', Date.now(), clientID);
                console.log('Disconnecting player ID : ' + clientID);
                if (this.players.containsKey(clientID)){
                    this.players.getValue(clientID).PurgeAllPlayData();
                    this.players.remove(clientID);
                }
            }
        }
    }

    //Send approval message back to just connected player, send the index of the piece just added so the player can place it
    private SendConnectionResponse = (socket: SocketIO.Socket, startingPiece: ServerPiece.ServerPiece) => {
        //respons to the player that has just connected, so it can spawn its piece
        socket.emit('connectionresponse', Date.now(), socket.client.id, startingPiece.GetPieceType(), startingPiece.GetPieceMapIndex(), startingPiece.GetCellSatOn().GetIndexX(), startingPiece.GetCellSatOn().GetIndexY());
        //Let the just joined player know about all the other pieces
        for (var player of this.players.values()) {
            if (player != null) {
                if (startingPiece.GetOwningPlayer() != player) { //Dont send information about itself to the player
                    player.NetworkSendAllPieceState(socket);
                    player.SendPlayerProperties(socket);
                }
            }
        }
        //Let all the other players know about the just joined player
        startingPiece.GetOwningPlayer().NetworkSendAllPieceState(socket, true);
    }

    //After recieving a packet about player properties, set them in the correct player struct. Throw an exception if there isnt a player of this id, cause there really should be.
    private SetPlayerProperties = (timeStamp: number, playerID: string, playerName: string): void => {
        if (!this.players.containsKey(playerID)) { throw new Error('Player map does not contain player of ID : ' + playerID + ' in SetPlayerProperties. Attempted Player Name : ' + playerName); }
        console.log('Setting player Properties for Player ID : ' + playerID + ' Name : ' + playerName);
        this.players.getValue(playerID).SetPlayerProperties(playerName, Connector.IO); //Set the player properties, this also broadcasts them to everyone else
    }

    //Recieve a movement request from a player about a specific piece, determine whether that piece is allowed to move, then respond to that player, letting it know the piece can move, and letting it know at when the piece should arrive
    private RecieveMovementRequest = (timestamp: number, playerID: string, pieceIndexMap: number, pieceType: ServerPiece.PieceType, moveRequestIndexX: number, moveRequestIndexY: number) => {
        console.log("Recieving movement request for playerID : " + playerID + " Piecetype : " + pieceType.toString() + " At Tile     X: " + moveRequestIndexX.toString() + " Y: " + moveRequestIndexY.toString());

        if (this.players.containsKey(playerID)) {
            //Player object performs validation and sends movement approval response.
            this.players.getValue(playerID).ValidateAndRespondToMovementRequest(timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY);
        }
    }

    public GetPlayerMap = (): Collections.Dictionary<string, ServerPlayer.ServerPlayer> => {
        return this.players;
    }
}