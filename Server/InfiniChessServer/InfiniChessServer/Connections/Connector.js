"use strict";
var ServerPlayer = require('../Gameplay/ServerPlayer');
var Collections = require('../TSCollections/collections');
//Connector that handles listening for clients and dealing with adding/removing them from play
var Connector = (function () {
    function Connector(nodeStatic, http, socketIO, serverGrid) {
        var _this = this;
        this.StartListeningForConnections = function () {
            // listen for a connection|
            Connector.IO.on('connection', _this.ConnectPlayer);
            console.log('Server Listening on port : ' + Connector.LISTENPORT);
        };
        this.StartListeningForGameUpdatesFromPlayer = function (playerSocket) {
            playerSocket.on('setserverplayerproperties', _this.SetPlayerProperties);
            playerSocket.on('movementrequest', _this.RecieveMovementRequest);
            playerSocket.on('disconnect', _this.InformOfPlayerDisconnects);
            console.log('Server Listening for game updates from player id : ' + playerSocket.id);
        };
        //Called on a player connecting
        this.ConnectPlayer = function (socket) {
            _this.StartListeningForGameUpdatesFromPlayer(socket);
            //Create a new player object and call initialistation functions
            var playerToAdd = new ServerPlayer.ServerPlayer(_this.serverGrid, socket.client, _this);
            _this.players.setValue(playerToAdd.GetNetworkClient().id, playerToAdd);
            //Create the first piece for the player.
            var firstSpawnedPiece = playerToAdd.SpawnFirstPiece();
            //Output information about the new player to server console
            console.log('Player ID : ' + socket.client.id + ' Connected, IP : ' + socket.client.conn.remoteAddress);
            console.log('Spawning First Player Piece at Index. X : ' + firstSpawnedPiece.GetCellSatOn().GetIndexX() + ', Y : ' + firstSpawnedPiece.GetCellSatOn().GetIndexY());
            //Send the connection response back to the connected player
            console.log('Sending Connection Response');
            console.log('');
            _this.SendConnectionResponse(socket, firstSpawnedPiece);
            return true;
        };
        //After recieving a disconnect message from a player, let the rest of the room know about this disconnection
        this.InformOfPlayerDisconnects = function () {
            for (var _i = 0, _a = _this.players.keys(); _i < _a.length; _i++) {
                var clientID = _a[_i];
                if ((_this.players.getValue(clientID).GetNetworkClient().conn.readyState == 'closed') || (_this.players.getValue(clientID).GetNetworkClient().conn.readyState == 'closing')) {
                    Connector.IO.emit('disconnectnotify', Date.now(), clientID);
                    console.log('Disconnecting player ID : ' + clientID);
                    if (_this.players.containsKey(clientID)) {
                        _this.players.getValue(clientID).PurgeAllPlayData();
                        _this.players.remove(clientID);
                    }
                }
            }
        };
        //Send approval message back to just connected player, send the index of the piece just added so the player can place it
        this.SendConnectionResponse = function (socket, startingPiece) {
            //respons to the player that has just connected, so it can spawn its piece
            socket.emit('connectionresponse', Date.now(), socket.client.id, startingPiece.GetPieceType(), startingPiece.GetPieceMapIndex(), startingPiece.GetCellSatOn().GetIndexX(), startingPiece.GetCellSatOn().GetIndexY());
            //Let the just joined player know about all the other pieces
            for (var _i = 0, _a = _this.players.values(); _i < _a.length; _i++) {
                var player = _a[_i];
                if (player != null) {
                    if (startingPiece.GetOwningPlayer() != player) {
                        player.NetworkSendAllPieceState(socket);
                        player.SendPlayerProperties(socket);
                    }
                }
            }
            //Let all the other players know about the just joined player
            startingPiece.GetOwningPlayer().NetworkSendAllPieceState(socket, true);
        };
        //After recieving a packet about player properties, set them in the correct player struct. Throw an exception if there isnt a player of this id, cause there really should be.
        this.SetPlayerProperties = function (timeStamp, playerID, playerName) {
            if (!_this.players.containsKey(playerID)) {
                throw new Error('Player map does not contain player of ID : ' + playerID + ' in SetPlayerProperties. Attempted Player Name : ' + playerName);
            }
            console.log('Setting player Properties for Player ID : ' + playerID + ' Name : ' + playerName);
            _this.players.getValue(playerID).SetPlayerProperties(playerName, Connector.IO); //Set the player properties, this also broadcasts them to everyone else
        };
        //Recieve a movement request from a player about a specific piece, determine whether that piece is allowed to move, then respond to that player, letting it know the piece can move, and letting it know at when the piece should arrive
        this.RecieveMovementRequest = function (timestamp, playerID, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY) {
            console.log("Recieving movement request for playerID : " + playerID + " Piecetype : " + pieceType.toString() + " At Tile     X: " + moveRequestIndexX.toString() + " Y: " + moveRequestIndexY.toString());
            if (_this.players.containsKey(playerID)) {
                //Player object performs validation and sends movement approval response.
                _this.players.getValue(playerID).ValidateAndRespondToMovementRequest(timestamp, pieceIndexMap, pieceType, moveRequestIndexX, moveRequestIndexY);
            }
        };
        this.GetPlayerMap = function () {
            return _this.players;
        };
        this.players = new Collections.Dictionary();
        // create our file server config
        this.file = new nodeStatic.Server('bin', {
            cache: 0,
            gzip: true // gzip our assets
        });
        // create our server
        this.httpServer = http.createServer(function (request, response) {
            request.addListener('end', function () {
                this.file.serve(request, response);
            });
            request.resume();
        }).listen(Connector.LISTENPORT);
        Connector.IO = socketIO();
        Connector.IO.serveClient(false); // the server will not serve the client js file
        Connector.IO.attach(this.httpServer);
        this.serverGrid = serverGrid; //Store a reference to the server grid for adding players as they join
    }
    Connector.LISTENPORT = 3000;
    return Connector;
}());
exports.Connector = Connector;
//# sourceMappingURL=Connector.js.map